({ 
    updateContentVersion: function(component,parentId,docType,sendDocToClaimHub,IsActivityRequired,claimId,documentId,authorityId) {
        console.log('All Values :'+'--'+parentId+'--'+docType+'--'+sendDocToClaimHub+'--'+claimId+'--'+documentId+'--'+authorityId);
        if(claimId==''){
            claimId = parentId;
        }
        var action = component.get("c.updateFileDescription");
        action.setParams({
            parentId:parentId,
            docType:docType,
            sendDocToClaimHub:sendDocToClaimHub,
            isActivityRequired:IsActivityRequired,
            claimId:claimId,
            documentId : documentId,
            claimJobId : authorityId
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            console.log('state'+state);
            if (state === "SUCCESS") {
                component.set("v.listOfDocuments", a.getReturnValue());
            }else{
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Unknown error"+errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                
            }
        });
        $A.enqueueAction(action);
    },
    show: function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
    },
    hide:function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    },
    checkVaildation: function(component, event) {
        var onChangevalue = component.find("sendDocToClaimHub").get("v.checked");  
        var doctype = component.find('fieldId').get('v.value');
        component.set("v.isFileUpload", onChangevalue);
        if(onChangevalue == false){
            if(doctype == ''){
                component.set('v.disabled',true); 
            }else{
                component.set('v.disabled',false); 
            }    	
        }else{
            var selectedRecord=component.get("v.selectedRecord");
            console.log(selectedRecord+'-----'+doctype);
            if(doctype == '' || selectedRecord == '' || selectedRecord == null || selectedRecord == 'null'){
                component.set('v.disabled',true); 
            }else{
                component.set('v.disabled',false); 
            }                       
        }
	}
})