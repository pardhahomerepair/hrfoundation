({  
    doInit : function(component, event, helper){
        console.log('doInit:');
        component.set("v.selectedValue",'');
        if(!$A.util.isUndefined(component.find("sendDocToClaimHub"))){
            component.find("sendDocToClaimHub").set("v.checked",false);
        }
        //Fetch custom metadata values 
        var actionPickList = component.get("c.fetchCustomMetaDataValues"); 
        actionPickList.setParams({
            platform : component.get("v.Platform")
        });
        actionPickList.setCallback(this, function(response) {
            component.set("v.showLoadingSpinner", false);
            var state = response.getState();
            if (state === "SUCCESS") { 
                console.log('platform :',component.get("v.Platform"))
                console.log('doInit:',response.getReturnValue());
                if(!$A.util.isEmpty(response.getReturnValue())){ 
                    component.set("v.docType", response.getReturnValue());                    
                    var action = component.get("c.fetchContentDocument"); 
                    action.setParams({
                        caseId : component.get("v.recordId")
                    });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            if(!$A.util.isEmpty(response.getReturnValue())){                    
                                component.set("v.listOfDocuments", response.getReturnValue());
                            }
                        }
                        else if(state === "ERROR"){
                            console.log('A problem occurred: ' + JSON.stringify(response.error));
                        }
                    });        
                    $A.enqueueAction(action);
                    
                }
            }else if(state === "ERROR"){
                console.log('A problem occurred: ' + JSON.stringify(response.error));
            }
        });
        $A.enqueueAction(actionPickList);
    },
    handleUploadFinished: function(component, event, helper) {
        var uploadedFiles = event.getParam("files");
        var documentId=uploadedFiles[0].documentId;
        var parentId = component.get("v.recordId");
        var docType = component.get("v.selectedValue");
        var sendDocToClaimHub=false;
        var IsActivityRequired=component.get("v.IsActivityRequired");
        var claimId=component.get("v.claimId");
        var authorityId=null;
        console.log('IsActivityRequired',IsActivityRequired);
        if(!$A.util.isUndefined(component.find("sendDocToClaimHub"))){
            var sendDocToClaimHub = component.find("sendDocToClaimHub").get("v.checked");
        }
        
        if(component.get("v.selectedRecord") != null){
            authorityId = component.get("v.selectedRecord").Id;
        }
      //  alert(component.get("v.selectedRecord"));
        console.log('IsActivityRequired call Update');
        helper.updateContentVersion(component,parentId,docType,sendDocToClaimHub,IsActivityRequired,claimId,documentId,authorityId);
        component.set("v.uploadedAttachments", uploadedFiles);
        $A.get('e.force:refreshView').fire();
        location.reload();
    },
    onChangeSendDocToClaimHub : function(component, event, helper) {        
        helper.checkVaildation(component, event);
    },
    handleComponentEvent: function(component, event, helper) { 
        helper.checkVaildation(component, event);
    },
    handleClearEvent: function(component, event, helper) {        
        component.set("v.selectedRecord",'');
        helper.checkVaildation(component, event);
    },
    onChange: function (component, evt, helper) {
        if(component.get("v.Platform") == "Claim"){
            helper.checkVaildation(component, event);
        }else{
            var doctype = component.find('fieldId').get('v.value');
            if(doctype == ''){
                component.set('v.disabled',true); 
            }else{
                component.set('v.disabled',false); 
            }
        }        
    }  
})