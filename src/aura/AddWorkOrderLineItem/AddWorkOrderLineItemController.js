({
    doInit : function(component, event, helper) {
        helper.initHelper(component, event, helper);
    },
    
    saveWoliRecord : function(component, event, helper) {
        var woliObj = component.get("v.objWorkOrderLineItem");
        var type;
        if(component.get("v.selectedPricebookEntryLookUpRecord") != undefined){ 
            if(component.get("v.selectedRoomLookUpRecord") != undefined){
                woliObj.Room__c = component.get("v.selectedRoomLookUpRecord").Id;                
                var labourAmount = component.find("labourAmount").get("v.value");
                var materialAmount = component.find("materialAmount").get("v.value");
                var labourHr;
                var quantity = component.find("quantity").get("v.value");
                woliObj.PricebookEntryId = component.get("v.selectedPricebookEntryLookUpRecord").Id;
                woliObj.Service_Appointment__c = component.get("v.selectedServiceApptLookUpRecord").Id;
                var hr = component.find("labourHr").get("v.value");
                var min = component.find("labourMin").get("v.value");            
                labourHr=parseInt((hr*60))+parseInt(min);
                woliObj.WorkOrderId = component.get("v.recordId");
                woliObj.Labour_Time__c = labourHr;
                woliObj.Quantity = quantity;
                woliObj.Labour_Rate__c=component.get("v.selectedPricebookEntryLookUpRecord").Labour_Price__c;
                woliObj.Material_Rate__c=component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c;
            }else{
                type = "error";             
                helper.showToast(type, 'Room cannot be empty. Please select Room');
            }
        }else{
            var message;
            if(component.get("v.selectedPricebookEntryLookUpRecord") == undefined){
                message = "Product cannot be empty. Please select Product"; 
            }
            type = "error";             
            helper.showToast(type, message);
        }
        
        if (type != 'error'){
            //call apex class method
            component.set("v.Spinner", true); 
            var action = component.get('c.createWorkOrderLineItem');
            action.setParams({
                'woli': woliObj
            })
            action.setCallback(this, function(response) {
                //store state of response
                var state = response.getState();
                if (state === "SUCCESS") {
                    var woLineItemNumber = response.getReturnValue();
                    console.log('woNumber ' + woLineItemNumber);
                    var message = "WorkOrderLineItem" + ' ' + woLineItemNumber + ' ' + "created successfuly";
                    var type = "success";             
                    helper.showToast(type, message);
                    $A.get('e.force:refreshView').fire();
                }else{
                   helper.showToast('error', response.getReturnValue());
                   alert(response.getReturnValue());
                }
            });
            $A.enqueueAction(action);   
        }
        
    },
    handleCancel : function(component, event, helper) {
        var message = "WorkOrderLineItem creation cancelled";
        var type = "error";             
        helper.showToast(type, message);
        $A.get('e.force:refreshView').fire();
    },
    handleTimevalues : function(component, event, helper) {
        if(component.get("v.selectedPricebookEntryLookUpRecord") != undefined){
            var labourTime=Math.floor(component.get("v.selectedPricebookEntryLookUpRecord").Labour_TIME_mins__c);
            var quantity=1;
            component.find("quantity").set("v.value",quantity);
            var HrMin = (Number(labourTime)/60).toFixed(2);
            var hr= parseInt(HrMin);
            var min=labourTime-(hr*60);
            component.find("labourHr").set("v.value",hr);
            component.find("labourMin").set("v.value",min);
            
            var labourAmount=component.get("v.selectedPricebookEntryLookUpRecord").Labour_Price__c;
            var labourMin=labourTime;
            
            if(isNaN(((labourAmount*labourMin)/60)*quantity)){
                component.find("labourAmount").set("v.value",0.0);
                component.find("totalAmount").set("v.value",0.0);
            }else{
                component.find("labourAmount").set("v.value",((labourAmount*labourMin)/60)*quantity);
                var totalAmount=((labourAmount*labourMin)/60)*quantity + (component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c*quantity);
                component.find("totalAmount").set("v.value",totalAmount);
            }
            var materialPrice = component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c;
            if(isNaN(materialPrice)){
                component.find("materialAmount").set("v.value",0.0);
            }
            
        }
    },
    handleChange: function(component, event, helper) {
        if(component.get("v.selectedPricebookEntryLookUpRecord") != undefined){
            var quantity=component.find("quantity").get("v.value");
            var hr = component.find("labourHr").get("v.value");
            var min = component.find("labourMin").get("v.value");            
            var labourAmount=component.get("v.selectedPricebookEntryLookUpRecord").Labour_Price__c;
            var labourMin=parseInt((hr*60))+parseInt(min);
            console.log(isNaN(((labourAmount*labourMin)/60)*quantity));
            if(isNaN(((labourAmount*labourMin)/60)*quantity)){
                component.find("labourAmount").set("v.value",0.0);
                component.find("totalAmount").set("v.value",0.0);
            }else{
                component.find("labourAmount").set("v.value",((labourAmount*labourMin)/60)*quantity);
                var totalAmount=((labourAmount*labourMin)/60)*quantity + (component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c*quantity);
                component.find("totalAmount").set("v.value",totalAmount);
            }
            var materialPrice = component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c;
            if(isNaN(materialPrice)){
                component.find("materialAmount").set("v.value",0.0);
            }
        }
    },
    handleQuantityChange: function(component, event, helper) {
        if(component.get("v.selectedPricebookEntryLookUpRecord") != undefined){
            var quantity=component.find("quantity").get("v.value");
            if (quantity > 0){
                var labourTime;
                var hr;
                var min;
                if (component.get("v.selectedPricebookEntryLookUpRecord").Labour_TIME_mins__c == 0){
                    hr = component.find("labourHr").get("v.value");
                    min = component.find("labourMin").get("v.value");
                }else{
                    labourTime=Math.floor(component.get("v.selectedPricebookEntryLookUpRecord").Labour_TIME_mins__c)*quantity;
                    var HrMin = (Number(labourTime)/60).toFixed(2);
                    hr= parseInt(HrMin);
                    min=labourTime-(hr*60)
                }
                console.log('labourTime',labourTime);;
                component.find("labourHr").set("v.value",hr);
                component.find("labourMin").set("v.value",min);
                var labourMin=parseInt((hr*60))+parseInt(min);
                var labourAmount=component.get("v.selectedPricebookEntryLookUpRecord").Labour_Price__c;
                if(isNaN(((labourAmount*labourMin)/60)*quantity)){
                    component.find("labourAmount").set("v.value",0.0);
                    component.find("totalAmount").set("v.value",0.0);
                }else{
                    component.find("labourAmount").set("v.value",((labourAmount*labourMin)/60)*quantity);
                    var totalAmount=((labourAmount*labourMin)/60)*quantity + (component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c*quantity);
                    component.find("totalAmount").set("v.value",totalAmount);
                }
                var materialPrice = component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c;
                if(isNaN(materialPrice)){
                    component.find("materialAmount").set("v.value",0.0);
                }
            }
        }
    },
    clearScreen: function(component, event, helper) {
        console.log('clearScreen fired');
        var objectName = event.getParam("objectName");
        console.log('objectName: ' + objectName)
        if (objectName == 'PricebookEntry'){
            component.find("labourAmount").set("v.value", "0.00");
            component.find("materialAmount").set("v.value", "0.00");
            component.set("v.selectedPricebookEntryLookUpRecord",null)
        }
    },
    
    overrideCosts : function(component, event, helper) {
        console.log('overrideCosts fired');
        component.find("labourAmount").set("v.value", component.get("v.labourAmount"));
        component.find("materialAmount").set("v.value", component.get("v.materialAmount"));
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
})