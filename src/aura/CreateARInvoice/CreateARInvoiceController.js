({
	doInit : function(component, event, helper) {
        console.log('initiating');
		var action = component.get("c.createManualAR");
        var spinner = component.find("mySpinner");
		$A.util.toggleClass(spinner, "slds-hide");
        action.setParams({            
            'recordId' : component.get('v.recordId')
        });

         action.setCallback(this, function(response) { 
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
			dismissActionPanel.fire();
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {                
                if(response.getReturnValue().includes('Success')) {                 	
                    var array = response.getReturnValue().split(":");
                    helper.showToast('success', 'AR Invoice created Successfully');
                    $A.get('e.force:refreshView').fire();

				 }
                else {
                    helper.showToast('error', response.getReturnValue());
                }
            } 
            $A.util.toggleClass(spinner, "slds-hide");

        });        
        $A.enqueueAction(action);
	}
  
})