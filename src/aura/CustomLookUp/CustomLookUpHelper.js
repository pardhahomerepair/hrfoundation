({
    searchHelper : function(component,event,getInputkeyWord) {
        // call the apex class method 
        var action = component.get("c.fetchLookUpValues");
        // set param to method  
        //alert(component.get("v.objectAPIName"));
           //  alert(component.get("v.objectRecordId"));

        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'objectName' : component.get("v.objectAPIName"),
            'recordId' : component.get("v.objectRecordId")
        });            
        
        // set a callBack    
        action.setCallback(this, function(response) {
            var spinner = component.find('spinner');
            var evt = spinner.get("e.toggle");
            evt.setParams({ isVisible : false });
            evt.fire();
            var state = response.getState();
            console.log('state',state);
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log(storeResponse);
                // if storeResponse size is equal 0 ,display No Records Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Records Found...');
                } else {
                    component.set("v.Message", '');
                    // set searchResult list with return value from server.
                }
          //  alert(JSON.stringify(storeResponse));
                component.set("v.listOfSearchRecords", storeResponse); 
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    },
    clearLookup : function(component) {
        
        var pillTarget = component.find("lookup-pill");
        var lookUpTarget = component.find("lookupField"); 
        
        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');
        
        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');
        
        
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null );
        
        component.set("v.selectedRecord",{});
        
        // call the event   
        var compEvent = component.getEvent("clearLookUpRecordEventFired");
        // set the clearance flag to the event attribute.  
        compEvent.setParams({"objectName" : component.get("v.objectAPIName")});  
        // fire the event  
        compEvent.fire(); 
        component.set("v.SearchKeyWord",null);
    }
})