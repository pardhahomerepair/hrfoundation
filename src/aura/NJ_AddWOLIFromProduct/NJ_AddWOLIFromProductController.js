({
    handleInit : function(component, event, helper) {
      helper.initWoli(component);
    },
    handleSel1Change : function(component, event, helper) {
      let sVal = component.find('selVal1').get('v.value');
      let whrFields = [];
      let whrFieldsVals = [];
      whrFields.push('Work_Type__r.Name');
      whrFields.push('Level_1_Description__c');
      whrFieldsVals.push(component.get("v.selwtName"));
      whrFieldsVals.push(sVal);
      helper.populateDropdown(component, "v.lvl2", "Level_2_Description__c", whrFields, whrFieldsVals);
    },
    handleSel2Change : function(component, event, helper) {
      let sVal = component.find('selVal2').get('v.value');

      let whrFields = [];
      let whrFieldsVals = [];
      whrFields.push('Work_Type__r.Name');
      whrFields.push('Level_1_Description__c');
      whrFields.push('Level_2_Description__c');

      whrFieldsVals.push(component.get("v.selwtName"));
      whrFieldsVals.push(component.get("v.sellvl1"));
      whrFieldsVals.push(sVal);

      helper.populateDropdown(component, "v.lvl3", "Level_3_Description__c", whrFields, whrFieldsVals);

    },
    handleSel3Change : function(component, event, helper) {
      let sVal = component.find('selVal3').get('v.value');

      let whrFields = [];
      let whrFieldsVals = [];
      whrFields.push('Work_Type__r.Name');
      whrFields.push('Level_1_Description__c');
      whrFields.push('Level_2_Description__c');
      whrFields.push('Level_3_Description__c');

      whrFieldsVals.push(component.get("v.selwtName"));
      whrFieldsVals.push(component.get("v.sellvl1"));
      whrFieldsVals.push(component.get("v.sellvl2"));
      whrFieldsVals.push(sVal);

      helper.populateDropdown(component, "v.lvl4", "Level_4_Description__c", whrFields, whrFieldsVals);

    },
    handleSel4Change : function(component, event, helper) {
      let sVal = component.find('selVal4').get('v.value');
      let whrFields = [];
      let whrFieldsVals = [];
      whrFields.push('Work_Type__r.Name');
      whrFields.push('Level_1_Description__c');
      whrFields.push('Level_2_Description__c');
      whrFields.push('Level_3_Description__c');
      whrFields.push('Level_4_Description__c');

      whrFieldsVals.push(component.get("v.selwtName"));
      whrFieldsVals.push(component.get("v.sellvl1"));
      whrFieldsVals.push(component.get("v.sellvl2"));
      whrFieldsVals.push(component.get("v.sellvl3"));
      whrFieldsVals.push(sVal); 
      helper.populateDropdown(component, "v.lvl5", "Level_5_Description__c", whrFields, whrFieldsVals);
    },
    handleSel5Change : function(component, event, helper) {
      
      helper.getPriceBookEntry(component);
    },
    handleCancel : function(component, event, helper) {
      var message = "WorkOrderLineItem creation cancelled";
      var type = "error";     
      helper.showToast("error", "Error",message, 20000 );        
      $A.get('e.force:refreshView').fire();
    },
    handleTimevalues : function(component, event, helper) {
      helper.adjustTimevalues(component);
    },
    handleChange: function(component, event, helper) {
      if(component.get("v.selectedPricebookEntryLookUpRecord") != undefined){
        var quantity=component.find("quantity").get("v.value");
        var hr = component.find("labourHr").get("v.value");
        var min = component.find("labourMin").get("v.value");            
        var labourAmount = component.get("v.selectedPricebookEntryLookUpRecord").Labour_Price__c;
        var labourMin = parseInt((hr*60))+parseInt(min);
        if(isNaN(((labourAmount*labourMin)/60)*quantity)){
            component.find("labourAmount").set("v.value",0.0);
            component.find("totalAmount").set("v.value",0.0);
        }else{
            component.find("labourAmount").set("v.value",((labourAmount*labourMin)/60)*quantity);
            var totalAmount=((labourAmount*labourMin)/60)*quantity + (component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c*quantity);
            component.find("totalAmount").set("v.value",totalAmount);
        }
        var materialPrice = component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c;
        if(isNaN(materialPrice)){
            component.find("materialAmount").set("v.value",0.0);
        }
      }
    },
    handleQuantityChange: function(component, event, helper) {
      if(component.get("v.selectedPricebookEntryLookUpRecord") != undefined){
        var quantity = component.find("quantity").get("v.value");
        if (quantity > 0){
          var labourTime;
          var hr;
          var min;
          if (component.get("v.selectedPricebookEntryLookUpRecord").Labour_TIME_mins__c == 0){
              hr = component.find("labourHr").get("v.value");
              min = component.find("labourMin").get("v.value");
          }else{
              labourTime = Math.floor(component.get("v.selectedPricebookEntryLookUpRecord").Labour_TIME_mins__c)*quantity;
              var HrMin = (Number(labourTime)/60).toFixed(2);
              hr= parseInt(HrMin);
              min=labourTime-(hr*60)
          }
          console.log('labourTime',labourTime);;
          component.find("labourHr").set("v.value",hr);
          component.find("labourMin").set("v.value",min);
          var labourMin = parseInt((hr*60))+parseInt(min);
          var labourAmount = component.get("v.selectedPricebookEntryLookUpRecord").Labour_Price__c;
          if(isNaN(((labourAmount*labourMin)/60)*quantity)){
              component.find("labourAmount").set("v.value",0.0);
              component.find("totalAmount").set("v.value",0.0);
          }else{
              component.find("labourAmount").set("v.value",((labourAmount*labourMin)/60)*quantity);
              var totalAmount=((labourAmount*labourMin)/60)*quantity + (component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c*quantity);
              component.find("totalAmount").set("v.value",totalAmount);
          }
          var materialPrice = component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c;
          if(isNaN(materialPrice)){
              component.find("materialAmount").set("v.value",0.0);
          }
        }
      }
    },
    clearScreen: function(component, event, helper) {
      console.log('clearScreen fired');
      var objectName = event.getParam("objectName");
      console.log('objectName: ' + objectName)
      if (objectName == 'PricebookEntry'){
        component.find("labourAmount").set("v.value", "0.00");
        component.find("materialAmount").set("v.value", "0.00");
        component.set("v.selectedPricebookEntryLookUpRecord",null);
      }
    },
    overrideCosts : function(component, event, helper) {
      console.log('overrideCosts fired');
      component.find("labourAmount").set("v.value", component.get("v.labourAmount"));
      component.find("materialAmount").set("v.value", component.get("v.materialAmount"));
    },
    handleSaveWoliRecord : function(component, event, helper) {
      helper.saveWoliRecord(component);
    }
})