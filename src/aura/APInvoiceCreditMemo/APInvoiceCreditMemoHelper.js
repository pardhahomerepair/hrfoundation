({
	showToast : function(type, message) {        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": message,
            "type": type,            
            "mode": "sticky",
        });
        toastEvent.fire();        
    }
})