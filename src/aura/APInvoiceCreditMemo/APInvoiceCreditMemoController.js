({
	CreateCreditMemo : function(component, event, helper) {  
        var spinner = component.find("mySpinner");
		$A.util.toggleClass(spinner, "slds-hide");
        
    	var action = component.get("c.createCreditMemo");        
        action.setParams({            
            'supplierNumber' : component.get('v.supplierInvoiceNo'),
            'recordId' : component.get('v.recordId'),
            'creditMemoDetails' : component.get('v.creditMemoDetails')
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                
                if(response.getReturnValue().includes('Success')) {
                    var array = response.getReturnValue().split(":");
                    helper.showToast('success', 'Credit Memo created Successfully');
                    $A.get('e.force:refreshView').fire();
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                      "recordId": array[1]                      
                    });
                    navEvt.fire();

                }
                else {
                    helper.showToast('error', response.getReturnValue());
                    alert(response.getReturnValue());
                }
            }            
            $A.util.toggleClass(spinner, "slds-hide");
        });        
        $A.enqueueAction(action);
	}
})