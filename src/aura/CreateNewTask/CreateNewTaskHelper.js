({
    showToast : function(type, message) {        
          var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
              "message": message,
              "type": type,
              "duration": 100,
              "mode": "dismissible",
          });
          toastEvent.fire();        
      },
      
      //Added By CRMIT TO show atoast msg if no value selcted at authority lookup field
          validateFields: function(component,event){
        var isValid = true;
         // get lookup value 
         var lVal = component.get("v.selectedAuthorities");  
         if(lVal == null){
            isValid = false ;
            //alert('Lookup field is required...Please select contact');
             var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
              "title": "ERROR!",
              "message": "Please select a authority...",
              "type" : 'Error'
          });
          toastEvent.fire();
         }
         return isValid;
  },
  })