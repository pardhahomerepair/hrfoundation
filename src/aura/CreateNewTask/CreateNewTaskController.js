({
    doInit : function(component) {
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set("v.minDate", today);        
        var action = component.get("c.getTaskMatrixList");
        action.setCallback(this, function(response) {
            var taskMatrixList = response.getReturnValue();
            console.log(taskMatrixList);
            component.set("v.taskMatrixList", taskMatrixList);
            var actionUser = component.get("c.getDefaultUser");
            actionUser.setCallback(this, function(response) {
                component.set("v.spinner",false);        
                var getSelectRecord = response.getReturnValue();
                component.set("v.assignUser", getSelectRecord);               
            })
            $A.enqueueAction(actionUser);
        })
        $A.enqueueAction(action);
    },
    handleActivityDateChange : function(component, event, helper) {
        var maxDueDays = component.get("v.maxDueDays");
        var actDate = component.find('activityDate'); 
        if (maxDueDays != ''){
            var today = new Date();
            var activityDate = new Date(component.find('activityDate').get("v.value"));
            var diff = activityDate.getTime() - today.getTime();
            var days = Math.round(Math.abs(diff/(1000*60*60*24)));
            console.log('days ',days);
            console.log('maxDueDays ',maxDueDays);
            if (days > maxDueDays){
                actDate.setCustomValidity('Due date must not be after ' + maxDueDays + ' days');
                actDate.showHelpMessageIfInvalid();
                component.set("v.isValid",false);
            }else{
                actDate.setCustomValidity('');
                actDate.showHelpMessageIfInvalid();
                component.set("v.isValid",true);
            }
        }
    },
    saveTask: function(component, event, helper) {
        //helper.validateFields(component,event) added by CRMIT to validate authority lookup filed
        if (component.get("v.isValid")&& helper.validateFields(component,event)){
            var maxDueDays = component.get("v.maxDueDays");
            var newTask = component.get("v.newTask");
            component.set("v.spinner",true);
            newTask.WhatId=component.get("v.recordId");
            newTask.Authority__c=component.get("v.selectedAuthorities.Id" );
            newTask.ActivityDate=$A.localizationService.formatDate(newTask.ActivityDate, "yyyy-MM-dd");
            newTask.OwnerId=component.get("v.assignUser.Id");
            console.log('Task',JSON.stringify(newTask));
            component.set("v.newTask",newTask);
            var action = component.get("c.createTask");
            action.setParams({
                "task":component.get("v.newTask")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                component.set("v.spinner",false);
                if(component.isValid() && state == "SUCCESS"){
                    console.log('task Id : '+response.getReturnValue());
                    var message = "Task created Successfully";
                    var type = "success";             
                    helper.showToast(type, message);
                    $A.get('e.force:refreshView').fire();
                }else{
                   console.log('error: '+response.getReturnValue());
                   helper.showToast('error', response.getReturnValue());
                }
            })
            $A.enqueueAction(action);
        }
       
    },
    handleCancel : function(component, event, helper) {
        var message = "Task creation cancelled";
        var type = "error";             
        helper.showToast(type, message);
        $A.get('e.force:refreshView').fire();
    },
    handleChange : function(component, event, helper) {
        var actDate = component.find('activityDate');
        actDate.setCustomValidity('');
        actDate.showHelpMessageIfInvalid();
        component.set("v.isValid",true);
        var selectedValue=event.getParam("selectOption");
        var taskMatrixList=component.get("v.taskMatrixList");
        var newTask = component.get("v.newTask");        
        newTask.ActivityDate='';
        newTask.Priority='';
        console.log(taskMatrixList);
        var isNotExist=true;
        for (var i = 0; i < taskMatrixList.length; i++) {
            var taskDueDate = new Date();
            var maxDueDays = new Date();
            if(taskMatrixList[i].Task_Name__c == selectedValue){
				taskDueDate = new Date(taskDueDate.getTime());
    			var day = taskDueDate.getDay();                
   //             if(taskMatrixList[i].Due_Days__c == ''){  // Added another condition to the below IF)
                  if(taskMatrixList[i].Due_Days__c == '' && taskMatrixList[i].Due_Days__c != 0){
                   console.log(taskDueDate.getDate() + 5);
                    taskDueDate.setDate(taskDueDate.getDate() + 5 + (day === 6 ? 2 : +!day) + (Math.floor((5 - 1 + (day % 6 || 1)) / 5) * 2));
                	console.log('bDate',taskDueDate);                 
                }else{
                    console.log('DueDate : ',taskMatrixList[i].Due_Days__c);
                    console.log(taskDueDate.getDate() + taskMatrixList[i].Due_Days__c);
                    
                    console.log(taskDueDate);
                    taskDueDate.setDate(taskDueDate.getDate() + taskMatrixList[i].Due_Days__c + (day === 6 ? 2 : +!day) + (Math.floor((taskMatrixList[i].Due_Days__c - 1 + (day % 6 || 1)) / 5) * 2));
                	console.log('bDate',taskDueDate);
                    
                    if(taskMatrixList[i].Max_Due_Days__c != undefined || taskMatrixList[i].Max_Due_Days__c == ''){
                     	maxDueDays.setDate(maxDueDays.getDate() + taskMatrixList[i].Max_Due_Days__c + (day === 6 ? 2 : +!day) + (Math.floor((taskMatrixList[i].Max_Due_Days__c - 1 + (day % 6 || 1)) / 5) * 2));
                    	var today = new Date();
            			var activityMaxDate = new Date(maxDueDays);
            			var diff = activityMaxDate.getTime() - today.getTime();
            			maxDueDays = Math.round(Math.abs(diff/(1000*60*60*24)));                        
                    }else{
                        maxDueDays=taskMatrixList[i].Max_Due_Days__c;
                    }
                    component.set("v.maxDueDays",maxDueDays);
                }
                var dueDate = $A.localizationService.formatDate(taskDueDate, "yyyy/MM/dd");
                newTask.ActivityDate=dueDate;
                if(taskMatrixList[i].Priority__c == ''){
                    newTask.Priority= 'Normal';
                }else{
                    newTask.Priority= taskMatrixList[i].Priority__c;
                }
                isNotExist=false;
                break;
            }
        }
        //Default due date to today + 5 days and status to Normal if record is 
        //not present in task matrix
        if(isNotExist){
            var taskDueDate = new Date();
            console.log(taskDueDate.getDate());
            taskDueDate = new Date(taskDueDate.getTime());
    		var day = taskDueDate.getDay();
            taskDueDate.setDate(taskDueDate.getDate() + 5 + (day === 6 ? 2 : +!day) + (Math.floor((5 - 1 + (day % 6 || 1)) / 5) * 2));
            var dueDate = $A.localizationService.formatDate(taskDueDate, "yyyy/MM/dd");
            newTask.ActivityDate=dueDate;
            newTask.Priority= 'Normal';
        }
        component.set("v.newTask",newTask);  
    },
})