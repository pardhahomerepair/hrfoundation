trigger ClaimJobExceptionCapture on BatchApexErrorEvent (after insert) {
    Set<Id> asyncApexJobIds = new Set<Id>();
    for(BatchApexErrorEvent evt:Trigger.new){
        asyncApexJobIds.add(evt.AsyncApexJobId);
    }
    
    Map<Id,AsyncApexJob> jobs = new Map<Id,AsyncApexJob>(
        [SELECT id, ApexClass.Name FROM AsyncApexJob WHERE Id IN :asyncApexJobIds]
    );
    List<Claim_Job__c> records = new List<Claim_Job__c>();
    
    List<Claim_Job_Ingestion_Staging__c> cjisList = new List<Claim_Job_Ingestion_Staging__c>();
    for(BatchApexErrorEvent evt:Trigger.new){
        //only handle events for the job(s) we care about
        if(jobs.get(evt.AsyncApexJobId).ApexClass.Name == 'BatchClaimJobIngestionStagingProcess'){
            for (String item : evt.JobScope.split(',')) {
                Claim_Job_Ingestion_Staging__c cjis = new Claim_Job_Ingestion_Staging__c(
                    Id = (Id)item,
                    Batch_Error__c=evt.Message
                );
                cjisList.add(cjis);
            }
        }else  if(jobs.get(evt.AsyncApexJobId).ApexClass.Name == 'OnceOffCreateAuthorityUpdtWO'){
            for (String item : evt.JobScope.split(',')) {
                Claim_Job__c a = new Claim_Job__c(
                    Id = (Id)item /*,
ExceptionType__c = evt.ExceptionType,
Batcherror__c = true,
Errormsg__c=evt.Message*/
                );
                records.add(a);
            }
        }
    }
    if(cjisList.size()>0){
        update cjisList;
    } if(records.size()>0){
        update records;
    }
}