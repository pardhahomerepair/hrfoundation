@isTest
public with sharing class CompletionCallTest {
    @TestSetup
    public static void setupTestData() {
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug('po=======>' +po);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        system.debug('con=======>' +con);

        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        system.debug('======parent case' +cs);

        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        system.debug('wt=======>' +wt);
        
        //Commented by CRMIT -19/02/2019  to create a child case instead of Claim_Job__c object
        //Claim_Job__c cj=HomeRepairTestDataFactory.createClaimJob(cs.Id);
        //Added by CRMIT to create a child case for parent case
         Case childCaseObj = HomeRepairTestDataFactory.createChildClaimAuthority(cs.Id);
         system.debug('======childCase' +childCaseObj);
        
        //Commented by CRMIT - 19/02/2019
        //WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair',cs.id,wt.id,cj.id);
        //Added by CRMIT for create workorders for case and child case
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrdersWithCase('Home Repair',cs.id,wt.id,childCaseObj.id);
        system.debug('======wo' +wo);

        //AP_Invoice__c invoice = HomeRepairTestDataFactory.createAPInvoiceWithWorkOrder(wo.id);
    }
    public static testMethod void dncJobTest() {
        WorkOrder wo = [SELECT Id,Authority__c FROM WorkOrder LIMIT 1 ];
        system.debug('======wo inside--->' +wo);

        Case job = [SELECT Id,Case.ParentId FROM Case Where Job_Type__c = 'doAndCharge' AND ParentID != Null limit 1];
       	system.debug('======job inside--->' +job);
        
        wo.Status = NJ_CONSTANTS.WO_STATUS_JOBCOMPLETE;
        //Added By CRMIT to replace Claim_Job__c field with Authority__c
		wo.Authority__c = job.Id;
        update wo;
        
        Test.startTest();
        
            TaskService.createCompletionCallTask(new set<Id>{job.Id});
        	//Added BY CRMIT 
        	Task t = [SELECT Id FROM Task WHERE Subject =: NJ_Constants.TASK_COMPLETION_CALL_SUBJECT];
            t.status = 'Completed';
            t.Case_Comments_Added__c = true;
            update t; 
            
            AP_Invoice__c invoice = HomeRepairTestDataFactory.createAPInvoiceWithWorkOrder(wo.id);
            system.debug('======invoice inside--->' +invoice);

            invoice.Status__c = NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED;
            update invoice;
            system.debug('======invoice inside after update--->' +invoice);

            AP_SOW__c sow = new AP_SOW__c(Prepayment_Amount__c = 100, Quantity__c = 5, Scope_Of_Work__c = 'Scope', UOM__c = 'UOM__c', AP_Invoice__c = invoice.Id);
            insert sow;
            system.debug('======sow inside --->' +sow);

        	//Added By CRMIT 
        	TaskService.invokeARGeneration(new set<Id>{job.Id});
            TaskService.createInvoiceRecord(new set<Id>{job.Id});
            
            aR_Invoice__c inv = [select id,Authority__c from AR_Invoice__c];
            system.debug('======inv--->' +inv);
        
        	//User U = HomeRepairTestDataFactory.createSystemAdminUser('Testing','User');
            //system.debug('======userrrrrr--->' +u);
            // System.runAs(u)
            
        	//Added/udated by CRMIT 
            TaskService.createManualAR(job.Id); 
            inv.Status__c = NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED;
        	inv.Invoice_Type__c =  NJ_CONSTANTS.INVOICE_CREDIT_MEMO_CUSTOMER;
            inv.Rejection_Reason__c = 'reason';
            update inv;
        
            TaskService.createManualAR(job.Id);
            delete inv;
            delete wo;
            job.Job_Type__c = NJ_CONSTANTS.JOB_TYPE_DOANDCHARGE;
            update job;
            TaskService.invokeARGenerationFromAP(new list<Id>{job.Id});
        	Test.stopTest();
    }
    public static testMethod void approveInvoiceTest() {
        WorkOrder wo = [SELECT Id FROM WorkOrder LIMIT 1 ];
        wo.Status = NJ_CONSTANTS.WO_STATUS_JOBCOMPLETE;
        update wo;
        AP_Invoice__c invoice = HomeRepairTestDataFactory.createAPInvoiceWithWorkOrder(wo.id);
        AP_SOW__c sow = new AP_SOW__c(Prepayment_Amount__c = 100, Quantity__c = 5, Scope_Of_Work__c = 'Scope', UOM__c = 'UOM__c', AP_Invoice__c = invoice.Id);
        insert sow;
        
        Test.startTest();
       	// User U = HomeRepairTestDataFactory.createSystemAdminUser('Testing','User');
        // system.debug('======userrrrrr--->' +u);
        // System.runAs(u) 
        invoice.Status__c = 'Approved';
        update invoice;
          
        Test.stopTest();    
    }
  /* public static testMethod void approveInvoiceTaskTest() {
        WorkOrder wo = [SELECT Id FROM WorkOrder LIMIT 1 ];
        wo.Status = NJ_CONSTANTS.WO_STATUS_JOBCOMPLETE;        
        AP_Invoice__c invoice = HomeRepairTestDataFactory.createAPInvoiceWithWorkOrder(wo.id);
        AP_SOW__c sow = new AP_SOW__c(Prepayment_Amount__c = 100, Quantity__c = 5, Scope_Of_Work__c = 'Scope', UOM__c = 'UOM__c', AP_Invoice__c = invoice.Id);
        insert sow;        
        update wo;
        Claim_Job__c job = [SELECT Id FROM Claim_Job__c limit 1];
        TaskService.createCompletionCallTask(new set<Id>{job.Id});
            
        Test.startTest();
            invoice.Status__c = 'Approved';
            invoice.Work_Order__c = wo.Id;
            update invoice;            
            
            Task t = [SELECT Id FROM Task WHERE Subject =: NJ_Constants.TASK_COMPLETION_CALL_SUBJECT];
            t.status = 'Completed';
            t.Case_Comments_Added__c = true;
            update t;
            
            wo.Status = 'Closed';
            wo.Claim_Job__c = job.Id;
            update wo;
            TaskService.invokeARGeneration(new set<Id>{job.Id});
        Test.stopTest();    
    }*/
}