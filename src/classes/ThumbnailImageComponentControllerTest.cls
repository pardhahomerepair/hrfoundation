/* =====================================================================================
Type:       Test class
Purpose:    Test cases for ThumbnailImageComponentController
========================================================================================*/
@isTest
private class ThumbnailImageComponentControllerTest{
    @testSetup 
    static void setup(){
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);                
        HomeRepairTestDataFactory.addContentVersionToParent(cs.id);      
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Assessment');
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);
        
    }   
    static testMethod void ThumbnailImageComponentControllerTest() {
        WorkOrder wo=[SELECT id FROM WorkOrder LIMIT 1];
        ThumbnailImageComponentController.getAllThumbnails(wo.Id);
        
    }
    
}