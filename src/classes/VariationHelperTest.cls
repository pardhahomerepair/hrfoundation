/***************************************************************** 
Purpose: TEST CLASS FOR VARIATIONHELPER AND VariationHandler
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Nikhil                       Created      Home Repair Claim System  
*******************************************************************/

@isTest
public class VariationHelperTest {
    

/***************************************************************** 
Purpose: METHOD TO CREATE TEST DATA
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Nikhil                       Created      Home Repair Claim System  
*******************************************************************/
    
    @TestSetup
    static void TestdataCreateMethod() {
        final Id hrClaimRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();
        Home_Repairs_Trigger_Switch__c switchVar = new Home_Repairs_Trigger_Switch__c();
        switchVar.Trigger_On_Variation__c = true;
        insert switchVar;
        
        Account acc = HomeRepairTestDataFactory.createAccounts('Test WorkOrder Account')[0];
        
        contact conVar = HomeRepairTestDataFactory.createTradeContact('test WO Contatct',acc.Id);
        
        //Case caseVar   = HomeRepairTestDataFactory.createCase();
        //create policy
        policy__c testPolicy = homeRepairTestDataFactory.createPolicy();
        //System.debug('policy: ' + testPolicy);
        
        //create case associated to policy
        case caseVar = new Case();
        caseVar.Origin = 'Web';
        caseVar.Status = 'Suncorp';
        caseVar.Priority = 'Medium';        
        caseVar.Policy__c = testPolicy.id;
        caseVar.RecordTypeId=hrClaimRecId;
        //caseVar.Policy__r.State__c = 'VIC';
        caseVar.Total_Price__c = 200.00;
        insert caseVar;
		
        
        //Commented by CRMIT
        //Added BY CRMIT
        case childAuthority = HomeRepairTestDataFactory.createChildClaimAuthority(caseVar.id);

        /*Claim_Job__c testClaimJob = new Claim_Job__c();
        testClaimJob.Claim__c = caseVar.id;
        testClaimJob.Job_Type__c = 'doAndCharge';
        insert testClaimJob; */
        
        WorkType wot = HomeRepairTestDataFactory.createWorkType('Repair Items','Test Work type');
        
        Id RecordTypeId = [SELECT Id from Recordtype WHERE developername = 'Home_Repair'].Id;
        String RecordTypeIdName = [SELECT Name from Recordtype WHERE developername = 'Home_Repair'].Name;

        
        WorkOrder wo = HomeRepairTestDataFactory.createWorkOrdersWithCase(RecordTypeIdName,caseVar.Id,wot.Id,childAuthority.Id);
        wo.Status = 'In Progress';
        update wo;
        
        Variation__c var = new Variation__c(Work_Order__c = wo.Id 
                                            ,Variation_Reason__c = 'Additional Scope'
                                            ,Additional_Labour_Amount__c=1
                                            ,Additional_Labour_Time_Hours__c = 1 
                                            , Additional_Materials_Amount__c = 1
                                            , Variation_Details__c = 'test details',Variation_Status__c = 'Requested');
        insert var;
                       
        
    }


/***************************************************************** 
Purpose: METHOD TO CREATE VARIATION FOR TRIGGER CONDITION
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Nikhil                       Created      Home Repair Claim System  
*******************************************************************/
    
    private static testMethod void testTriggerInsert(){
    
        test.startTest();
            Id wo = [SELECT Id FROM WorkOrder].Id;
            Variation__c var = new Variation__c(Work_Order__c = wo 
                                                , Variation_Reason__c = 'Additional Scope'
                                                ,Additional_Labour_Amount__c=1
                                                ,Additional_Labour_Time_Hours__c = 1 
                                                , Additional_Materials_Amount__c = 1
                                                , Variation_Details__c = 'test details');
            insert var;
            
            
            workOrder wordr = [SELECT Id,Recordtype.DeveloperName FROM WorkOrder WHERE Id =: wo];
         
            Variation__c varRec = [SELECT Id,Recordtype.DeveloperName FROM Variation__c WHERE Id =: var.Id];
            //checking recordType
            //System.assertEquals(NJ_Constants.WORKORDER_LOCKED_RT_DEVELOPERNAME, wordr.Recordtype.DeveloperName);
        test.stopTest();
    
    }


/***************************************************************** 
Purpose: METHOD TO UPDATE VARIATION FOR TRIGGER CONDITION
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Nikhil                       Created      Home Repair Claim System  
*******************************************************************/
    
    private static testMethod void testTriggerUpdate(){
    
        test.startTest();
            Variation__c var = [SELECT Id,Variation_Status__c FROM Variation__c];
        	var.Variation_Status__c = 'Approved';
            update var;
           
            //checking size of list Of WorkOrderLineItem created
            //System.assertEquals([SELECT Id FROM WorkOrderLineItem].Size(), 1);
            //System.assertEquals([SELECT Id FROM ServiceAppointment].Size(), 1);
        test.stopTest();
        
    }
    
}