/**
 * @File Name          : GenerateDistributionSetsTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 4/8/2019, 3:01:40 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    4/8/2019, 3:01:25 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest(SeeAllData=false)
public without sharing class GenerateDistributionSetsTest {
     public static final Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
        public static final Id hrClaimRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();

    @isTest static void testDistributionSetGeneration(){
        //create policy
        policy__c testPolicy = homeRepairTestDataFactory.createPolicy();
        //System.debug('policy: ' + testPolicy);
        
        //create case associated to policy
        case testCase = new Case();
        testCase.Origin = 'Web';
        testCase.Status = 'Suncorp';
        testCase.Priority = 'Medium';        
        testCase.Policy__c = testPolicy.id;
        //Added by CRMIT defalut recordtype
       	testCase.RecordTypeId = hrClaimRecId;
        //testCase.Policy__r.State__c = 'VIC';
        testCase.Total_Price__c = 200.00;
        insert testCase;
        //System.debug('testCase: ' + testCase);
        
        String caseID = testCase.id;
       // Added by CRMIT to create authority case to parent case via HomeRepairTestDataFactory
        case childCase = HomeRepairTestDataFactory.createClaimAuthority(caseID);
        
        //Commented BY CRMIT
        //AuthorityCase job associated to claim
        //Case testChildCase = new Case();
        //testChildCase.ParentId = caseID;
        //testChildCase.Job_Type__c = 'doAndCharge'
        //insert testChildCase;
        //create service resource Account relatable to work order
        Account testAccount = new Account();
        //business details
        testAccount.Name = 'testAccount';
        testAccount.ABN__c = '1234';
        testAccount.ABN_Status__c = 'Active';
        testAccount.GST_Status__c = 'Active';
        testAccount.Phone = '0404040404';
        testAccount.When_did_your_business_commence_trading__c = date.newInstance(2011,11,11);
        testAccount.Business_Structure__c = 'sample structure';
        testAccount.RCTI__c = true;
        //business work type
        testAccount.Work_Type__c = 'CabinetMaker';
        testAccount.Service_Areas__c = 'VIC';
        testAccount.Jobs_Per_Week__c = 40;
        //insurance
        testAccount.Public_Liability_Insurance_Provider__c = 'sample insurer';
        testAccount.Public_Liability_Cover_Amount__c = 100.00;
        testAccount.Public_Liability_Expiry__c = date.newInstance(3000, 3, 3);
        testAccount.Work_Cover_Amount__c = 100.00;
        testAccount.Work_Cover_Expiry__c = date.newInstance(3000, 3, 3);
        testAccount.Work_Cover_Insurance_Provider__c = 'sample insurer';
        testAccount.Work_Cover_Insurance_State__c = 'VIC';
        //safe work agreement
        testAccount.Safe_Work_Method_Statement_Agreement__c = 'Yes';
        //bank details
        testAccount.Bank_Account_Name__c = 'sample bank';
        testAccount.BSB__c = '123123';
        testAccount.Account_Number__c = '22221111';
        //accounts contact
        testAccount.Accounts_First_Name__c = 'john';
        testaccount.Accounts_Last_Name__c = 'tron';
        testAccount.Accounts_Phone__c = '2424242424';
        testAccount.Accounts_Email_Address__c = 'jtron@fakemail.com';
        testAccount.AC_Position_in_Company__c = 'cc';
        //jobs related contact
        testAccount.Job_Related_First_Name__c = 'john';
        testAccount.Job_Related_Last_Name__c = 'Gong';
        testAccount.Job_Related_Phone__c = '4545454545';
        testAccount.Job_Related_Position_in_Company__c = 'ff';
        testAccount.Job_Email_Address__c = 'jgong@fakemail.com';
        //License, Authorities, certs and rego's
        testAccount.Any_previous_complaints__c = 'No';
        //other work
        testAccount.Any_current_work_orders__c = 'No';
        testAccount.Any_previous_work__c = 'No';
        //referred?
        testAccount.Referred__c = 'No';
        
        insert testAccount;
        //system.debug('testAccount: ' + testAccount);
        RecordType rt = [SELECT Id FROM RecordType WHERE Name = 'Repair Items' limit 1];
        WorkType workStatusObj = new WorkType(Name = 'Cabinetmaker', EstimatedDuration = 4, DurationType = 'Hours');//[SELECT Id FROM WorkType where Name = 'Cabinetmaker' limit 1];
        insert workStatusObj;
       // String workTypeID = '08qN00000008OTB';//Cabinetmaker
        
        //generate AP workOrder
        workOrder testWo = new workOrder();
        //Updated By CRMIT testWo.Authority__c instead of testWo.Claim_Job__c
        testWo.Authority__c = childCase.id;
        testWo.Subject = 'repair';
        testWo.WorkTypeid = workStatusObj.Id;
        testWo.CaseId = caseID;
        testWo.Customer_Invoice_Ref__c = 'INVREF1234';
        //testWo.RCTI__c = true;
        testWo.Call_Invoice_Gen_Srvc_for_Non_RCTI__c = true;
        //testWo.Service_Resource__c = testResource.id;
        testWo.Service_Resource_Company__c = testAccount.id;
        testWo.Status = 'Job Complete';
        insert testWo;
        //system.debug('testWo: ' + testWo);

        //generate gst chargeable products for woli's
        Product2 testProduct = new Product2();
        
        testProduct.Name = 'testProduct';
        testProduct.GST_Exempt_Labour__c = false;
        testProduct.GST_Exempt_Material__c = false;
        insert testProduct;
        //system.debug('testProduct: ' + testProduct);
        
        //generate woli's associated to workOrder and product
        workOrderLineItem testWOLI = new workOrderLineItem();
        //testWOLI.PricebookEntryId = testProduct.id;
        testWOLI.WorkOrderId = testWo.Id;
        testWOLI.WorkTypeId = workStatusObj.Id;
        testWOLI.Description = 'abc';
        testWOLI.Site_Visit_Number__c = 1;
        testWOLI.Labour_Rate__c = 100.00;
        testWOLI.Labour_Time__c = 1.00;
        testWOLI.Material_Rate__c = 100.00;
        //testWOLI.Quantity = 1;
        testWOLI.Status = 'closed';
        insert testWOLI;
        
        //system.debug('testWOLI: ' + testWOLI);
        //calculate for a GST value above zero
        
        //get work order 
        workOrder x = [SELECT id, LineItemCount, Do_Not_Invoice_AP__c, Work_Type_Name__c,
                                         Internal_Resource__c, Invoice_Type__c, Status, SupplierInvCheck__c, 
                                         RCTI__c, Call_Invoice_Gen_Srvc_for_Non_RCTI__c
                                  FROM workOrder
                                  WHERE id = :testWo.id];
                
        test.startTest();
        AP_Invoice__c testAPInv = homeRepairTestDataFactory.createAPInvoiceWithWorkOrder(x.Id);
        test.stopTest();
        system.debug('testAPInv: ' + testAPInv);
        
        AP_Invoice__c Inv = [SELECT id, Labour_Distribution_Set__c, Material_Distribution_Set__c, GST_Distribution_Set__c
                             FROM AP_Invoice__c
                             WHERE id = :testAPInv.id];
        system.debug('Inv: ' + Inv);
        
        system.debug('DisSet Labour: ' + Inv.Labour_Distribution_Set__c);
        system.debug('DisSet Material: ' + Inv.Material_Distribution_Set__c);
        system.debug('DisSet GST: ' + Inv.GST_Distribution_Set__c);
        
        String expectedLabourResult = ('11.303.51300.220.000');
    }
}