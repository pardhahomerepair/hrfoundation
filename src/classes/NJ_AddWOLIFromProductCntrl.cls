/**
 * Controller class for NJ_AddWOLIFromProduct component
 * @Author : Nikhil Jaitly
 * @CreatedDate : 25/06/2019
 */
public with sharing class NJ_AddWOLIFromProductCntrl {
  public class AddWoliInfo {
    @AuraEnabled
    public List<NJ_SelectOptionLightning> workTypes;
    @AuraEnabled
    public List<NJ_SelectOptionLightning> lvl1;
    @AuraEnabled
    public List<NJ_SelectOptionLightning> lvl2;
    @AuraEnabled
    public List<NJ_SelectOptionLightning> lvl3;
    @AuraEnabled
    public List<NJ_SelectOptionLightning> lvl4;
    @AuraEnabled
    public List<NJ_SelectOptionLightning> lvl5;
    @AuraEnabled
    public WorkOrder woInfo;
    @AuraEnabled
    public PricebookEntry pbe;
    public AddWoliInfo() {
      workTypes = new List<NJ_SelectOptionLightning>();
    }
  }
  /**
   * This method is used to initialize add WOLI from product component
   * @Author : Nikhil Jaitly
   * @CreatedDate : 26/06/2019
   */
  @AuraEnabled
  public static AddWoliInfo initAddWOLIFromProduct(String woId){
    AddWoliInfo awi = new AddWoliInfo();
    for(WorkType wt : [SELECT Id, Name
                       FROM WorkType
                       ]) {
      awi.workTypes.add(new NJ_SelectOptionLightning(wt.Name, wt.Name));
    } 
    for(WorkOrder wo : [SELECT Id, Status, Authority__r.Status,Case.Status,Case.Assessment_Count__c,RecordLocked__c, WorkType.Name,
                               Case.State1__c
                        FROM WorkOrder
                        WHERE Id = :woId
                        LIMIT 1]) {
      awi.woInfo = wo;
    }
    if(awi.woInfo.WorkType.Name != null) {
      awi.lvl1 = getProductDropdown('Level_1_Description__c', new List<String> { 'Work_Type__r.Name' }, new List<String> { awi.woInfo.WorkType.Name });
    }
    /*if(awi.lvl1 != null && !awi.lvl1.isEmpty()) {
      awi.lvl2 = getProductDropdown('Level_2_Description__c', 'Level_1_Description__c', awi.lvl1[0].value);
    }
    if(awi.lvl2 != null && !awi.lvl2.isEmpty()) {
      awi.lvl3 = getProductDropdown('Level_3_Description__c', 'Level_2_Description__c', awi.lvl2[0].value);
    }
    if(awi.lvl3 != null && !awi.lvl3.isEmpty()) {
      awi.lvl4 = getProductDropdown('Level_4_Description__c', 'Level_3_Description__c', awi.lvl3[0].value);
    }
    if(awi.lvl4 != null && !awi.lvl4.isEmpty()) {
      awi.lvl5 = getProductDropdown('Level_5_Description__c', 'Level_4_Description__c', awi.lvl4[0].value);
    }
    if(awi.woInfo.WorkType.Name != null && !awi.lvl1.isEmpty() && !awi.lvl2.isEmpty() && !awi.lvl3.isEmpty() &&
       !awi.lvl4.isEmpty() && !awi.lvl5.isEmpty()) {
      awi.pbe = getPriceBookEntry(awi.woInfo.WorkType.Name, awi.woInfo.Case.State1__c, awi.lvl1[0].value, awi.lvl2[0].value, awi.lvl3[0].value, 
                                awi.lvl4[0].value, awi.lvl5[0].value);
    }*/
    return awi;
  }
  /**
   * This method is used to getProductDropdown
   * @Author : Nikhil Jaitly
   * @CreatedDate : 26/06/2019
   */
  @AuraEnabled
  public static List<NJ_SelectOptionLightning> getProductDropdown(String selField, List<String> whrFields, List<String> whrFieldValues){
    List<NJ_SelectOptionLightning> values = new List<NJ_SelectOptionLightning>();
    String query = '';
    query += ' SELECT ' + selField;
    query += ' FROM Product2 ';
    query += ' WHERE Id != null ';
    Integer cnt = 0;
    for(String whr : whrFields) {
      query += ' AND ' + whr + ' = \'' + whrFieldValues.get(cnt++) + '\'';
    }
    System.debug('Nikhil query '+query);
    Set<String> fieldValues = new Set<String>();
    for(sObject obj : Database.query(query)) {
      fieldValues.add(String.valueOf(obj.get(selField)));
    }
    values.add(new NJ_SelectOptionLightning('None', 'None', false, true));
    for(String val : fieldValues) {
      values.add(new NJ_SelectOptionLightning(val, val));
    }
    return values;
  }
  @AuraEnabled 
    public static string createWorkOrderLineItem(WorkOrderLineItem woli){ //changed from void to string
      Savepoint sp; 
      try{
        sp = Database.setSavepoint();
        List <WorkOrderLineItem> returnList = new List <WorkOrderLineItem> ();        
        List<PricebookEntry> pricebookEntryList = [Select ID,Product2Id
                                                    FROM PricebookEntry 
                                                    WHERE Id =: woli.PricebookEntryId
                                                  ];
        
        List<PricebookEntry> pbeList = [Select ID,Product2Id
                                        FROM PricebookEntry 
                                        WHERE Product2Id =: pricebookEntryList[0].Product2Id AND
                                        Pricebook2.Name like 'Standard Price Book' AND IsActive = true
                                        ];
        WorkOrder wo = [SELECT Id,WorkType.Name,Rectification__c,WorkTypeId 
                        FROM WorkOrder WHERE Id = :woli.WorkOrderId 
                        LIMIT 1];
        woli.WorkTypeId = wo.WorkTypeId;
        if (wo.Rectification__c == true){
            woli.RecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Rectification').getRecordTypeId();               
        }else{
            woli.RecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Repair Items').getRecordTypeId();
        }
        if(!pbeList.isEmpty())
            woli.PricebookEntryId = pbeList[0].Id;     
        insert woli;
        returnList = [Select ID, LineItemNumber FROM WorkOrderLineItem 
                      WHERE Id =: woli.Id
                      ]; 
        
        return returnList[0].LineItemNumber;
      }
      catch (System.DmlException e) {
          Database.rollback(sp);
          system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
          List<String> errorMessageList = new List<String>();
          errorMessageList.add(e.getDmlMessage(0));            
          String errorMessage = String.join(errorMessageList,',');
          return errorMessage;
      }
      catch (Exception e) {
          Database.rollback(sp);
          system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
          return e.getMessage();
      } 
    }   
    /*
  @AuraEnabled 
    public static string createWorkOrderLineItem(String woTypeName, String lvl1, String lvl2, String lvl3, 
                                                 String lvl4, String lvl5,WorkOrderLineItem woli){ 
      Savepoint sp; 
      try{
        sp = Database.setSavepoint();
        List <WorkOrderLineItem> returnList = new List <WorkOrderLineItem> (); 
              
        WorkOrder wo=[SELECT id, Case.State1__c,WorkType.Name,Rectification__c,WorkTypeId 
                      FROM WorkOrder 
                      WHERE Id=:woli.WorkOrderId limit 1];
        woli.WorkTypeId=wo.WorkTypeId;
        if (wo.Rectification__c == true){
            woli.RecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Rectification').getRecordTypeId();               
        }else{
            woli.RecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Repair Items').getRecordTypeId();
        }
        PricebookEntry pbe = getPriceBookEntry(woTypeName,wo.Case.State1__c, lvl1, lvl2, lvl3, lvl4, lvl5);
        if(pbe != null) {
          woli.PricebookEntryId = pbe.Id;
        }
        insert woli;
        returnList = [Select ID, LineItemNumber FROM WorkOrderLineItem 
                      WHERE Id =: woli.Id
                      ]; 
        
        return returnList[0].LineItemNumber;
      }
      catch (System.DmlException e) {
        Database.rollback(sp);
        system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
        List<String> errorMessageList = new List<String>();
        errorMessageList.add(e.getDmlMessage(0));            
        String errorMessage = String.join(errorMessageList,',');
        return errorMessage;
      }
      catch (Exception e) {
        Database.rollback(sp);
        system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
        return e.getMessage();
      }
    }  */
    @AuraEnabled
    public static PricebookEntry getPriceBookEntry(String woTypeName, String state, String lvl1, String lvl2, String lvl3, 
                                                    String lvl4, String lvl5) {
      System.debug(' woTypeName ' + woTypeName + ' state ' + state + ' lvl1 ' + lvl1 + ' lvl2 ' + lvl2 );
      System.debug(' lvl3 ' + lvl3 + ' lvl4 ' + lvl4 + ' lvl5 ' + lvl5);

      String pbe;
      String prodId;

      String query = '';
      query += ' SELECT Id ';
      query += ' FROM Product2 ';
      query += ' WHERE Work_Type__r.Name = \'' + woTypeName + '\'';
       if(lvl1 != null) {
        query += ' AND Level_1_Description__c = \'' + lvl1 +'\'';
      }
     if(lvl2 != null) {
        query += ' AND Level_2_Description__c = \'' + lvl2 + '\'';
      }
      if(lvl3 != null) {
        query += ' AND Level_3_Description__c = \'' + lvl3 + '\'';
      }
      if(lvl4 != null) {
        query += ' AND Level_4_Description__c = \'' + lvl4 + '\'' ;
      }
      if(lvl5 != null) {
        query += ' AND Level_5_Description__c = \'' + lvl5 + '\'';
      }
      query += ' LIMIT 1 '; 
      System.debug('query '+query);
      for(Product2 prod : Database.query(query)) {
        prodId = prod.Id;
      }
      System.debug('prodId '+prodId);
      List<PricebookEntry> pbeList = [SELECT Id, Name, Product2.Name, Product2Id, Product2.UOM__c, Material_Price__c, 
                                             Labour_TIME_mins__c, Labour_Price__c
                                      FROM PricebookEntry 
                                      WHERE Product2Id = :prodId 
                                      AND Satte__c = :state 
                                      AND IsActive = true
                                      ];     
      System.debug('Nikhil pbeList '+pbeList);
      if(!pbeList.isEmpty()) {
        return pbeList[0];
      } else {
        return null;
      }                           
    }
}