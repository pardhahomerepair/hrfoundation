@isTest
public class hrTradeWOAuthCaptureProcessorTest {

   @TestSetup
    public static void setupTestData() {
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        
               
       // WorkType wtMSAsbestos=HomeRepairTestDataFactory.createWorkType('Repair Items','MS - Asbestos');
       // WorkOrder woAsbestos=HomeRepairTestDataFactory.createMakeSafeAuthWO('Home Repair Trades',cs.id,wtMSAsbestos.id,cjms.id); 
         
    }
  
    public static testMethod void testReportQuoteWOAuthority() {
        Case cs=[select Id from case limit 1];
        case cj=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Engineer');
        HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,cj.id);  
        WorkOrder WORQ = [SELECT Id,status,Work_Order_Authority__c FROM Workorder Limit 1];
         
        Test.startTest();
            woRQ.Work_Order_Authority__c='Report and Quote';
            update woRQ;
        Test.stopTest();        
    }
    
    public static testMethod void testMakeSafeGeneralWOAuthority() {
        WorkType wtMakeSafe=HomeRepairTestDataFactory.createWorkType('MakeSafe Items','MS - Electrician');
        Case cs=[select Id from case limit 1];
        case cjms=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        Test.startTest();
        cjms.Job_Type__c = 'makeSafe';
        update cjms;
            HomeRepairTestDataFactory.createMakeSafeAuthWO('HomeRepair MakeSafe',cs.id,wtMakeSafe.id,cjms.id);  
        Test.stopTest();        
    }
    
     public static testMethod void testMakeSafeAsbestosWOAuthority() {
        WorkType wtMakeSafe=HomeRepairTestDataFactory.createWorkType('MakeSafe Items','MS - Asbestos');
        Case cs=[select Id from case limit 1];
        case cjms=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        Test.startTest();
         cjms.Job_Type__c = 'makeSafe';
        update cjms;
            HomeRepairTestDataFactory.createMakeSafeAuthWO('HomeRepair MakeSafe',cs.id,wtMakeSafe.id,cjms.id);  
        Test.stopTest();        
    }
    
     public static testMethod void testMakeSafeTreeLooperWOAuthority() {
        WorkType wtMakeSafe=HomeRepairTestDataFactory.createWorkType('MakeSafe Items','MS - Tree Lopper');
        Case cs=[select Id from case limit 1];
        case cjms=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        Test.startTest();
         cjms.Job_Type__c = 'makeSafe';
        update cjms;
           HomeRepairTestDataFactory.createMakeSafeAuthWO('HomeRepair MakeSafe',cs.id,wtMakeSafe.id,cjms.id);  
        Test.stopTest();        
    }
    
    public static testMethod void testReportQuoteRooferMetalWOAuthority() {
        WorkType wtRooferMetal=HomeRepairTestDataFactory.createWorkType('Repair Items','Roofer - Metal');
        Case cs=[select Id from case limit 1];
        case cj=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wtRooferMetal.id,cj.id);  
        WorkOrder WORoofMetal = [SELECT Id,status,Work_Order_Authority__c FROM Workorder Limit 1];
         
        
        Test.startTest();
            WORoofMetal.Work_Order_Authority__c='Report and Quote';
            update WORoofMetal;
        Test.stopTest();        
    }
    
    public static testMethod void testReportQuoteRooferTilesWOAuthority() {
        WorkType wtRooferTiles=HomeRepairTestDataFactory.createWorkType('Repair Items','Roofer-Tiles');
        Case cs=[select Id from case limit 1];
        case cj=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wtRooferTiles.id,cj.id);  
        WorkOrder WORoofTiles = [SELECT Id,status,Work_Order_Authority__c FROM Workorder Limit 1];
         
        
        Test.startTest();
            WORoofTiles.Work_Order_Authority__c='Report and Quote';
            update WORoofTiles;
        Test.stopTest();        
    }
    
    public static testMethod void testReportQuoteRooferPCarbonateWOAuthority() {
        WorkType wtPCarbonate=HomeRepairTestDataFactory.createWorkType('Repair Items','Roofer - Polycarbonate');
        Case cs=[select Id from case limit 1];
       case cj=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wtPCarbonate.id,cj.id);  
        WorkOrder WOPCarbonate = [SELECT Id,status,Work_Order_Authority__c FROM Workorder Limit 1];
         
        
        Test.startTest();
            WOPCarbonate.Work_Order_Authority__c='Report and Quote';
            update WOPCarbonate;
        Test.stopTest();        
    }
    
    public static testMethod void testReportQuoteGeneralWOAuthority() {
        WorkType wtGeneral=HomeRepairTestDataFactory.createWorkType('Repair Items','Electrician');
        Case cs=[select Id from case limit 1];
        case cj=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wtGeneral.id,cj.id);  
        WorkOrder woGeneral = [SELECT Id,status,Work_Order_Authority__c FROM Workorder Limit 1];
         
        
        Test.startTest();
            woGeneral.Work_Order_Authority__c='Report and Quote';
            update woGeneral;
        Test.stopTest();        
    }
    
    public static testMethod void testReportQuoteLeakDetectionWOAuthority() {
        WorkType wtLeakDetection=HomeRepairTestDataFactory.createWorkType('Repair Items','Leak Detection');
        Case cs=[select Id from case limit 1];
        case cj=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wtLeakDetection.id,cj.id);  
        WorkOrder WOLeakDetection = [SELECT Id,status,Work_Order_Authority__c FROM Workorder Limit 1];
         
        
        Test.startTest();
            WOLeakDetection.Work_Order_Authority__c='Report and Quote';
            update WOLeakDetection;
        Test.stopTest();        
    }
    
    
     
}