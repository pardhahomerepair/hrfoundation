/**
 * Class TaskTriggerHandler
 * Author: Jitendra Kawale
 * Trigger Handler for the Task SObject. This class implements the ITrigger
 * interface to help ensure the trigger code is bulkified and all in one place.
 */
public with sharing class TaskTriggerHandler
    implements ITrigger{    
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        if(Trigger.isInsert) {
            set<Id> caseId = new set<Id>();            
            for(Sobject tsk : Trigger.New) {                
                system.debug('T==>'+tsk);
                Task T = (Task)tsk;
                if(T.WhatId != null && String.valueOf(T.WhatId).left(3) == '500' && T.get('Subject') != NJ_CONSTANTS.TASK_FOLLOWER_SUBJECT_CLOSED)
                    caseId.add((Id)T.get('WhatId'));  
                }
                    
            
            system.debug('caseId==>'+caseId);
            Map<Id, Case> cases = new Map<Id, Case>([Select Id, Status FROM Case WHERE ID IN: caseId and Status =: NJ_CONSTANTS.CASE_STATUS_CLOSED]);
           /* for(Sobject T : Trigger.New) {
                Task tsk = (Task)T;
                if(cases.containsKey(tsk.WhatId))
                    tsk.addError('You Cannot add tasks to closed claim');
             }*/
             
        }
    }
    
    public void bulkAfter()
    {
        if(Trigger.isUpdate) {            
            set<Id> completionCallOrders = new set<Id>();
            for(Sobject tsk : Trigger.New) {                                
                Task T = (Task)tsk;                
                system.debug('Claim_Job__c==> '+T.Authority__c);
                //create AR Invoice On completion call task closure 
                if((T.Subject == NJ_CONSTANTS.TASK_COMPLETION_CALL_SUBJECT || T.Subject == NJ_CONSTANTS.FINAL_CONTACT_CALL_SUBJECT) && T.Status == NJ_CONSTANTS.TASK_STATUS_COMPLETED ) {
                    system.debug('Inside==');
                    completionCallOrders.add((Id)T.get('Authority__c')); 
                }
                    
            }            
            system.debug('completionCallOrders==>'+completionCallOrders);
            if(!completionCallOrders.isEmpty()) {
                TaskService.invokeARGeneration(completionCallOrders);
            }
        }
    }
        
    public void beforeInsert(SObject so)
    {
    }
    
    public void beforeUpdate(SObject oldSo, SObject so)
    {
    }
    
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so)
    {   
        
    }
    public void beforeUnDelete(SObject so)
    {   
        
    }
    public void afterInsert(SObject so)
    {
    }
    
    public void afterUpdate(SObject oldSo, SObject so)
    {
    }
    
    public void afterDelete(SObject so)
    {
    }
     public void afterUnDelete(SObject so)
    {
    }
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this 
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        
    }
}