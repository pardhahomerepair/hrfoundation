/**
 * Class APInvoiceHandler
 * Author: Jitendra Kawale
 * Trigger Handler for the AP_Invoice__c SObject. This class implements the ITrigger
 * interface to help ensure the trigger code is bulkified and all in one place.
 */
public with sharing class APInvoiceHandler
    implements ITrigger{ 
        //Added by CRMIT
    public static final Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        List<AP_Invoice__c> newList = (List<AP_Invoice__c>) Trigger.New; 
        
        if(Trigger.isInsert || Trigger.isUpdate) 
        {
            if(CheckRecursive.runOnceAPInvInsertUpdate())
                {
                List<Id> parentIdList = new List<Id>();
                for(AP_Invoice__c invoice : newList)  { 
                    parentIdList.add(invoice.Parent_Invoice__c);
                }
                
                if(!parentIdList.isEmpty())
                {
                    Map<Id, AP_Invoice__c> parentInvoiceMap = new Map<Id, AP_Invoice__c>([Select Id, Labour_ex_GST__c, Material_ex_GST__c
                                                                              FROM AP_Invoice__c Where Id IN : parentIdList]);
                    for(AP_Invoice__c invoice : newList)  {  
                        if(invoice.Invoice_Type__c == NJ_CONSTANTS.AP_CREDIT_MEMO) {
                            // Please don't remove commented code
                            /*if(invoice.Adjustment_Amount_Labour__c != null) {
                                Decimal labourAmount = parentInvoiceMap.get(invoice.Parent_Invoice__c).Labour_ex_GST__c;
                                system.debug('labourAmount==>'+labourAmount * -1);                        
                                invoice.Labour_ex_GST__c =  labourAmount  - invoice.Adjustment_Amount_Labour__c;
                                //if(invoice.Labour_ex_GST__c > 0)
                                invoice.Labour_ex_GST__c = invoice.Labour_ex_GST__c * -1;
                                system.debug('invoice.Labour_ex_GST__c==>'+invoice.Labour_ex_GST__c);
                            }
                            if(invoice.Adjustment_Amount_Materials__c != null) {
                                Decimal materialAmount = parentInvoiceMap.get(invoice.Parent_Invoice__c).Material_ex_GST__c ;
                                invoice.Material_ex_GST__c = materialAmount - invoice.Adjustment_Amount_Materials__c;
                                //if(invoice.Material_ex_GST__c > 0) 
                                invoice.Material_ex_GST__c = invoice.Material_ex_GST__c * -1;
                                
                            }  */
                        }
                        system.debug('invoice.Labour_ex_GST__c==>'+invoice.Labour_ex_GST__c+'  invoice.Material_ex_GST__c==>'+invoice.Material_ex_GST__c);
                    }
                }
            }
        }
        //Logic to Prevent approving APInvoice when ARInvoice is in Submitted/Awaiting Response status 　- start
        if(Trigger.isUpdate) {
            if(CheckRecursive.runOnceAPInvUpdate())
            {
                System.Debug('_________RecursiveCheckAPInvoice________');
                Map<Id,AP_Invoice__c> mapOldAP = (Map<Id,AP_Invoice__c>)Trigger.oldMap;
                Map<Id,AP_Invoice__c> mapNewAP = (Map<Id,AP_Invoice__c>)Trigger.newMap;
                Map<Id,AP_Invoice__c> authorityAPMap = new Map<Id, Ap_Invoice__c>([SELECT Work_Order__r.Authority__c FROM AP_Invoice__c WHERE Id in: Trigger.New]);
                List<Id> auList = new List<id>();
                for (AP_Invoice__c ap : newList){
                    auList.add(authorityAPMap.get(ap.Id).Work_Order__r.Authority__c);
                }
               
                Map<Id, Case> authorityARMap = new Map<Id, Case>([SELECT Id, ParentId, 
                                                                          (SELECT Id,Status__c FROM AR_Invoices1__r WHERE Invoice_Type__c =: NJ_CONSTANTS.INVOICE_CUSTOMER AND Status__c !=:NJ_CONSTANTS.ARINVOICE_STATUS_REJECTED AND Status__c !=:NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED)
                                                                           FROM Case WHERE Id IN : auList and RecordtypeId =: hrAuthorityRecId ]);
                for(AP_Invoice__c APInv: newList)  {
                    If(APInv.Status__c == NJ_CONSTANTS.AP_INVOICE_STATUS_APPROVED && APInv.Status__c != mapOldAP.get(APInv.Id).Status__c){                    
                        If(authorityARMap.get(authorityAPMap.get(APInv.Id).Work_Order__r.Authority__c).AR_Invoices1__r.size()>0) 
                        mapNewAP.get(APInv.Id).addError('There is an open AR Invoice. Please Reject the AR Invoice.');
                        break;
                    }
                }
            }
        }
        //Logic to Prevent approving APInvoice when ARInvoice is in Submitted/Awaiting Response status 　- End
    }
    //Logic migrated by CRMIT from claimjob to CaseAuthority
    public void bulkAfter()
    {
    
        if(CheckRecursive.runOnceAPInvBulkAfter())
        {
        if(Trigger.isInsert || Trigger.isUpdate) {
            List<AP_Invoice__c> newList = (List<AP_Invoice__c>) Trigger.New; 
            MAP<Id, AP_Invoice__c> oldMAP  = (MAP<Id, AP_Invoice__c>)Trigger.oldMap;
            Map<Id,AP_Invoice__c> mapNewAP = (Map<Id,AP_Invoice__c>)Trigger.newMap;
            Set<Id> arInvoiceSet = new Set<Id>();
            List<Id> parentInvoiceIdList = new List<Id>();
            Map<Id, Ap_Invoice__c> apMap = new Map<Id, Ap_Invoice__c>([SELECT Work_Order__r.Authority__c FROM AP_Invoice__c WHERE Id in: Trigger.New]);
            system.debug('apMap==>'+apMap);
            
            //Update ARInvoice in Submitted status if Do_Not_Invoice_AR__c is clicked in Approved AP Invoice
            //Display Error Message when an Approved AR is present
            List<Id> auList = new List<id>();
            for (AP_Invoice__c ap : newList){
                auList.add(apMap.get(ap.Id).Work_Order__r.Authority__c);
            }
           
            Map<Id, case> authorityARMap = new Map<Id, case>([SELECT Id, ParentId, 
                                                                      (SELECT Id,Status__c FROM AR_Invoices1__r WHERE Invoice_Type__c =: NJ_CONSTANTS.INVOICE_CUSTOMER AND Status__c !=:NJ_CONSTANTS.ARINVOICE_STATUS_REJECTED)
                                                                         FROM case WHERE Id IN : auList]);
            //Update ARInvoice in Submitted status if Do_Not_Invoice_AR__c is clicked in Approved AP Invoice
            
            for(AP_Invoice__c invoice : newList ) {
                system.debug(invoice);
                system.debug('GST_Distribution_Set__c==>'+invoice.GST_Distribution_Set__c);
                if(invoice.Invoice_Type__c == NJ_CONSTANTS.AP_CREDIT_MEMO && !String.isEmpty(invoice.Parent_Invoice__c)) {
                    parentInvoiceIdList.add(invoice.Parent_Invoice__c);
                }
               /* if(Trigger.isUpdate) {
                    if((invoice.Do_Not_Invoice_AR__c != oldMAP.get(invoice.Id).Do_Not_Invoice_AR__c) || 
                        (invoice.Status__c == NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED && invoice.Status__c != oldMAP.get(invoice.Id).Status__c ))
                        arInvoiceSet.add(apMap.get(invoice.Id).Work_Order__r.Claim_Job__c);
                }*/
                //Update ARInvoice in Submitted status if Do_Not_Invoice_AR__c is clicked in Approved AP Invoice
                //Display Error Message when an Approved AR is present
               if(Trigger.isUpdate) {
                    if((invoice.Do_Not_Invoice_AR__c != oldMAP.get(invoice.Id).Do_Not_Invoice_AR__c) && invoice.Status__c == NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED)
                        If(authorityARMap.get(apMap.get(invoice.Id).Work_Order__r.Authority__c).AR_Invoices1__r.size()>0){
                           If(authorityARMap.get(apMap.get(invoice.Id).Work_Order__r.Authority__c).AR_Invoices1__r[0].Status__c == NJ_CONSTANTS.ARINVOICE_STATUS_SUBMITTED){ 
                               arInvoiceSet.add(apMap.get(invoice.Id).Work_Order__r.Authority__c);
                            }
                            else if (authorityARMap.get(apMap.get(invoice.Id).Work_Order__r.Authority__c).AR_Invoices1__r[0].Status__c == NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED){
                                    list<PermissionSetAssignment> permlst=[SELECT Id,PermissionSetId,Assignee.Id,PermissionSet.name,Assignee.name FROM PermissionSetAssignment where PermissionSet.name=:system.label.ARClerk_Supervisor and Assignee.id=:UserInfo.getUserId()];
                                    If (permlst.isEmpty()){
                                        mapNewAP.get(invoice.Id).addError('The AR Invoice is Approved.Cannot Modify Do Not Invoice AR.');
                                    }
                                    
                                break;
                            }
                        }                       
               }
                system.debug('*************************claimjob list' + arInvoiceSet);
               //Update ARInvoice in Submitted status if Do_Not_Invoice_AR__c is clicked in Approved AP Invoice
            }
            system.debug('parentInvoiceIdList==>'+parentInvoiceIdList);
            if(!parentInvoiceIdList.isEmpty()) {
                List<AggregateResult> summaryResult = [SELECT Parent_Invoice__c, SUM(Labour_ex_GST__c) labourWithoutGST,
                                                        SUM(Material_ex_GST__c) materialWithoutGST, SUM(Total_ex_GST__c) total
                                                        FROM AP_Invoice__c WHERE Parent_Invoice__c IN: parentInvoiceIdList AND Invoice_Type__c =: NJ_CONSTANTS.AP_CREDIT_MEMO AND Status__c !=: NJ_CONSTANTS.ARINVOICE_STATUS_REJECTED GROUP BY Parent_Invoice__c];
                system.debug('summaryResult ==>'+summaryResult );   
                Map<Id, AP_Invoice__c> invoiceMap = new Map<Id, AP_Invoice__c>([SELECT Id, Labour_ex_GST__c, Adjustment_Amount_Labour__c,                                                               Adjustment_Amount_Materials__c, Material_ex_GST__c, Total_ex_GST__c,                                                               Parent_Invoice__r.Labour_ex_GST__c, Parent_Invoice__r.Material_ex_GST__c, Parent_Invoice__r.Total_ex_GST__c
                                                                     FROM AP_Invoice__c WHERE Id IN: parentInvoiceIdList]);
                Map<Id, AggregateResult> summaryMap = new Map<Id, AggregateResult>();
                set<Id> parentInvWithError = new set<Id>();
                for(AggregateResult result : summaryResult) {                    
                    Id parentId = (Id) result.get('Parent_Invoice__c');
                    summaryMap.put(parentId, result);                    
                }
                for(AP_Invoice__c invoice : newList ) {
                    
                    if(invoice.Invoice_Type__c == NJ_CONSTANTS.AP_CREDIT_MEMO && summaryMap.containsKey(invoice.Parent_Invoice__c)) {                       
                        AggregateResult parentSummary = summaryMap.get(invoice.Parent_Invoice__c);
                        Decimal totalCredit = (decimal)parentSummary.get('total') != null ? (decimal)parentSummary.get('total') : 0;
                        totalCredit = totalCredit < 0 ? totalCredit * -1 : totalCredit;
                        
                        Decimal totalLabourCredit = (decimal)parentSummary.get('labourWithoutGST') != null ? (decimal)parentSummary.get('labourWithoutGST') : 0;
                        totalLabourCredit = totalLabourCredit < 0 ? totalLabourCredit * -1 : totalLabourCredit;
                        
                        Decimal totalMaterialCredit = (decimal)parentSummary.get('materialWithoutGST') != null ? (decimal)parentSummary.get('materialWithoutGST') : 0;
                        totalMaterialCredit = totalMaterialCredit < 0 ? totalMaterialCredit * -1 : totalMaterialCredit;
                        
                        AP_Invoice__c parentInvoice = invoiceMap.get(invoice.Parent_Invoice__c);
                        system.debug('totalLabourCredit==>'+totalLabourCredit+' totalMaterialCredit==>'+totalMaterialCredit+' totalCredit==>'+totalCredit);
                        system.debug('parentInvoice.Labour_ex_GST__c==>'+parentInvoice.Labour_ex_GST__c+' parentInvoice.Material_ex_GST__c==>'+parentInvoice.Material_ex_GST__c+' parentInvoice.Total_ex_GST__c==>'+parentInvoice.Total_ex_GST__c);
                        if(totalLabourCredit > parentInvoice.Labour_ex_GST__c ||  
                           totalMaterialCredit > parentInvoice.Material_ex_GST__c ) {
                            //invoice.addError('Credit Memo amount must not be greater than the invoice amount');
                        }                       
                        if((invoice.Adjustment_Amount_Labour__c > 0 && invoice.Labour_ex_GST__c > 0) ||
                            (invoice.Adjustment_Amount_Materials__c > 0 && invoice.Material_ex_GST__c > 0)) {
                                //invoice.addError('Credit Memo amount must not be greater than the invoice amount');
                        }
                    }
                       
                }            
            }
//Update ARInvoice in Submitted status if Do_Not_Invoice_AR__c is clicked in Approved AP Invoice            
            if(!arInvoiceSet.isEmpty()){
                TaskService.updateARAmount(arInvoiceSet);}
        }
        }
    }
        
    public void beforeInsert(SObject so)
    {
    }
    
    public void beforeUpdate(SObject oldSo, SObject so)
    {
    }
    
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so)
    {   
        
    }
    public void beforeUnDelete(SObject so)
    {   
        
    }
    public void afterInsert(SObject so)
    {
    }
    
    public void afterUpdate(SObject oldSo, SObject so)
    {
    }
    
    public void afterDelete(SObject so)
    {
    }
     public void afterUnDelete(SObject so)
    {
    }
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this 
     * method to accomplish any final operations such as creation or updates of other records. 
     */
    public void andFinally()
    {
        
    }
}