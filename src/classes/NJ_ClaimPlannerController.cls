/**
 * Class for NJ_ClaimPlanner class
 * @Author : Nikhil Jaitly
 */
public with sharing class NJ_ClaimPlannerController {
  public class SummVal {
    public String label;
    public String colorCode;
    public String iconName;
    public SummVal(String lbl, String code, String iconName) {
      this.label = lbl;
      this.colorCode = code;
      this.iconName = iconName;
    }
  }
  public static Map<String, SummVal> summaryLabelMap = new Map<String,SummVal> {'New' => new SummVal('New', '#A2D9CE', 'action:description'),
                                                                                'Tentative' => new SummVal('Tentative', '#F4D03F', 'action:info'),
                                                                                'Awaiting Confirmation' => new SummVal('AwaitConf', '#1132D5', 'action:submit_for_approval'),
                                                                                'Confirmed' => new SummVal('Confirmed', '#85C1E9', 'action:user'),
                                                                                'Cannot Complete' => new SummVal('CantCompl', '#EC7063', 'action:reject'),
                                                                                'Cancelled'  => new SummVal('Cancelled', '#18090E', 'action:close'),  
                                                                                'Completed' => new SummVal('Completed', '#58D68D', 'action:approval')};
  public static Map<String, SummVal> summarySmartTile = new Map<String,SummVal> {'New' => new SummVal('New', '#A2D9CE', 'utility:activity'),
                                                                                'Tentative' => new SummVal('Tentative', '#F4D03F', 'utility:info'),
                                                                                'Awaiting Confirmation' => new SummVal('AwaitConf', '#1132D5', 'utility:clock'),
                                                                                'Confirmed' => new SummVal('Confirmed', '#85C1E9', 'utility:edit_form'),
                                                                                'Cannot Complete' => new SummVal('CantCompl', '#EC7063', 'utility:fallback'),
                                                                                'Cancelled'  => new SummVal('Cancelled', '#18090E', 'utility:cut'),  
                                                                                'Completed' => new SummVal('Completed', '#58D68D', 'utility:summary')};
                                                                                                                                              
  public class ClaimPlannerInitInfo {
    @AuraEnabled
    public List<NJ_SelectOptionLightning> summaries;
     @AuraEnabled
    public List<NJ_SelectOptionLightning> summariesSmartTile;
    @AuraEnabled
    public Boolean hasWorkOrder;
    @AuraEnabled
    public Boolean hasUpToAmount;
    @AuraEnabled
    public String woUptoAmount;
    @AuraEnabled
    public Boolean hasSA;
    public ClaimPlannerInitInfo() {
      summaries = new List<NJ_SelectOptionLightning>();
      summariesSmartTile = new List<NJ_SelectOptionLightning>();
      hasWorkOrder = false;
      hasUpToAmount = true;
      hasSA = false;
      woUptoAmount = '';
    }
  }
  /**
   * This method is used to initialize the claim planner component
   * 
   * @param  claimId Case Id
   * @return         ClaimPlannerInitInfo
   */
  @AuraEnabled
  public static ClaimPlannerInitInfo initClaimPlanner(String claimId) {
    System.debug(claimId);
    ClaimPlannerInitInfo cpii = new ClaimPlannerInitInfo();
    for(String summLabel : summaryLabelMap.keySet()) {
      cpii.summaries.add(new NJ_SelectOptionLightning(summLabel, summaryLabelMap.get(summLabel).label, summaryLabelMap.get(summLabel).colorCode,summaryLabelMap.get(summLabel).iconName));
      cpii.summariesSmartTile.add(new NJ_SelectOptionLightning(summLabel, summarySmartTile.get(summLabel).label, summarySmartTile.get(summLabel).colorCode,summarySmartTile.get(summLabel).iconName));

    }
    Integer totalStatus = 0;
    for(AggregateResult agr : [SELECT COUNT(Id) total, Status stat
                               FROM ServiceAppointment
                               WHERE Claim__c = :claimId
                               AND NJ_WO_Cash_Settled__c  = false
                               GROUP BY Status]) {
      for(NJ_SelectOptionLightning summ : cpii.summaries) {
        if(summ.value == String.valueOf(agr.get('stat'))) {
          summ.total = Integer.valueOf(String.valueOf(agr.get('total')));
        }
      }
      for(NJ_SelectOptionLightning summ : cpii.summariesSmartTile) {
        if(summ.value == String.valueOf(agr.get('stat'))) {
          summ.total = Integer.valueOf(String.valueOf(agr.get('total')));
        }
      } 
      totalStatus += Integer.valueOf(String.valueOf(agr.get('total')));
    }
    List<WorkOrder> workOrders = [SELECT Id, Work_Type_Name__c, NJ_WO_Cash_Settled__c, Up_To_Amount__c, WorkOrderNumber,
                                         Authority__r.Job_Type__c, RecordType.DeveloperName, ServiceAppointmentCount
                                  FROM WorkOrder
                                  WHERE CaseId = :claimId];
    
    for(WorkOrder wo : workOrders){ 
      if(wo.ServiceAppointmentCount > 0) {
        cpii.hasSA = true;
      }
      if(!NJ_Constants.UPTOAMOUNTS.contains(wo.Work_Type_Name__c)
        // && wo.RecordType.DeveloperName != NJ_Constants.WORK_ORDER_RT_HOMEREPAIRTRADES
         && wo.Up_To_Amount__c == null
         && wo.Authority__r.Job_Type__c == NJ_Constants.JOB_TYPE_DOANDCHARGE && !wo.NJ_WO_Cash_Settled__c){
          cpii.hasUpToAmount = false;
          cpii.woUptoAmount = wo.WorkOrderNumber + ',';
          System.debug('Nikhil cpii '+cpii);
      }
    }
    if(cpii.woUptoAmount.length() > 0) {
      cpii.woUptoAmount = cpii.woUptoAmount.substring(0, cpii.woUptoAmount.length() - 1);
    }
    if(workOrders.isEmpty()) {
      cpii.hasWorkOrder = false;
    } else {
      cpii.hasWorkOrder = true;
    }
    NJ_SelectOptionLightning totalOpt = new NJ_SelectOptionLightning('Total','Total','','action:add_relationship');
    totalOpt.total = totalStatus;
    cpii.summaries.add(totalOpt);
    totalOpt = new NJ_SelectOptionLightning('Total','Total','#ABC233','utility:list'); 
    totalOpt.total = totalStatus;
    cpii.summariesSmartTile.add(totalOpt);
    return cpii;
  }
}