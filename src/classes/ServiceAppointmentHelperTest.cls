/***************************************************************** 
Purpose: Test Class for SeviceAppointMentHelper 
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            -          20/02/2018      Created      Home Repair Claim System  
*******************************************************************/
@istest
public class ServiceAppointmentHelperTest {
    
    @TestSetup
    static void TestdataCreateMethod() {
       	
        Account acc = HomeRepairTestDataFactory.createAccounts('Test WorkOrder Account')[0];
        insert acc;
        
        contact conVar = HomeRepairTestDataFactory.createTradeContact('test WO Contatct',acc.Id);
        insert conVar;
        
      	Case caseVar   = HomeRepairTestDataFactory.createCase();
      
  		//Commented By CRMIT to create child Authority from calling to HomeRepairTestDataFactory
        /*Claim_Job__c testClaimJob = new Claim_Job__c();
        testClaimJob.Claim__c = caseVar.id;
        testClaimJob.Job_Type__c = 'doAndCharge';
        insert testClaimJob; */
        
        //Added By CRMIT to create child Authority
        case childCase = HomeRepairTestDataFactory.createClaimAuthority(caseVar.Id);
        
        WorkType wot = HomeRepairTestDataFactory.createWorkType('Repair Items','Test Work type');
        insert wot;
        
        Id RecordTypeId = HomeRepairTestDataFactory.retriveRecordTypeId('Home Repair','WorkOrder');
        
        WorkOrder wo = HomeRepairTestDataFactory.createWorkOrderWithCase(RecordTypeId,caseVar.Id,wot.Id,childCase.Id);
        insert wo;
    }
}