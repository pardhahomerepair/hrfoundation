/**
 * Class ARInvoiceHandler
 * Author: Jitendra Kawale
 * Trigger Handler for the AR_Invoice__c SObject. This class implements the ITrigger
 * interface to help ensure the trigger code is bulkified and all in one place.
 */
public with sharing class ARInvoiceHandler
    implements ITrigger{    
        private List<TaskWrapper> tskwprList  = new List<TaskWrapper>();
 public static final Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
 public static final Id hrClaimRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        if(Trigger.isInsert) {          
            List<Id> authorityIdList = new List<Id>();
            List<Id> taskParentId = new List<Id>();
            Map<Id, AR_Invoice__c> authorityInvoiceMap = new Map<Id, AR_Invoice__c>();
            
            Map<Id, Id> claimToAuthorityMap = new Map<Id, Id>();
            for(Sobject invoice : Trigger.new) {                
                authorityIdList.add((Id)invoice.get('Authority__c'));
                authorityInvoiceMap.put((Id)invoice.get('Authority__c'), (AR_Invoice__c)invoice);
            }
            Map<Id, Case> authorityMap = new Map<Id, Case>([SELECT Id, ParentId FROM Case WHERE Id IN: authorityIdList and RecordTypeId =: hrAuthorityRecId]);
            for(case c : authorityMap.values()) {
                taskParentId.add(c.Id);
                taskParentId.add(c.ParentId);
                claimToAuthorityMap.put(c.ParentId, c.Id);
            }
            List<Task> taskList = [SELECT Id, Subject, WhatId FROM Task WHERE WhatId IN : taskParentId 
                                   AND Status !=: NJ_CONSTANTS.SA_STATUS_COMPLETED AND Subject =: NJ_CONSTANTS.TASK_COMPLETION_CALL_SUBJECT ];
            system.debug('taskList==>'+taskList);                    
            /*for(Task incompleteTask : taskList) {
                if(claimJobInvoiceMap.containsKey(incompleteTask.WhatId))
                    claimJobInvoiceMap.get(incompleteTask.WhatId).addError('Please complete all \'completion call\' tasks');
                else {
                    Id jobId = claimToJobMap.get(incompleteTask.WhatId);
                    claimJobInvoiceMap.get(jobId).addError('Please complete all \'completion call\' tasks');
                }
            }*/
        }
        if(Trigger.isUpdate) { 
            Map<Id,AR_Invoice__c> mapOldWO = (Map<Id,AR_Invoice__c>)Trigger.oldMap;
            Map<Id,AR_Invoice__c> mapNewWO = (Map<Id,AR_Invoice__c>)Trigger.newMap;
            Set<Id> approveInvoices = new Set<Id>();
            List<Task> rejectedTasks = new List<Task>();
            Set<Id> rejectedJobIds = new Set<Id>();
            Map<Id, Id> authorityInvoiceMap = new Map<Id, Id>();
            for(Sobject invoice : Trigger.new) {       
                AR_Invoice__c oldInvoice = mapOldWO.get((Id)invoice.get('Id'));
                if((String)invoice.get('Status__c') == NJ_Constants.ARINVOICE_STATUS_APPROVED && (String)invoice.get('Status__c') != oldInvoice.Status__c && (String)invoice.get('Invoice_Type__c') == NJ_Constants.INVOICE_CUSTOMER) {
                    approveInvoices.add((Id)invoice.get('Authority__c'));
                    authorityInvoiceMap.put((Id)invoice.get('Authority__c'), (Id)invoice.get('Id'));
                }
            }
            if(!approveInvoices.isEmpty()) {
                List<AP_Invoice__c> arInvoiceWithAPList = [SELECT Id, AR_Invoice__c, Work_Order__r.Authority__c, Status__c FROM AP_Invoice__c WHERE Work_Order__r.Authority__c IN: approveInvoices];
                //for(AR_Invoice__c inv : arInvoiceWithAPList) {
                    for(AP_Invoice__c apInv : arInvoiceWithAPList) {
                        system.debug('apInv.Status__c==>'+apInv.Status__c);
                        if(apInv.Status__c != NJ_Constants.ARINVOICE_STATUS_APPROVED && apInv.Status__c != NJ_Constants.ARINVOICE_STATUS_REJECTED) {
                            mapNewWO.get(authorityInvoiceMap.get(apInv.Work_Order__r.Authority__c)).addError('Please approve related AP Invoices');
                            break;
                        }
                    }
                //}
                //Logic to Prevent approving ARInvoice when Open workorder is present - Start
                List<WorkOrder> authorityWOList = [SELECT Id, Status,Authority__c from WorkOrder WHERE Authority__c IN :approveInvoices];
                System.debug('authorityWOList****************'+ authorityWOList);
                for(WorkOrder wOrder : authorityWOList){
                    system.debug('workorder status *******************'+ wOrder.Status);
                    if(wOrder.Status != NJ_Constants.WO_STATUS_CLOSED && wOrder.Status != NJ_Constants.WO_STATUS_CANCELLED && wOrder.Status != NJ_Constants.WO_STATUS_REJECTED ){
                            mapNewWO.get(authorityInvoiceMap.get(wOrder.Authority__c)).addError('There is an open work order, please reject this invoice to create new AR invoice once all work orders are closed.');
                            break;
                    }
                }
                //Logic to Prevent approving ARInvoice when Open workorder is present - End

            }
        }
    }
    
    public void bulkAfter()
    {
    
        if(Trigger.isUpdate) {
            Map<Id,AR_Invoice__c> mapOldWO = (Map<Id,AR_Invoice__c>)Trigger.oldMap;
            Map<Id,AR_Invoice__c> mapNewWO = (Map<Id,AR_Invoice__c>)Trigger.newMap;
            set<Id> authorityIds = new set<Id>();
            Map<Id, Id> approvedCustomerInvoiceJob = new Map<Id, Id>();
            List<Task> rejectedTasks = new List<Task>();
            Set<Id> rejectedJobIds = new Set<Id>();
            for(Sobject invoice : Trigger.new) {                
                authorityIds.add((Id)invoice.get('Authority__c'));
                AR_Invoice__c oldInvoice = mapOldWO.get((Id)invoice.get('Id'));
                if((String)invoice.get('Status__c') == NJ_Constants.ARINVOICE_STATUS_REJECTED && (String)invoice.get('Status__c') != oldInvoice.Status__c) {
                    rejectedJobIds.add((Id)invoice.get('Authority__c'));                    
                }  
                if((String)invoice.get('Status__c') == NJ_Constants.ARINVOICE_STATUS_APPROVED && (String)invoice.get('Status__c') != oldInvoice.Status__c && (String)invoice.get('Invoice_Type__c') == NJ_Constants.INVOICE_CUSTOMER) {
                    approvedCustomerInvoiceJob.put((Id)invoice.get('Authority__c'), (Id)invoice.get('Id'));
                }
            }
            system.debug('approvedCustomerInvoiceJob==>'+approvedCustomerInvoiceJob);
            //
            if(!approvedCustomerInvoiceJob.isEmpty()) {
                List<AR_Invoice__c> invoiceList = [SELECT Id, Status__c, Authority__c FROM AR_Invoice__c 
                                                   WHERE Authority__c IN: approvedCustomerInvoiceJob.keySet() AND Invoice_Type__c !=: NJ_Constants.INVOICE_CUSTOMER AND Status__c !=: NJ_Constants.ARINVOICE_STATUS_APPROVED AND Status__c !=: NJ_Constants.ARINVOICE_STATUS_REJECTED];
                for(AR_Invoice__c inv : invoiceList) {
                    mapNewWO.get(approvedCustomerInvoiceJob.get(inv.Authority__c)).addError('Please approve CM Fees Invoice related to the authority');
                }
            }
            List<Case> authorityList = [SELECT Id, Status, AR_Inv_Approval_Check__c , ParentId,
                                                (SELECT Id, Status__c FROM AR_Invoices1__r WHERE Status__c !=:NJ_Constants.ARINVOICE_STATUS_REJECTED)
                                               FROM Case WHERE Id IN : authorityIds and RecordTypeId =: hrAuthorityRecId];
            
            List<User> userList=[Select Id,Name,Email 
                                 FROM User
                                 WHERE Name =: NJ_Constants.HR_CLAIM 
                                 Limit 1];
                                 
            List<case> authorityListToUpdate = new List<case>();            
            for(case authorityCase : authorityList) {
                Integer count = 0;
                for(AR_Invoice__c invoice : authorityCase.AR_Invoices1__r) {
                    if(invoice.Status__c == NJ_Constants.ARINVOICE_STATUS_APPROVED)
                        count++;  
                }
                if(count == authorityCase.AR_Invoices__r.size()) {
                    authorityCase.Status = NJ_Constants.CLAIM_JOB_STATUS_COMPLETED; 
                    authorityCase.AR_Inv_Approval_Check__c = true;                    
                }
                else {                    
                    authorityCase.AR_Inv_Approval_Check__c = false;
                }
                authorityListToUpdate.add(authorityCase);
                //Create Task for rejected invoices
                if(rejectedJobIds.contains(authorityCase.Id)) {
                    rejectedTasks.add(new Task(priority = NJ_CONSTANTS.TASK_PRIORITY_NORMAL ,
                         status = NJ_CONSTANTS.TASK_STATUS_NOT_STARTED ,
                         subject = NJ_Constants.INVOICE_REJECTED,
                         IsReminderSet = true,                             
                         OwnerId = userList[0].id,
                         ActivityDate = System.Today() + 2,
                         ReminderDateTime = System.now()+2,
                         Task_Type_Reporting__c = NJ_Constants.INVOICE_REJECTED,
                         WhatId =  authorityCase.ParentId,
                         Authority__c = authorityCase.Id));
                }
            }
            
            //if(claimJobListToUpdate.size() > 0)
            //update claimJobListToUpdate;
            if(rejectedJobIds.size() > 0) {
                system.debug('rejectedTasks==>'+rejectedTasks);
                insert rejectedTasks;
                //Lock Existing work orders 
                List<WorkOrder> workOrderListToLock = [SELECT ID, Status                                                      
                                                       FROM WorkOrder 
                                                       WHERE Authority__c IN :rejectedJobIds];
                system.debug('workOrderListToLock========>'+workOrderListToLock);
                //Lock Existing work orders in status Closed and Cancelled
                for(WorkOrder order : workOrderListToLock) {
                    if(order.Status==NJ_Constants.WO_STATUS_CLOSED ||order.Status==NJ_Constants.WO_STATUS_CANCELLED ){
                        order.RecordLocked__c = true;
                    }
                }
     
                update workOrderListToLock;
                
                //If ARInvoice is rejected completion call created is set to false
                //This facilitates another Completion call to be created for future workorders
                List<case> rejectedAuthorityList = [SELECT Id, Completion_Call_created__c,Date_Completed__c, Status ,Job_Type__c
                                                        FROM case WHERE Id IN : rejectedJobIds and RecordTypeId =: hrAuthorityRecId];
                system.debug('Rejected Authority List========>'+rejectedAuthorityList);

                for(case authorityClaim : rejectedAuthorityList) {
                    If(authorityClaim.Job_Type__c == NJ_CONSTANTS.JOB_TYPE_DOANDCHARGE || authorityClaim.Job_Type__c == NJ_CONSTANTS.JOB_TYPE_CONTENT){
                        authorityClaim.Completion_Call_created__c = false;
                    }
                    authorityClaim.Date_Completed__c = Null;
                }
                update rejectedAuthorityList;
            }
        }
        
        
    }
        
    public void beforeInsert(SObject so)
    {
    }
    
    public void beforeUpdate(SObject oldSo, SObject so)
    {
    }
    
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so)
    {   
        
    }
    public void beforeUnDelete(SObject so)
    {   
        
    }
    public void afterInsert(SObject so)
    {
    }
    
    public void afterUpdate(SObject oldSo, SObject so)
    {
    }
    
    public void afterDelete(SObject so)
    {
    }
     public void afterUnDelete(SObject so)
    {
    }
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this 
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        
    }
}