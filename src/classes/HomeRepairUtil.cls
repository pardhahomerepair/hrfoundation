public without sharing class HomeRepairUtil{
    public static boolean UpdateDone = false;
    public static ServiceAppointment createServiceAppointment(String workOrderId){
        ServiceAppointment sa=new ServiceAppointment();
        sa.ParentRecordId=workOrderId;
        sa.Status='New';
        sa.SchedEndTime=System.today().addDays(+2);
        sa.SchedStartTime=System.now();
        return sa;
    }

    private static void createTaskTradePortal(string objectId,string docType,string claimId){
        // New Task object to be created
        Task[] newTaskList = new Task[0];
        string taskDescription='';
        Id workOrderId;
        List<User> userList=[Select Id,Name,Email 
                             FROM User
                             WHERE Name = : NJ_CONSTANTS.HR_CLAIM 
                             Limit 1];
        List<Document_Type__mdt> documentTypeList = new List<Document_Type__mdt>();
        documentTypeList=Database.query('SELECT DeveloperName,Activity_Description__c FROM Document_Type__mdt WHERE MasterLabel = \''+ docType + '\'');
        System.debug('DocTypeList:'+documentTypeList);    
        If (documentTypeList[0].Activity_Description__c != null){
         
            if(documentTypeList.size() > 0){
                taskDescription=documentTypeList[0].Activity_Description__c;
            }
            
            String objectType = String.valueOf(Id.valueOf(objectId).getsobjecttype());
            system.debug('objectType :' + objectType);
            if(objectType == 'WorkOrder'){
                WorkOrder wo = new WorkOrder();
                wo = [select Id, WorkOrderNumber, Service_Resource__r.name from WorkOrder where Id = :objectId  Limit 1];
                workOrderId = wo.Id;
                taskDescription = taskDescription + ' from ' + wo.Service_Resource__r.name + ' for Work Order ' + wo.WorkOrderNumber;
                if(docType == 'Invoice'){
                    wo.Invoice_Uploaded__c=true;
                }
                update wo;
                system.debug('taskDescription ' +taskDescription);
            }
            Task t = new Task();
            t.priority = NJ_CONSTANTS.TASK_PRIORITY_IMPORTANT;
            t.status = NJ_CONSTANTS.TASK_STATUS_NOT_STARTED;
            
            If(documentTypeList[0].Activity_Description__c == 'Invoice Received')
                taskDescription = 'Invoice Received' ;
            If(documentTypeList[0].Activity_Description__c == 'Review Correspondence')
                taskDescription = 'Review Correspondence' ;

            t.subject = taskDescription;
            t.IsReminderSet = true;                            
            t.OwnerId = userList[0].id;
            t.ActivityDate = System.Today() + 1;
            t.ReminderDateTime = System.now()+1;
            t.WhatId =  claimId;
            t.Work_Order__c = workOrderId;
            newTaskList.add(t);
            if (newTaskList.Size() > 0){
              insert newTaskList; 
            }
        }
    }
    public static List<ContentDocumentLink> contentDocumentList(Set<String> objectIds) {
        return [SELECT id,ContentDocumentId,ContentDocument.LatestPublishedVersion.Title,ContentDocument.LatestPublishedVersionId,ContentDocument.LatestPublishedVersion.LastModifiedBy.Name,ContentDocument.Description,ContentDocument.CreatedDate FROM ContentDocumentLink where LinkedEntityId IN: objectIds order by ContentDocument.LatestPublishedVersion.CreatedDate DESC limit 5];
    }
 
    public static String deleteContentVersion(Set<String> contentDocumentIds) {
        System.Debug(LoggingLevel.DEBUG, 'Entering HomeRepairUtil with Param contentDocumentIds : '+contentDocumentIds);
        String returnMessage = '';
        //check if content document Ids are not empty
        if(!contentDocumentIds.isEmpty()){  
            ContentDocument[] documentFiles = [SELECT Id FROM ContentDocument WHERE Id IN: contentDocumentIds]; 
            try {
                //delete content
                delete documentFiles;
                returnMessage = 'Success';
            } catch (DmlException e) {
                // Process exception here
                returnMessage = 'Error';
            }
        }
        system.debug('Exiting HomeRepairUtil with returnMessage: '+returnMessage);
        return returnMessage;
    }
    public static String deleteRepairItems(Set<String> deleteIds,String objectName) {
        System.Debug(LoggingLevel.DEBUG, 'Entering HomeRepairUtil with Param deleteIds : '+deleteIds+' objectName:'+objectName);
        String returnMessage = '';
        if(objectName=='Room__c'){
            List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem >();
            Set<String> claimIds=new Set<String>();
            List<WorkOrder> workOrderList =new List<WorkOrder>();
            List<Room__c> roomList = Database.query('SELECT Id,Claim__c,(Select id from Work_Order_Line_Items__r) FROM  Room__c WHERE Id IN : deleteIds');
            for(Room__c rm : roomList){
                if(rm.Work_Order_Line_Items__r.size() > 0){
                    woliList.addAll(rm.Work_Order_Line_Items__r);
                }
                claimIds.add(rm.Claim__c);
            }
            try {
                delete roomList;
            } catch (DmlException e) {
                // Process exception here
                returnMessage = 'Delete Room failure : '+e.getMessage();
            }
            try {
                delete woliList;
            } catch (DmlException e) {
                // Process exception here
                returnMessage = 'Delete WorkOrderLineItems failure : '+e.getMessage();
            }
            List<WorkOrder> workOrderClaimList = [select id,CaseId,WorkTypeId,WorkType.Name,TotalPrice,Sort_Order__c,(select id,WorkTypeId,WorkType.Name,Room__c,Room__r.Name,PricebookEntryId,PricebookEntry.Name,Cash_Settled__c,Contents_Type__c,UnitPrice,TotalPrice,Quantity,Description from WorkOrderLineItems) from WorkOrder where WorkOrder.RecordType.Name='Home Repair Trades' and CaseId IN : claimIds];
            for(WorkOrder wo : workOrderClaimList){            
                if(wo.WorkOrderLineItems.size() == 0){
                    workOrderList.add(wo);
                }
            }
            //delete workorders
            if(!workOrderList.isEmpty()){                
                try {
                    delete workOrderList;
                } catch (DmlException e) {
                    // Process exception here
                    returnMessage = 'Delete WorkOrder failure : '+e.getMessage();
                }
            }
        }else{
            List<sObject> sobjList = Database.query('SELECT Id FROM '+ objectName +' WHERE Id IN : deleteIds');
            try {
                delete sobjList;
            } catch (DmlException e) {
                // Process exception here
                returnMessage = 'failure : '+e.getMessage();
            }
        }
        returnMessage = 'success';
        system.debug('Exiting HomeRepairUtil with returnMessage: '+returnMessage);
        return returnMessage;
    }
    public static string updateContentFile(String docType, Boolean sendDocToClaimHub,String parentId,Boolean isActivityRequired, string documentId, string claimId, string claimJobId) {
        String ContentDocumentId=[select id from ContentVersion where ContentDocumentId=:documentId].Id;
        ContentVersion cv = new ContentVersion();
        cv.Id=ContentDocumentId;
        cv.Send_to_ClaimHub__c = sendDocToClaimHub;
        cv.Authority__c=claimJobId;
        cv.OwnerId=System.Label.HR_Claims;
        cv.Description=docType;
        try {
            update cv;
            if(isActivityRequired){
                createTaskTradePortal(parentId,docType,claimId);
            }
            return 'Success';
        } catch (DmlException e) {
            return 'Failed';
        }        
    }
  
    public static String doUploadContentFromAttachment(List<Attachment> attachments,Set<String> conIds) {
        List<ContentVersion> contentToBeCreated = new List<ContentVersion>();
        List<ContentDocumentLink> contentLinkToBeCreated = new List<ContentDocumentLink>();
        if (attachments.size() == 0) {
            return 'success';
        }
        Map<string,String> leadToAccountIdMap = new Map<string,String>();
        //check if parent is present
        if(attachments.size() > 0) {
            //for each attachment record, create a content object record to be inserted
            for(Attachment att: attachments){
                if(att.Body != null) {
                    ContentVersion cv = new ContentVersion();
                    cv.ContentLocation = 'S';
                    cv.VersionData = EncodingUtil.base64Decode(EncodingUtil.base64Encode(att.Body));
                    cv.Title = att.Name;
                    cv.Description=att.Description;
                    String fileType='';
                    if(att.ContentType != null){ 
                        fileType=att.ContentType;                   
                        fileType=fileType.SubString(fileType.indexOf('/')+1,fileType.length());
                    }
                    cv.PathOnClient = att.Name+'.'+fileType;  
                    leadToAccountIdMap.put(cv.PathOnClient,att.ParentId);                  
                    contentToBeCreated.add(cv);
                }                   
            }
            //insert content
            if(!contentToBeCreated.isEmpty()){
                insert contentToBeCreated;
            }
            //get all inserted Ids
            Set<Id> cIds = new Set<Id>();
            for(ContentVersion c:contentToBeCreated){
                if(c.ContentDocumentId == null){
                    cIds.add(c.Id);
                }
            }  
            //query the content document ids and form content links to be inserted
            for(ContentVersion cnt : [SELECT Id, ContentDocumentId,PathOnClient FROM ContentVersion WHERE Id IN: cIds]){
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = cnt.ContentDocumentId;
                if(leadToAccountIdMap.containsKey(cnt.PathOnClient)){
                    cdl.LinkedEntityId = leadToAccountIdMap.get(cnt.PathOnClient);
                }
                cdl.ShareType = 'V';
                cdl.Visibility = 'AllUsers';
                contentLinkToBeCreated.add(cdl);
            }
            //insert content links
            if(!contentLinkToBeCreated.isEmpty()){
                insert contentLinkToBeCreated;
                List<Attachment> attList = [SELECT Id FROM Attachment where ParentId IN : conIds];
                if(!attList.isEmpty()){
                    delete attList;
                }
            }
            return 'success';   
        } else {
            system.debug('Exiting HomeRepairUtil'); 
            return 'No Attchment Found';
        }
        
    }
    public static String doUploadContent(String parentId,string woID, List<contentWrapper> attachments) {
        
        System.Debug(LoggingLevel.DEBUG, 'Entering HomeRepairUtil with Param parentId : '+parentId);
        List<ContentVersion> contentToBeCreated = new List<ContentVersion>();
        List<ContentDocumentLink> contentLinkToBeCreated = new List<ContentDocumentLink>();
        if (attachments.size() == 0) {
            return 'success';
        }
        
        //check if parent is present
        if(parentId != null || woID != null) {
            //for each attachment record, create a content object record to be inserted
            for(contentWrapper attach: attachments){
                if(attach.attachmentBody != null) {
                    ContentVersion cv = new ContentVersion();
                    cv.ContentLocation = 'S';
                    cv.ContentDocumentId = attach.ContentDocumentId;
                    cv.VersionData = EncodingUtil.base64Decode(attach.attachmentBody);
                    cv.Title = attach.attachmentName;
                    cv.PathOnClient = attach.attachmentName;
                    contentToBeCreated.add(cv);
                }                   
            }
            //insert content
            if(!contentToBeCreated.isEmpty()){
                insert contentToBeCreated;
                                system.debug('in HRUTIL 2' + contentToBeCreated);
            }
            //get all inserted Ids
            Set<Id> cIds = new Set<Id>();
            for(ContentVersion c:contentToBeCreated){
                if(c.ContentDocumentId == null){
                    cIds.add(c.Id);
                }
            }  
            //query the content document ids and form content links to be inserted
            for(ContentVersion cnt : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN: cIds]){
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = cnt.ContentDocumentId;
                cdl.LinkedEntityId = parentId;
                if(woID != null) 
                     cdl.LinkedEntityId = woID; 
                cdl.ShareType = 'V';
                cdl.Visibility = 'AllUsers';
                contentLinkToBeCreated.add(cdl);
            }
            //insert content links
            if(!contentLinkToBeCreated.isEmpty()){
                insert contentLinkToBeCreated;
                system.debug('in HRUTIL 1' + contentLinkToBeCreated);
            }
            return 'success';   
        } else {
            system.debug('Exiting HomeRepairUtil'); 
            return 'Parent Id was null';
        }
        
    }
    @AuraEnabled  
    public static Map<String,List<String>> getDependentOptions(string objApiName , string contrfieldApiName , string depfieldApiName){
        system.debug(objApiName + '##' + contrfieldApiName + '###' + depfieldApiName);
        
        String objectName = objApiName.toLowerCase();
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();
        
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        //get the string to sobject global map
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
        
        if (!Schema.getGlobalDescribe().containsKey(objectName)){
            System.debug('OBJNAME NOT FOUND --.> ' + objectName);
            return null;
        }
        
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        if (objType==null){
            return objResults;
        }
        Bitset bitSetObj = new Bitset();
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        //Check if picklist values exist
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            System.debug('FIELD NOT FOUND --.> ' + controllingField + ' OR ' + dependentField);
            return objResults;     
        }
        
        List<Schema.PicklistEntry> contrEntries = objFieldMap.get(controllingField).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> depEntries = objFieldMap.get(dependentField).getDescribe().getPicklistValues();
        objFieldMap = null;
        List<Integer> controllingIndexes = new List<Integer>();
        for(Integer contrIndex=0; contrIndex<contrEntries.size(); contrIndex++){            
            Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
            String label = ctrlentry.getLabel();
            objResults.put(label,new List<String>());
            controllingIndexes.add(contrIndex);
        }
        List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry>();
        List<PicklistEntryWrapper> objJsonEntries = new List<PicklistEntryWrapper>();
        for(Integer dependentIndex=0; dependentIndex<depEntries.size(); dependentIndex++){            
            Schema.PicklistEntry depentry = depEntries[dependentIndex];
            objEntries.add(depentry);
        } 
        objJsonEntries = (List<PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(objEntries), List<PicklistEntryWrapper>.class);
        List<Integer> indexes;
        for (PicklistEntryWrapper objJson : objJsonEntries){
            if (objJson.validFor==null || objJson.validFor==''){
                continue;
            }
            indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);
            for (Integer idx : indexes){                
                String contrLabel = contrEntries[idx].getLabel();
                objResults.get(contrLabel).add(objJson.label);
            }
        }
        objEntries = null;
        objJsonEntries = null;
        system.debug('objResults--->' + objResults);
        return objResults;
    }
    //wrapper class to store content information
    public class contentWrapper {
        public String attachmentBody {get; set;}
        public String attachmentName {get; set;}
        public String ContentDocumentId {get; set;}
        //constructor method. 
        public contentWrapper(String a, String b, String c) {
            attachmentBody = a;
            attachmentName = b;
            ContentDocumentId = c;
        }
    }
    
  
  /*  Public static string createTask ( List<taskWrapper> tskWrapper ){
        string resp = ' ';
        List<Task>   tskList = new List<Task>();
        
        string resp = 'Success';
        system.debug('HomeRepairUtil Task Creation');
        if(!UpdateDone) { 
        For(taskWrapper ts : tskWrapper)
        {
            System.Debug('ts: '+ ts);
            Task  t = new Task();
            t.Subject                   = ts.Subject;
            t.ActivityDate              = ts.ActivityDate;
            t.WhatId                    = ts.WhatId;
            t.Description               = ts.Description;
            t.Priority                  = ts.Priority;
            t.whoID                     = ts.whoIDs;
            t.Work_Order__c             = ts.workOrder;            
            if(ts.assignID != null && ts.assignID != '') { 
                system.debug('assignID ' + ts.assignID); 
                t.OwnerId                   = ts.assignID;
            }
            
            t.Task_Type_Reporting__c    = ts.TaskType;
            t.Status                    = ts.status;
            tskList.add(t); 
            
        }
            Insert  tskList ;
            UpdateDone = true;
            system.debug('tskList ' +tskList);
            resp = 'Success';
        }  
       
        return resp;
            
         
        
    }*/
  
}