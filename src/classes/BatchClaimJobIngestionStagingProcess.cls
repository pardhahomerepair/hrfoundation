/***************************************************************** 
Purpose: Batch job to process instance of a Claim Job staging object into the resulting 
Claims (Cases)and Claim Jobs, Claim Job Work Items and update any existing ones.
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Vasu          20/02/2018      Created      Home Repair Claim System  
*******************************************************************/

global class BatchClaimJobIngestionStagingProcess implements Database.Batchable<sObject>, Database.Stateful,Database.RaisesPlatformEvents {
    /***************************************************************** 
Purpose: Batch start method to fetch records from Claim Job Ingestion 
Staging with status Pending.
Parameters:None
Returns: Query 
Throws [Exceptions]: None                                                          
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL   Description 
1.0            Vasu          13/03/2018     Created  Home Repair Claim system 
*******************************************************************/
    //Variables 
    private static final String STATUS='Pending';
    private static final String PROCESS_STATUS_EXCLUDED = 'Excluded';
    private static final String PROCESS_STATUS_COMPLETED = 'Completed';
    private static final String PROCESS_STATUS_ERROR = 'Error';
    public static final String CLAIM_TYPE_REPAIR ='ContentsRepair';
    public static final String CLAIM_TYPE_REPLACE ='ContentsReplace';
    public static final String CLAIM_TYPE_BUILDING_CONTENTS ='Building and Contents'; 
    public static final String JOB_TYPE_HOME_ASSISST = 'homeAssist';
    public static final String JOB_TYPE_CONTENTS = 'Contents';
    public static final String JOB_SUB_TYPE_DOANDCHARGE = 'doAndCharge';
    public static final String CASE_STATUS ='New';
    public static final String CASE_ORIGIN = 'Suncorp';
    public static final String PHCONTACTRECORDTYPE ='Policy Holder Contact';
    public static final String PHACCOUNTRECORDTYPE ='Policy Holder Account';
    public static final String IPACCOUNTRECORDTYPE ='Insurance Provider'; 
      
    global Database.QueryLocator start(Database.BatchableContext BC) {
      String query = 'Select AuthorisedAmount__c,Asbestos_Present__c,DateIssued__c,Brand__c,ClaimNumber__c,Construction_Type__c,' +
          'ClaimType__c,Claim_Description__c,DateReceived__c,DateRespondBy__c,Excess__c,HomeAge__c,Id,Insured__c,' +
          'InternalReferenceId__c,JobNumber__c,JobSubType__c,JobType__c,Job_Description__c,LossDate__c,LossCause__c,' +
          'Name,Mould_Present__c,OccupancyType__c,OwnerId,Policy_Number__c,PrimaryContactEmailAddress__c,PrimaryContactHomePhone__c,' +
          'PrimaryContactMobilePhone__c,PrimaryContactRelationshipToInsured__c,PrimaryContactWorkPhone__c,PrimaryContact__c,' +
          'RiskAddressLine1__c,RiskAddressLine2__c,RiskAddressLine3__c,RiskAddressPostcode__c,RiskAddressState__c,' +
          'RiskAddressSuburb__c,RoofType__c,Building_under_construction__c,StatusUpdateDate__c,Status__c, Batch_Error__c, Storeys__c,SpecialInstructions__c,' +
          'VendorReferenceNumber__c ,HomeAssist__c, Source_Channel__c, (select id,Age__c, Category__c, Work_Item_Type__c,Description__c, Item__c, Item_Location__c,'+
          'Make_Model__c, Number_Of_Items__c, Quantity__c, Quote_Amount__c,'+ 
          'Source_Internal_Reference_Id__c, Special_Instructions__c, Unit__c,'+ 
          'Job_Number__c from Claim_Job_Work_Items_Staging__r) FROM Claim_Job_Ingestion_Staging__c Where Process_Status__c =:STATUS';
          Datetime currTimeMinusFive = Datetime.now().addMinutes(-5);         
          if( !Test.isRunningTest() ){
            query += ' AND CreatedDate <= :currTimeMinusFive ';
        }
      System.debug('Main query resultsss============== '+Database.getQueryLocator(query)); 
      return Database.getQueryLocator(query);
      //System.debug('Main query resultsss11============== '+query); 

    }
    
    /***************************************************************** 
Purpose: Batch execute method to process Claim Job Ingestion from Start method result
Parameters:List<Claim_Job_Ingestion_Staging__c>
Returns: none
Throws [Exceptions]: DML                                                          
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL   Description 
1.0            Vasu          13/03/2018     Created  Home Repair Claim system 
*******************************************************************/   
    global void execute(Database.BatchableContext BC, List<Claim_Job_Ingestion_Staging__c> scope) {
       System.debug('inside execute method=======' +scope.size()); 
       System.debug('inside execute method=======' +scope); 

        Set<String> nameSet = new Set<String>();
        Set<String> policyNumberSet = new Set<String>();
        Map<Id, Claim_Job_Ingestion_Staging__c> claimJobProcessList = new Map<Id, Claim_Job_Ingestion_Staging__c>();
        Map<String, Historical_Claim_Data__c> historicalClaimMap = new Map<String, Historical_Claim_Data__c>();
        Map<String, Historic_Claim_Job_Data__c> historicalClaimJobMap = new Map<String, Historic_Claim_Job_Data__c>();
        Map<String, Historical_Claim_Data__c> historicalClaimFalseMap = new Map<String, Historical_Claim_Data__c>();
        List<Historical_Claim_Data__c> updateHistoricalClaimDataList = new List<Historical_Claim_Data__c>();   

        GeneralSettings__c gs = GeneralSettings__c.getValues('ClaimIngestionFilter');
        
        Savepoint sp = Database.setSavepoint();
        // Check if filter required if claim present in G-Claim History table
       // try {
          claimJobProcessList = ClaimJobIngestionUtility.getIngestionForProcessing(scope, historicalClaimMap, historicalClaimFalseMap);
          for (Claim_Job_Ingestion_Staging__c cjis : claimJobProcessList.values()) {
            policyNumberSet.add(cjis.Policy_Number__c);
            nameSet.add(cjis.PrimaryContact__c);
          }
          System.debug('claimJobProcessList======= '+claimJobProcessList); 
          System.debug('nameSet======= '+nameSet); 

		  
		  //Additional processing to find the historical claims
          set<string> claimNumberSet=new set<string>();
          map<String,Id> claimsMap=new map<string,Id>();
          map<String,Id> claimsHistMap=new map<string,Id>();
          for (Claim_Job_Ingestion_Staging__c cjis : scope) {
              if(claimJobProcessList.containsKey(cjis.Id) )
                 claimNumberSet.add(cjis.ClaimNumber__c);
          } 
          System.debug('claimNumberSet======= '+claimNumberSet); 


          for (Historical_Claim_Data__c hc:[select Id,Claim_Number__c from Historical_Claim_Data__c where Claim_Number__c=:claimNumberSet]){
              claimsHistMap.put(hc.Claim_Number__c,hc.Id);
          }
          System.debug('claimsHistMap======= '+claimsHistMap); 
  
         
          
          // Insert Accounts
          Map<String,Account> accountMap = ClaimJobIngestionUtility.upsertAccounts(claimJobProcessList.values(), nameSet, policyNumberSet);
          Map<String, Contact> contactMap = ClaimJobIngestionUtility.upsertContacts(claimJobProcessList.values(), nameSet, policyNumberSet, accountMap);
          ClaimJobIngestionUtility.upsertPolicy(claimJobProcessList.values());
          System.debug('accountMap======= '+accountMap); 
          System.debug('contactMap======= '+contactMap); 
            //Added by CRMIT for to get the ClaimId(  Map<String,Id> claimMap)
          Map<String,Id> claimMap = ClaimJobIngestionUtility.upsertClaim(claimJobProcessList.values(), contactMap,claimsHistMap);
          System.debug('claimMap======= '+claimMap);
            //Added By CRMIT(passing claimMap to the upsertClaimJob)
          Map<String,case> authorityMap = ClaimJobIngestionUtility.upsertClaimJob(claimJobProcessList.values(),claimMap);
          System.debug('authorityMap======= '+authorityMap); 

          if(!authorityMap.isEmpty()) {
            ClaimJobIngestionUtility.upsertClaimJobItem(claimJobProcessList.values(),authorityMap);
          }
  

          //Additional processing to assoicate the claim to Historical cliam data table 
          for (Case c:[select Id,Claim_Number__c from case where Claim_Number__c=:claimNumberSet]){
              claimsMap.put(c.Claim_Number__c,c.Id);
          }
		  
		  for (Claim_Job_Ingestion_Staging__c cjis : scope) {
              // associate historical G-claim Job data with claim 
              if(claimJobProcessList.containsKey(cjis.Id) && claimsHistMap.containsKey(cjis.ClaimNumber__c)){
                  Historical_Claim_Data__c hcd=new Historical_Claim_Data__c();
                  hcd.Id = claimsHistMap.get(cjis.ClaimNumber__c);
                  hcd.Claim__c=claimsMap.get(cjis.ClaimNumber__c);
                  updateHistoricalClaimDataList.add(hcd);
              }
          }
		  
          Map<String, Historical_Claim_Data__c> historicalClaimJobLegacyMap=new Map<String, Historical_Claim_Data__c>();
          historicalClaimJobMap=ClaimJobIngestionUtility.fetchHistoricalClaimJobData(scope); 
          historicalClaimJobLegacyMap=ClaimJobIngestionUtility.fetchHistoricalClaimData(scope,historicalClaimJobMap,false);
          
         // Kemoji added below 2 lines of code.   
          historicalClaimMap=ClaimJobIngestionUtility.fetchHistoricalClaimData(scope,historicalClaimJobMap,true);
          for (Claim_Job_Ingestion_Staging__c cjis : scope) {
            // Check if filter required
            if (gs.Value__c == 'TRUE'){
              // associate historical G-claim with claim job ingestion staging  and update status as Excluded
              if(historicalClaimMap.containsKey(cjis.ClaimNumber__c)){
                cjis.Process_Status__c = PROCESS_STATUS_EXCLUDED ;
                cjis.Historical_Claim_Data__c=historicalClaimMap.get(cjis.ClaimNumber__c).Id;
              } else {
                    //Update status as Excluded for Building and Contents job
                if (cjis.ClaimType__c == CLAIM_TYPE_BUILDING_CONTENTS && cjis.JobSubType__c == JOB_SUB_TYPE_DOANDCHARGE){
                  cjis.Process_Status__c = PROCESS_STATUS_EXCLUDED;
                } else{
                  cjis.Process_Status__c = PROCESS_STATUS_COMPLETED;
                }
              }

            If (historicalClaimJobLegacyMap.containsKey(cjis.ClaimNumber__c)) cjis.Process_Status__c='Decline';
            }
            
          }
         

          if(!scope.isEmpty()){
              update scope;
          }
          if(updateHistoricalClaimDataList.size() > 0){
              update updateHistoricalClaimDataList;
          }
        /*} 
        catch(Exception ex) { //Database.rollback(sp);
                               system.debug(ex.getLineNumber()+ex.getMessage());
        /*  for(Claim_Job_Ingestion_Staging__c cjis : scope) {
            cjis.Batch_Error__c = ex.getMessage() + ' ' + ex.getStackTraceString();
            cjis.Process_Status__c = PROCESS_STATUS_ERROR;
          }
          update scope;
        }*/
    }
 
    
    
    global void finish(Database.BatchableContext BC) {  
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                              TotalJobItems, ApexClass.name, CreatedBy.Email, ExtendedStatus 
                              FROM AsyncApexJob WHERE Id = :bc.getJobId()];
     
   //  list<Profile> pp = [select id from profile where Name = 'Test Admin'];
   
     
    for(user uu : [select Id, Email from user where ID IN ( SELECT UserOrGroupId FROM GroupMember)])
     {
        
     // Send an email to the Apex job's submitter        //   notifying of job completion. 
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
       String[] toAddresses = new String[] {uu.Email};
       mail.setToAddresses(toAddresses);
       mail.setSenderDisplayName('Batch Execution Failure Notice');
       mail.setSubject('Batch: ' + a.ApexClass.name + ', Job Process ended with ' + a.NumberOfErrors  + ' Errors, Contact Support team');
        
       String Reason = ' Error Reason is --> ' ;
        Reason   =  Reason + a.ExtendedStatus;  
       if(a.ExtendedStatus == null )
       {
          Reason = ' ';
           String tt = ' This is for Test Coverage';
       }
        
    }
    }
    
    
}