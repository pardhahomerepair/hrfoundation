@isTest
public class APInvoiceCreditMemo_ccTest {
    @TestSetup
    public static void setupTestData() {
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        //changed to case authority by CRMIT
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);        
        AP_Invoice__c invoice = HomeRepairTestDataFactory.createAPInvoiceWithWorkOrder(wo.id);
        AR_Invoice__c ARinvoice = HomeRepairTestDataFactory.createARInvoiceCMFee(ca.id,cs.id);
        AR_Invoice__c ARinvoicecust = HomeRepairTestDataFactory.createARInvoiceCustomer(ca.id,cs.id);
    }
    public static testMethod void createCreditMemoWithoutSOWTest() {
        AP_Invoice__c invoice = [SELECT Id FROM AP_Invoice__c Limit 1];
        
        Test.startTest();
            String message = APInvoiceCreditMemo_cc.createCreditMemo('123456789', invoice.Id, 'Tester');
            system.assertEquals(message.contains('Success'), true);
        Test.stopTest();        
    }
    public static testMethod void createARCreditMemoCMFeeWithoutSOWTest() {
        AR_Invoice__c invoice = [SELECT Id FROM AR_Invoice__c where invoice_type__c='CM Fees' limit 1];
        ARInvoiceLWCWrapper arinvoicewrapper=new ARInvoiceLWCWrapper();
        arinvoicewrapper.recordId=invoice.Id;
        arinvoicewrapper.creditMemoDetails='Test';
        arinvoicewrapper.labourAmtExGST=0;
        arinvoicewrapper.materialAmtExGST=0;
        arinvoicewrapper.partialAmount=false;
        
        Test.startTest();
             String message = APInvoiceCreditMemo_cc.createCreditMemoAR(arinvoicewrapper);
          //  system.assertEquals(message.contains('Success'), true);
        Test.stopTest();        
    }
    public static testMethod void createARCreditMemoCustomerWithoutSOWTest() {
        AR_Invoice__c invoice = [SELECT Id FROM AR_Invoice__c where invoice_type__c='Customer' limit 1];
        ARInvoiceLWCWrapper arinvoicewrapper=new ARInvoiceLWCWrapper();
        arinvoicewrapper.recordId=invoice.Id;
        arinvoicewrapper.creditMemoDetails='Test';
        arinvoicewrapper.labourAmtExGST=0;
        arinvoicewrapper.materialAmtExGST=0;
        arinvoicewrapper.partialAmount=false;
        
        Test.startTest();
             String message = APInvoiceCreditMemo_cc.createCreditMemoAR(arinvoicewrapper);
          //  system.assertEquals(message.contains('Success'), true);
        Test.stopTest();        
    }
    public static testMethod void createCreditMemoWithSOWTest() {
        AP_Invoice__c invoice = [SELECT Id FROM AP_Invoice__c Limit 1];
        AP_SOW__c sow = new AP_SOW__c(AP_Invoice__c = invoice.Id);
        insert sow;
        Test.startTest();
            String message = APInvoiceCreditMemo_cc.createCreditMemo('123456789', invoice.Id, 'Tester');
            system.assertEquals(message.contains('Success'), true);
        Test.stopTest();
    }
    public static testMethod void createCreditMemoMultipleTest() {
        AP_Invoice__c invoice = [SELECT Id FROM AP_Invoice__c Limit 1];
        invoice.Labour_ex_GST__c = 100;
        invoice.Material_ex_GST__c = 100;
        update invoice;
        
        AP_SOW__c sow = new AP_SOW__c(AP_Invoice__c = invoice.Id);
        insert sow;
        String message = APInvoiceCreditMemo_cc.createCreditMemo('123456789', invoice.Id, 'Tester');
        List<string> messages = message.split(':');
        Ap_Invoice__c creditMemo = new AP_Invoice__c(Id = messages[1]);
        creditMemo.Adjustment_Amount_Labour__c = 50;
        creditMemo.Adjustment_Amount_Materials__c = 50;
        update creditMemo;
        Test.startTest();
            update creditMemo;
            message = APInvoiceCreditMemo_cc.createCreditMemo('123456789', invoice.Id, 'Tester');   
            system.assertEquals(message.contains('Success'), true);
            Case cse = [SELECT Id FROM Case Limit 1];
            try {
                cse.Status = 'Closed';
                update cse;
            }
            catch(Exception e) {
            }
        Test.stopTest();
    } 
    public static testMethod void createCreditNegativeTest() {        
        Test.startTest();           
            String message = APInvoiceCreditMemo_cc.createCreditMemo('123456789', '12313KL','Tester');
            APInvoiceCreditMemo_cc.createCreditMemo('01r2O0000008nBW', '12313KL','Tester');
            system.assertEquals(message.contains('success'), false); 
        Test.stopTest();
    }
}