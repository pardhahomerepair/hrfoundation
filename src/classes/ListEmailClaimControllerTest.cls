/* =====================================================================================
Type:       Test class
Purpose:    Test cases for ListEmailClaimController
========================================================================================*/
@isTest
private class ListEmailClaimControllerTest{
    @testSetup 
    static void setup(){
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Assessment');
        //Commented by CRMIT to replace Claim_Job__c with Case
        // Claim_Job__c cj=HomeRepairTestDataFactory.createClaimJob(cs.Id);
        //Added by CRMIT
        Case childCaseObj = HomeRepairTestDataFactory.createChildClaimAuthority(cs.Id);
        system.debug('======childCaseObj' +childCaseObj);
        //Commented By CRMIT
        // WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,childCaseObj.id);
        // Added By CRMIT
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrdersWithCase('Home Repair Trades',cs.id,wt.id,childCaseObj.id);
        system.debug('======wo' +wo);
        List<WorkOrderLineItem> woliList=HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
        system.debug('======woliList' +woliList);
        
        //Added By CRMIT to create a user with role - START
        UserRole r = [SELECT Id FROM UserRole WHERE Name='COO'];
        system.debug('role in : ' +r);
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
       	User u = new User(Alias = 'standt', Email='adminuser@testorg.com',
                         EmailEncodingKey='UTF-8', LastName='Test',FirstName='testing', LanguageLocaleKey='en_US',
                         LocaleSidKey='en_US', ProfileId = p.Id,UserRoleId = r.Id,
                         TimeZoneSidKey='America/Los_Angeles', UserName='adminuser@testorg.com');
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            insert u;
        }
        
        Account acc;
        System.runAs(u) {
          acc = HomeRepairTestDataFactory.createTradeAccount();
        }
	    //Added By CRMIT to create a user with role -END

        Contact tradeCont = HomeRepairTestDataFactory.createTradeContact('Test',acc.Id);
        user tradeUser = HomeRepairTestDataFactory.createTradeUser(tradeCont.Id);
        HomeRepairTestDataFactory.assignFSLPermissionSets(tradeUser.Id);
        ServiceResource sr = HomeRepairTestDataFactory.createNewServiceResource(userInfo.getUserId());
        ServiceAppointment sa= HomeRepairTestDataFactory.createNewServiceAppointment(wo.Id, 'New', cs.Id, sr.Id);
        Room__c rm=HomeRepairTestDataFactory.createRoom(cs.id,woliList);
        HomeRepairTestDataFactory.addContentVersionToParent(cs.id);
    }   
    static testMethod void ListEmailClaimControllerAllTest() {
        Test.startTest();
        Case cs = [SELECT id FROM Case LIMIT 1];
        List<Messaging.EmailFileAttachment> emailAttachmentList=ListEmailClaimController.getContentDocumentAsAttachement(new List<string>{cs.id});
        ListEmailClaimController.getClaimContactsEmailList(cs.id);
        ListEmailClaimController.getClaimContacts(cs.id);
        ListEmailClaimController.getFiles(cs.id);
        ListEmailClaimController.getTemplates(cs.Id);
        String tempId=[SELECT Id,Name,Subject,TemplateType,body FROM EmailTemplate limit 1].Id;
        ListEmailClaimController.getTemplateDetails(tempId);
        
        List<string> conList = new List<string>();
        List<String> IdList=new List<String>();
        for(Contact con : [SELECT id,Email FROM Contact]){
            conList.add(con.Id);
            IdList.add(con.Id);
        }
        List<string> attIds=new List<string>();
        attIds.add(cs.id);
        ListEmailClaimController.postChatterToClaim(cs.Id,conList);
        // User user= HomeRepairTestDataFactory.createStandardUser('user','Testing');
        //System.RunAs(user) {
        
        User ur=[SELECT id,Email FROM user LIMIT 1];
        List<Messaging.SingleEmailMessage> semList=new List<Messaging.SingleEmailMessage>();
        ListEmailClaimController.generateEmails('test@test.com',ur.id,cs.Id,tempId,'Test','Test',emailAttachmentList,semList,false);
        List<String> emailList=new List<String>();
        emailList.add(ur.Email);
        IdList.add(ur.Id);
        HomeRepairTestDataFactory.addContentVersionToParent(cs.Id);
        ListEmailClaimController.sendAnEmailMsg(tempId,IdList,emailList,emailList,emailList,'Test','Test',attIds,cs.Id);
        // }
        Test.stopTest();
    }
}