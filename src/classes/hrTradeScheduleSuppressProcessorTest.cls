@isTest
public class hrTradeScheduleSuppressProcessorTest {

   @TestSetup
    public static void setupTestData() {
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Account accinsexpiry=HomeRepairTestDataFactory.createTradeAccountWithInsuranceExpiry();
        Account accinsnoexpiry=HomeRepairTestDataFactory.createTradeAccount();
         //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        HomeRepairTestDataFactory.createProductForWorkcode('Test','100',1,'Test');
        case cj=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,cj.id);       
        
         
            
         
    }
  
    public static testMethod void testEmailCaptureOnWOs() {
        list<Id> acctIds=new list<Id>();
        for(Account acc:[select id from account]){
           acctIds.add(acc.Id);
        }
        
        
        Test.startTest();
            
            hrTradeScheduleSuppressProcessor.collectTradeAccountIds(acctIds);
        Test.stopTest();        
    }
}