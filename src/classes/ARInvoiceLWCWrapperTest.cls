@isTest
public class ARInvoiceLWCWrapperTest {
    
    @TestSetup
    public static void setupTestData() {
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        case authorityCase = HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        //Claim_Job__c cj=HomeRepairTestDataFactory.createClaimJob(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,authorityCase.id);        
        AP_Invoice__c invoice = HomeRepairTestDataFactory.createAPInvoiceWithWorkOrder(wo.id);
        AP_Invoice__c invoice2 = HomeRepairTestDataFactory.createAPInvoiceWithSupplierCreditInfo(wo.id,invoice.id);
        AR_Invoice__c ARinvoice = HomeRepairTestDataFactory.createARInvoiceCMFee(authorityCase.id,cs.id);
    }
    public static testMethod void updateAPinvoice() {
        List<AP_Invoice__c> invoice = [SELECT Id FROM AP_Invoice__c ];
        update invoice;
    }
    public static testMethod void createCreditMemoWithoutSOWTest() {
        AR_Invoice__c invoice = [SELECT Id FROM AR_Invoice__c Limit 1];
        
        Test.startTest();
        
        ARInvoiceLWCWrapper arinvoicewrapper =new ARInvoiceLWCWrapper();
        arinvoicewrapper.recordId=invoice.Id;
        arinvoicewrapper.creditMemoDetails='Test';
        arinvoicewrapper.labourAmtExGST=0;
        arinvoicewrapper.materialAmtExGST=0;
        arinvoicewrapper.partialAmount=false;
        
        Test.stopTest();        
    }
}