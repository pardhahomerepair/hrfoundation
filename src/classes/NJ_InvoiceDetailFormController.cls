/************************************************************************************************************
Name: InvoiceDetailFormController
=============================================================================================================
Purpose: Controller of Lightning Component InvoiceDetailForm 
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL       DESCRIPTION
1.0        Nikhil         15/03/2019         Created     InvoiceDetailFormController
*************************************************************************************************************/
public with sharing class NJ_InvoiceDetailFormController { 
    /**
     * This method is used to delete the file uploaded by the user
     * @param  contentDocumentId Content document Id of the file
     * @param  woId              Associated work order Id
     * @return                   
     */
    @AuraEnabled
    public static Boolean deleteFile(String contentDocumentId){
        Boolean isDeleted = true;
        try {
            List<ContentDocument> documents = [SELECT Id
                                               FROM ContentDocument
                                               WHERE Id = :contentDocumentId
                                              ];
            if(!documents.isEmpty()) {
                delete documents;
            }
        }
        catch(Exception ex) {
            isDeleted = false;
        }
        return isDeleted;
    }
    @AuraEnabled
    public static string updateFileDescription(String parentId,String docType,string documentId) { 
        HomeRepairUtil.updateContentFile(docType,false,parentId,false,documentId,null,null);
        return 'Success';
    }
    @AuraEnabled
    public static WorkOrder getWorkOrdereRecord(String woId){
        List<WorkOrder> lstWO = [SELECT Id, Status, Total_Amount__c, Total_Variations_Amounts_Ex_Rejected__c,
        (Select Id, Additional_Labour_Amount__c, Additional_Materials_Amount__c , Addl_Labour_Add_Material_GST__c from Variations__r where Variation_Status__c='Approved' and Work_Order__c=:woId),
        Up_To_Amount__c, RecordType.Name  FROM WorkOrder WHERE Id = :woId];
        return lstWO[0];
    }
    @AuraEnabled
    public static String createInvoiceRecord(String invoice, String woId){
        if (!Schema.sObjectType.AP_Invoice__c.isCreateable()) {
            return 'You do not have access to perform this action';
        }
        
        Id woLockedRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Home Repair Trades Locked').getRecordTypeId();
        Id woliLockedRTId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('WOLI Locked').getRecordTypeId();
        Id saLockedRTId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByName().get('SA Locked').getRecordTypeId();
        List<WorkOrderLineItem> lstWOLIToUpdate = new List<WorkOrderLineItem>();
        List<ServiceAppointment> lstSAToUpdate = new List<ServiceAppointment>();
        Savepoint sp = Database.setSavepoint();
        try{
            AP_Invoice__c invoiceObj = (AP_Invoice__c)JSON.deserialize(invoice, AP_Invoice__c.class);
            WorkOrder order = [SELECT Id, Service_Resource_Company__c,Service_Resource_Company__r.Name, Service_Resource_Company__r.ABN__c, Authority__r.Description,Claim__c, Case.Id
                               FROM WorkOrder WHERE Id =: woId];
                               
            
            //Check whether the Invoice Numer is unique for that Company is duplicate or not
            if(checkDuplicateController(invoiceObj.Supplier_Invoice_Ref__c, order.Service_Resource_Company__r.ABN__c)=='duplicate')
            {
                return 'The invoice number: ' + invoiceObj.Supplier_Invoice_Ref__c + ' has already been used.  Please enter a unique invoice number.';
            }
            
            system.debug('Invoice to Insert>>>'+ invoiceObj);
            system.debug('woId>>>'+ woId);
            invoiceObj.Work_Order__c = woId;
            Group grp = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName = :NJ_Constants.AP_QUEUE];
            invoiceObj.OwnerId = grp.Id;
            invoiceObj.Invoice_Description__c = order.Claim__c + ' - '+order.Authority__r.Description;
            invoiceObj.Claim__c = order.Case.Id;
            system.debug('Invoice_Description__c :'+invoiceObj.Invoice_Description__c);
            invoiceObj.New_Trade_Account__c = order.Service_Resource_Company__c;
            insert invoiceObj;
            
            //copy the attached file from workorder to AP-Invoice (ticket HRNEW-1418)
            List<ContentDocumentLink> contentDocLinks = [select id, ContentDocumentId,LinkedEntityId,ShareType, Visibility from ContentDocumentLink where LinkedEntityId=:woId order by SystemModstamp DESC];
            if(contentDocLinks.size()!=0)
            {
            //clone the ContentDocumentLink record

                ContentDocumentLink newFile = new ContentDocumentLink(
                        ContentDocumentId = contentDocLinks[0].ContentDocumentId,
                        Visibility = contentDocLinks[0].Visibility,
                        ShareType = contentDocLinks[0].ShareType);
                        
                
                newFile.LinkedEntityId=invoiceObj.Id;
                insert newFile;
            }
            
            //Create Invoice Line Items & associate to Invoice
            List<WorkOrderLineItem> workOrderLineItemList = [Select ID,WorkOrderId,Status,WorkTypeId,
                                                             Quantity,StartDate,Subtotal,TotalPrice,UnitPrice,Total_inc_GST__c,
                                                             LineItemNumber,Room__c,Room__r.Name,Product2Id,Work_Item_Description__c,GST__c,                                                        
                                                             Invoice_Type__c,Product_Description__c,UOM__c
                                                             FROM WorkOrderLineItem  
                                                             WHERE WorkOrderId =: woId
                                                             AND Cash_Settled__c != true                                                       
                                                             AND Do_Not_Invoice_Prepay_AP__c != true
                                                            ];
            
            Map<String,AP_SOW__c> invLIMap = new Map<String,AP_SOW__c>();
            
            for(WorkOrderLineItem woli : workOrderLineItemList) {
                AP_SOW__c InvLiItemToCreate = new AP_SOW__c();
                if(woli.Product_Description__c == 'Prepayment'){
                    InvLiItemToCreate.Scope_Of_Work__c = woli.Work_Item_Description__c;
                    InvLiItemToCreate.Prepayment_Amount__c = woli.TotalPrice;
                } else{
                    InvLiItemToCreate.Scope_Of_Work__c = woli.Product_Description__c;
                }
                if(woli.Room__c != null){
                    InvLiItemToCreate.Room__c = woli.Room__r.Name;
                }
                InvLiItemToCreate.Trade__c = woli.WorkTypeId;
                InvLiItemToCreate.Quantity__c = woli.Quantity;
                InvLiItemToCreate.UOM__c = woli.UOM__c;  
                //getting the corresponding invoice
                InvLiItemToCreate.AP_Invoice__c = invoiceObj.Id;
                invLIMap.put(woli.Id,InvLiItemToCreate);
                
            }
            if (invLIMap.size() > 0){
                insert invLIMap.Values();
            }
            
            WorkOrder wo = new WorkOrder(Id = woId,Invoice_Uploaded__c=true,recordTypeId = woLockedRTId);
            update wo;
            
            for(ServiceAppointment sa : [SELECT Id, recordTypeId FROM ServiceAppointment WHERE Work_Order__c = :woId]){
                sa.recordTypeId = saLockedRTId;
                lstSAToUpdate.add(sa);
            }
            
            if(!lstWOLIToUpdate.isEmpty()){
                update lstWOLIToUpdate;
            }
            
            if(!lstSAToUpdate.isEmpty()){
                update lstSAToUpdate;
            }
            return 'Success';
            
            
        }catch(Exception e){
            system.debug('Exception>>>'+e.getMessage());
            Database.rollback( sp );
            return 'ERROR'+e.getMessage();
        }
    }
    
    
    //This method checks whether the invoice number entered by the user is a duplicate one or not
    @AuraEnabled
    public static string checkDuplicateController(String invoiceNo, String accountABN) { 
     
        Integer historicalAPInvoicesCount = 0;
        Integer realtimeAPInvoicesCount = 0;
        
        if(invoiceNo!=null && !String.IsBlank(invoiceNo) && accountABN!=null && !String.IsBlank(accountABN))
        {
            historicalAPInvoicesCount = [Select Count() from Historical_AP_Invoice__c where Invoice_Ref__c=: invoiceNo and Trade_ABN__c=:accountABN];
            
            if(historicalAPInvoicesCount==0)
            {
                realtimeAPInvoicesCount = [Select Count() from AP_Invoice__c where Supplier_Invoice_Ref__c=: invoiceNo and New_Trade_Account__r.ABN__c =:accountABN and Status__c='Approved'];
            }
        }
        return historicalAPInvoicesCount==0 && realtimeAPInvoicesCount==0 ? 'original' : 'duplicate';
    }
    
    
    //this method is to check whether the User has visibility on Manual Invoice field. Only Admins, and users with AP Clerk permission should be able to see it.
    @AuraEnabled
    public static boolean fetchVisibility(String userId) {
        if(userId!=null && !String.isBlank(userId))
        {
            List<User> userList = [Select Id, Profile.Name from User where Id=: userId and Profile.Name='System Administrator'];
            System.debug('IS SysAdmin' + userList.size());
            System.debug('IS AP_Clerk' + FeatureManagement.checkPermission('AP_Clerk'));
            if(userList.size()!=0 || FeatureManagement.checkPermission('AP_Clerk'))
            {
                return true;
            }
        }
        return false;
    }

}