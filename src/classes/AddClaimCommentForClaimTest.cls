/* =====================================================================================
Type:       Test class
Purpose:    Test cases for AddClaimCommentForClaim
========================================================================================*/
@isTest
private class AddClaimCommentForClaimTest{
    static testMethod void fetchClaimForClaimJobTest() {
        Home_Repairs_Trigger_Switch__c hrts = HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        List<String> ids=new List<String>();
		 //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        //changed by CRMIT
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        Vendor_Note__c vend = HomeRepairTestDataFactory.createVendorNote(cs.Id,ca.Id);  
        ids.add(vend.id);
        AddClaimCommentForClaim.addClaimComment(ids);
        ca.Status = 'acceptedInWork';
        update ca;
    }
}