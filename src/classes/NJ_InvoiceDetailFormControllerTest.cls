/************************************************************************************************************
Name: NJ_InvoiceDetailFormControllerTest
=============================================================================================================
Purpose: Test calss for Controller of Lightning Component InvoiceDetailForm 
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL       DESCRIPTION
1.0        Nikhil                            Created     NJ_InvoiceDetailFormControllerTest
*************************************************************************************************************/
@isTest
private class NJ_InvoiceDetailFormControllerTest {
        public static final Id hrClaimRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();

    @testSetup
    private static void testDataFactory(){
        //create policy
        policy__c testPolicy = homeRepairTestDataFactory.createPolicy();
        //System.debug('policy: ' + testPolicy);
        
        //create case associated to policy
        String pbId = Test.getStandardPricebookId();
        case testCase = new Case();
        testCase.Origin = 'Web';
        testCase.RecordTypeId = hrClaimRecId;
        testCase.Status = 'Suncorp';
        testCase.Priority = 'Medium';        
        testCase.Policy__c = testPolicy.id;
        //testCase.Policy__r.State__c = 'VIC';
        testCase.Total_Price__c = 200.00;
        insert testCase;
        //System.debug('testCase: ' + testCase);
        
        String caseID = testCase.id;
        
        //claim job associated to claim
        case ca = HomeRepairTestDataFactory.createClaimAuthority(testCase.id);
        /* Claim_Job__c testClaimJob = new Claim_Job__c();
         testClaimJob.Claim__c = testCase.id;
        testClaimJob.Job_Type__c = 'doAndCharge';
        insert testClaimJob;*/
       
        //create service resource Account relatable to work order
        Account testAccount = new Account();
        //business details
        testAccount.Name = 'testAccount';
        testAccount.ABN__c = '1234';
        testAccount.ABN_Status__c = 'Active';
        testAccount.GST_Status__c = 'Active';
        testAccount.Phone = '0404040404';
        testAccount.When_did_your_business_commence_trading__c = date.newInstance(2011,11,11);
        testAccount.Business_Structure__c = 'sample structure';
        testAccount.RCTI__c = true;
        //business work type
        testAccount.Work_Type__c = 'CabinetMaker';
        testAccount.Service_Areas__c = 'VIC';
        testAccount.Jobs_Per_Week__c = 40;
        //insurance
        testAccount.Public_Liability_Insurance_Provider__c = 'sample insurer';
        testAccount.Public_Liability_Cover_Amount__c = 100.00;
        testAccount.Public_Liability_Expiry__c = date.newInstance(3000, 3, 3);
        testAccount.Work_Cover_Amount__c = 100.00;
        testAccount.Work_Cover_Expiry__c = date.newInstance(3000, 3, 3);
        testAccount.Work_Cover_Insurance_Provider__c = 'sample insurer';
        testAccount.Work_Cover_Insurance_State__c = 'VIC';
        //safe work agreement
        testAccount.Safe_Work_Method_Statement_Agreement__c = 'Yes';
        //bank details
        testAccount.Bank_Account_Name__c = 'sample bank';
        testAccount.BSB__c = '123123';
        testAccount.Account_Number__c = '22221111';
        //accounts contact
        testAccount.Accounts_First_Name__c = 'john';
        testaccount.Accounts_Last_Name__c = 'tron';
        testAccount.Accounts_Phone__c = '2424242424';
        testAccount.Accounts_Email_Address__c = 'jtron@fakemail.com';
        testAccount.AC_Position_in_Company__c = 'cc';
        //jobs related contact
        testAccount.Job_Related_First_Name__c = 'john';
        testAccount.Job_Related_Last_Name__c = 'Gong';
        testAccount.Job_Related_Phone__c = '4545454545';
        testAccount.Job_Related_Position_in_Company__c = 'ff';
        testAccount.Job_Email_Address__c = 'jgong@fakemail.com';
        //License, Authorities, certs and rego's
        testAccount.Any_previous_complaints__c = 'No';
        //other work
        testAccount.Any_current_work_orders__c = 'No';
        testAccount.Any_previous_work__c = 'No';
        //referred?
        testAccount.Referred__c = 'No';
        
        insert testAccount;
        //system.debug('testAccount: ' + testAccount);
                
        WorkType wt = homeRepairTestDataFactory.createWorkType('MakeSafe Items', 'testWorkType');
        String workTypeID = wt.Id; //'08qN00000008OTB';//Cabinetmaker
        
        //generate AP workOrder
        workOrder testWo = new workOrder();
        testWo.Authority__c = ca.id;
        testWo.Subject = 'repair';
        testWo.WorkTypeid = workTypeID;
        testWo.caseid = caseID;
        testWo.Customer_Invoice_Ref__c = 'INVREF1234';
        //testWo.RCTI__c = true;
        testWo.Call_Invoice_Gen_Srvc_for_Non_RCTI__c = true;
        //testWo.Service_Resource__c = testResource.id;
        testWo.Service_Resource_Company__c = testAccount.id;
        testWo.Pricebook2Id = pbId;
        insert testWo;

        //For ServiceAppointment
        ServiceAppointment testServ = FSL_TestDataFactory.createNewServiceAppointment(testWo.Id, 'Tentative', testCase.Id);

        //generate gst chargeable products for woli's
        Product2 testProduct = new Product2();
        
        testProduct.Name = 'testProduct';
        testProduct.GST_Exempt_Labour__c = false;
        testProduct.GST_Exempt_Material__c = false;
        insert testProduct;
        //system.debug('testProduct: ' + testProduct);
        
        string sPricebookEntryId;
        List <PricebookEntry> pbList = new List <PricebookEntry> ();      
        pbList = [select id, Name, ProductCode from PricebookEntry WHERE Product2Id = :testProduct.Id AND Pricebook2Id = :pbId limit 1];
        if(pbList.isEmpty()){
            PricebookEntry pbe = new PricebookEntry();
            pbe.Product2Id=testProduct.Id;
            pbe.IsActive=True;
            pbe.UnitPrice= 100 ;
            pbe.Pricebook2Id = pbId;
            pbe.UseStandardPrice=false;
            insert pbe;
            sPricebookEntryId = pbe.Id;
        }else{
            sPricebookEntryId = pbList[0].Id;
        }
        string woRecId = testWo.Id;
        
        
        //generate woli's associated to workOrder and product
        workOrderLineItem testWOLI = new workOrderLineItem();
        testWOLI.PricebookEntryId = sPricebookEntryId;
        testWOLI.WorkOrderId = testWo.Id;
        testWOLI.WorkTypeId = workTypeID;
        testWOLI.Description = 'abc';
        testWOLI.Site_Visit_Number__c = 1;
        testWOLI.Labour_Rate__c = 100.00;
        testWOLI.Labour_Time__c = 1.00;
        testWOLI.Material_Rate__c = 100.00;
        testWOLI.Quantity = 1;
        testWOLI.Status = 'closed';
        insert testWOLI;

        List<workOrderLineItem> woliList = new List<workOrderLineItem>();
        woliList.add(testWOLI);

        //generate room
        Room__c room = homeRepairTestDataFactory.createRoom(caseID, woliList);
     
    
        ContentVersion contentVersion = new ContentVersion(
          Title = 'Penguins',
          PathOnClient = 'Penguins.jpg',
          VersionData = Blob.valueOf('Test Content'),
          IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = caseID;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl2 = New ContentDocumentLink();
        cdl2.LinkedEntityId = room.Id;
        cdl2.ContentDocumentId = documents[0].Id;
        cdl2.shareType = 'V';
        insert cdl2;
    }

    private static testMethod void getWorkOrdereRecordTest() {
        WorkOrder wo = [SELECT Id,Subject FROM WorkOrder WHERE Subject = 'repair'];
        Test.startTest();
        WorkOrder workOdr = NJ_InvoiceDetailFormController.getWorkOrdereRecord(wo.Id);
        Test.stopTest();

        system.assertEquals(true, workOdr.Id != null);
    }

    private static testMethod void createInvoiceRecordTest(){

        Id woLockedRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Home Repair Trades Locked').getRecordTypeId();
        Id woliLockedRTId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('WOLI Locked').getRecordTypeId();
        Id saLockedRTId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByName().get('SA Locked').getRecordTypeId();

        WorkOrder wo = [SELECT Id,Subject FROM WorkOrder WHERE Subject = 'repair'];

        AP_Invoice__c invoice = new AP_Invoice__c();
        invoice.Supplier_Invoice_Ref__c = 'test Invoice number';
        invoice.Labour_ex_GST__c = 100;
        invoice.Invoice_Type__c = 'Supplier';
        invoice.Status__c = 'Submitted';

        String invoiceJSONString = Json.serialize(invoice); 

        Test.startTest();
        String result = NJ_InvoiceDetailFormController.createInvoiceRecord(invoiceJSONString, wo.Id);
        Test.stopTest();

        //system.assertEquals('Success', result);

        WorkOrder workOdr = [SELECT Id,RecordTypeId FROM WorkORder WhERE Id = :wo.Id];
        //system.assertEquals(workOdr.RecordTypeId , woLockedRTId);

        List<ServiceAppointment> saList = [SELECT Id, recordTypeId FROM ServiceAppointment WHERE Work_Order__c = :wo.Id];
        //system.assertEquals(sa.recordTypeId , saLockedRTId);

    }

    private static testMethod void createInvoiceRecordTest1(){

        Id woLockedRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Home Repair Trades Locked').getRecordTypeId();
        Id woliLockedRTId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('WOLI Locked').getRecordTypeId();
        Id saLockedRTId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByName().get('SA Locked').getRecordTypeId();

        WorkOrder wo = [SELECT Id,Subject FROM WorkOrder WHERE Subject = 'repair'];

        AP_Invoice__c invoice = new AP_Invoice__c();
        invoice.Supplier_Invoice_Ref__c = 'test Invoice number';
        invoice.Labour_ex_GST__c = 100;
        invoice.Invoice_Type__c = 'Supplier';
        invoice.Status__c = 'Submitted';

        String invoiceJSONString = Json.serialize(invoice); 

        Test.startTest();
        String result = NJ_InvoiceDetailFormController.createInvoiceRecord(invoiceJSONString, wo.Id);
        Test.stopTest();

        //system.assertEquals('Success', result);

        WorkOrder workOdr = [SELECT Id,RecordTypeId FROM WorkORder WhERE Id = :wo.Id];
        //system.assertEquals(workOdr.RecordTypeId , woLockedRTId);

        List<ServiceAppointment> saList = [SELECT Id, recordTypeId FROM ServiceAppointment WHERE Work_Order__c = :wo.Id];
        //system.assertEquals(sa.recordTypeId , saLockedRTId);

    }

    
   

    private static testMethod void saveChunkTest3(){

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        WorkOrder wo = [SELECT Id,Subject FROM WorkOrder WHERE Subject = 'repair'];
        
        
        Test.startTest();
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            User newAdminUser = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Test',FirstName='FirstName', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
			insert newAdminUser;
        	
            NJ_InvoiceDetailFormController.updateFileDescription(wo.Id,'Invoice',documents[0].Id);
            Boolean deleteFile = NJ_InvoiceDetailFormController.deleteFile(documents[0].Id);
            System.AssertEquals(deleteFile, true);
            String dupCheck = NJ_InvoiceDetailFormController.checkDuplicateController('testStr', 'accName');
			boolean checkpermission = NJ_InvoiceDetailFormController.fetchVisibility(newAdminUser.Id);
            System.AssertEquals(dupCheck,'original');
 
        Test.stopTest();

        

    }

}