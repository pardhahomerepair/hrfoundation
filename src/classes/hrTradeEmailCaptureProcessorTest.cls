@isTest
public class hrTradeEmailCaptureProcessorTest {

   @TestSetup
    public static void setupTestData() {
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        //Commented by CRMIT
        //Claim_Job__c cj=HomeRepairTestDataFactory.createClaimJob(cs.Id);
		//Added by CRMIT ro replace Claim_Job with Case
		Case childCase = HomeRepairTestDataFactory.createChildClaimAuthority(cs.Id);
        //WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,cj.id);
        //Added BY CRMIT create workorders for case
         WorkOrder wo=HomeRepairTestDataFactory.createWorkOrdersWithCase('Home Repair Trades',cs.id,wt.id,childCase.id);      
    }
  
    public static testMethod void testEmailCaptureOnWOs() {
        WorkOrder WO = [SELECT Id,status FROM Workorder Limit 1];
        
        Test.startTest();
            List<hrTradeEmailCaptureDataWrapper> hrTradeEmailCaptlst=new List<hrTradeEmailCaptureDataWrapper>();
            hrTradeEmailCaptureDataWrapper hrTradeEmailCapture=new hrTradeEmailCaptureDataWrapper();
            hrTradeEmailCapture.workorderId=WO.Id;
            hrTradeEmailCapture.emailTemplate='HAGeneral';
            hrTradeEmailCaptlst.add(hrTradeEmailCapture);
            //hrTradeEmailCaptureProcessor hrTradeEmailCaptProcessor=new hrTradeEmailCaptureProcessor();
            hrTradeEmailCaptureProcessor.collectTradeAllocWorkOrder(hrTradeEmailCaptlst);
        Test.stopTest();        
    }
}