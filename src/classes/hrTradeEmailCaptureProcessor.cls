/************************************************************************************************************
Name: hrTradeEmailCaptureProcessor
=============================================================================================================
Purpose: Class which hosts the invocable method to be called for Invoice Creation from WorkOrder.
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL        DESCRIPTION
1.0        xx               xxx              Created        HomeRepair 
*************************************************************************************************************/

public class hrTradeEmailCaptureProcessor {
    /***************************************************************** 
    Purpose: Invocable Method called from the Process builder which controls                                                      
    Parameters: List of Apex object type which holds workorderId and template
    Returns: none
    Throws [Exceptions]: None                                                          
    History                                                             
    --------                                                            
   VERSION    AUTHOR            DATE             DETAIL            DESCRIPTION
    1.0        xx               xxx              Created           HomeRepair  
    ****************************************************************************************/
    @InvocableMethod
    public static void collectTradeAllocWorkOrder(List<hrTradeEmailCaptureDataWrapper> workOrderDet) {
        System.debug('==== list of workOrderDet====>' +workOrderDet);
    
        Map<Id,String> workOrderDetMap = new Map<Id,String>();
        for(hrTradeEmailCaptureDataWrapper wo : workOrderDet) {
            workOrderDetMap.put(wo.workorderId, wo.emailTemplate);          
        }
        processEmailCaptureCls(workOrderDetMap);
        System.debug('==== list of workOrderDetMap====>' +workOrderDetMap);
 
    }

    @future
    public static void processEmailCaptureCls(Map<Id,String> workOrderDetMap){

        list<string> lststr=new list<string>{'{!WorkOrder.Service_Resource__c}','{!WorkOrder.Claim_Contact__c}','{!WorkOrder.Contact_Phone__c}',
                                            '{!WorkOrder.Risk_Address__c}','{!WorkOrder.Authority__c}','{!WorkOrder.Claim__c}','{!WorkOrder.Id}',
                                            '{!WorkOrder.Description}','{!WorkOrder.Special_Instructions_New__c}','{!WorkOrder.ThreadID__c}','{!WorkOrder.WorkOrderNumber}'};
        
        map<string,string> mapTemplateNames=new map<string,string>{'HAGeneral'=>'HomeAssist','MSRoof'=>'MakeSafe_Roofer','DOCHGLIAREQ'=>'Liability_Quote_Required',
                                                                 'MSRest'=>'MakeSafe_Restorer','MSTree'=>'MakeSafe_Tree_Removalist',
                                                                 'MSGeneral'=>'MakeSafe','DOCHGASSREP'=>'Assess_Repair',
                                                                 'DOCHGQUOAPR'=>'Quote_Approved','DOCHGASSQUO'=>'Assess_Quote',
                                                                 'DOCHGROOF'=>'Report_Required_Roofer','DOCHGREPREQ'=>'Report_Required','DOCHGGeneral'=>'Do_Charge'};
        string resultstr;
        System.debug('==== mapTemplateNames======>' +mapTemplateNames);

        list<WorkOrder> lstworkorder=[select Id,Service_Resource__r.Name,Claim_Contact__c,Contact_Phone__c,Risk_Address__c,WorkOrderNumber,
                                     Authority__r.CaseNumber,Claim__c,Description,Special_Instructions_New__c,ThreadID__c from WorkOrder
                                     where Id=:workOrderDetMap.keyset()];
        System.debug('==== lstworkorder info inside getting all values======>' +lstworkorder);

        for(WorkOrder wk:lstworkorder){
            EmailTemplate emtemp=[Select Name, Id, Body, HtmlValue,  IsActive, Subject from EmailTemplate where DeveloperName=:mapTemplateNames.get(workOrderDetMap.get(wk.Id))];
            System.debug('==== emtemp======>' +emtemp);
            resultstr='';
            resultstr=emtemp.Body;
            string name=wk.Service_Resource__r.Name;
            System.debug('==== name======>' +name);
            string tasksubj=emtemp.Subject;
            System.debug('==== tasksubj======>' +tasksubj);
            tasksubj =(wk.Claim__c!=null)?tasksubj.replace('{!WorkOrder.Claim__c}', wk.Claim__c):tasksubj.replace('{!WorkOrder.Claim__c}', '');
            System.debug('==== tasksubj after======>' +tasksubj);
            
            for(string s:lststr){
                if (resultstr.contains(s) && s=='{!WorkOrder.Service_Resource__c}'){
                    resultstr =(wk.Service_Resource__r.Name!=null) ? resultstr.replace('{!WorkOrder.Service_Resource__c}', wk.Service_Resource__r.Name):resultstr.replace('{!WorkOrder.Service_Resource__c}', '');
                }
                if (resultstr.contains(s) && s=='{!WorkOrder.Claim_Contact__c}'){
                    resultstr =(wk.Claim_Contact__c!=null)?resultstr.replace('{!WorkOrder.Claim_Contact__c}', wk.Claim_Contact__c):resultstr.replace('{!WorkOrder.Claim_Contact__c}', '');
                }
                if (resultstr.contains(s) && s=='{!WorkOrder.Contact_Phone__c}'){
                    resultstr =(wk.Contact_Phone__c!=null)?resultstr.replace('{!WorkOrder.Contact_Phone__c}', wk.Contact_Phone__c):resultstr.replace('{!WorkOrder.Contact_Phone__c}', '');
                }
                if (resultstr.contains(s) && s=='{!WorkOrder.Risk_Address__c}'){
                    resultstr =(wk.Risk_Address__c!=null)?resultstr.replace('{!WorkOrder.Risk_Address__c}', wk.Risk_Address__c):resultstr.replace('{!WorkOrder.Risk_Address__c}', '');
                }
                if (resultstr.contains(s) && s=='{!WorkOrder.Authority__c}'){
                    resultstr =(wk.Authority__r.CaseNumber!=null)?resultstr.replace('{!WorkOrder.Authority__c}',wk.Authority__r.CaseNumber):resultstr.replace('{!WorkOrder.Authority__c}', '');
                }
                if (resultstr.contains(s) && s=='{!WorkOrder.Claim__c}'){
                    resultstr =(wk.Claim__c!=null)?resultstr.replace('{!WorkOrder.Claim__c}', wk.Claim__c):resultstr.replace('{!WorkOrder.Claim__c}', '');
                }
                if (resultstr.contains(s) && s=='{!WorkOrder.Description}'){
                    resultstr =(wk.Description!=null)?resultstr.replace('{!WorkOrder.Description}', wk.Description):resultstr.replace('{!WorkOrder.Description}', '');
                }
                if (resultstr.contains(s) && s=='{!WorkOrder.Special_Instructions_New__c}'){
                    resultstr =(wk.Special_Instructions_New__c!=null)?resultstr.replace('{!WorkOrder.Special_Instructions_New__c}', wk.Special_Instructions_New__c):resultstr.replace('{!WorkOrder.Special_Instructions_New__c}', '');
                }
                if (resultstr.contains(s) && s=='{!WorkOrder.ThreadID__c}'){
                    resultstr =(wk.ThreadID__c!=null)?resultstr.replace('{!WorkOrder.ThreadID__c}', wk.ThreadID__c):resultstr.replace('{!WorkOrder.ThreadID__c}', '');
                }
                if (resultstr.contains(s) && s=='{!WorkOrder.WorkOrderNumber}'){
                    resultstr =(wk.WorkOrderNumber!=null)?resultstr.replace('{!WorkOrder.WorkOrderNumber}', wk.WorkOrderNumber):resultstr.replace('{!WorkOrder.WorkOrderNumber}', '');
                }
                 if (resultstr.contains(s) && s=='{!WorkOrder.Id}'){
                    resultstr =(wk.Id!=null)?resultstr.replace('{!WorkOrder.Id}', wk.Id):resultstr.replace('{!WorkOrder.Id}', '');
                }
            }
            System.debug('==== resultstr======>' +resultstr);
            Task t=new Task(ActivityDate = Date.today(),Subject=tasksubj, OwnerId = UserInfo.getUserId(),
                            Status='Completed',Description=resultstr,whatid=wk.id);
            System.debug('==== ttttttttt======>' +t);
            insert t;
            System.debug('==== ttttttttt======>' +t);

        }                             
    }
            
    


    
    
}