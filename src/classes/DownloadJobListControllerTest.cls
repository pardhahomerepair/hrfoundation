@IsTest
public class DownloadJobListControllerTest {
    @testSetup 
    static void setup(){
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Account Record.
        List<Account> acc =HomeRepairTestDataFactory.createAccounts('Download Test');
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
            con.AccountId=acc[0].Id;
            update con;
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        //Create the WorkType Record.
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        //Create the Product  Record.
        HomeRepairTestDataFactory.createProductForWorkcode('Test','100',1,'Test');
        //Modified by CRMIT
        //Create the Claim Authority Record.
        Case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        //Create the WorkOrder Records.
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);       
            wo.Status='Work Order Assigned'; 
            wo.Service_Resource_Company__c=acc[0].Id;
            update wo;
        //Create the WorkOrderLineItem Record.
        HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
    }
    
    //Test the Download file method with a valid Account Id
    @isTest static void checkDownloadFilewithValidId(){
        test.startTest();
        //fetch the Account
        List<Account> acc = [Select Id from Account];
        //call the Aura Method and pass a valid Account Id
        String returnedHTML = DownloadJobListController.getRelatedData(acc[0].Id);
        //assert that the HTML table is returned
        System.assert(returnedHTML!='');
    }

    //Test the Download file method with an invalid Account Id
     @isTest static void checkDownloadFilewithInvalidId(){
        test.startTest();
        //call the Aura Method and pass an invalid Account Id
        String returnedHTML = DownloadJobListController.getRelatedData('001234567891011');
        //assert that the HTML table is returned
        System.assertEquals(returnedHTML,'');
        test.stopTest();
    }
}