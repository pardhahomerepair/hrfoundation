/***************************************************************** 
Purpose: Release ExcludeResource Work Order Testclass
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            XXXX          03/01/2020    Created      Home Repair Claim System  
*******************************************************************/
@istest 
public class ReleaseExcludeResourceFromWOTest { 
    @testSetup 
    static void setup(){
    
         HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Account accinsexpiry=HomeRepairTestDataFactory.createTradeAccountWithInsuranceExpiry();
        Account accinsnoexpiry=HomeRepairTestDataFactory.createTradeAccount();
         //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        HomeRepairTestDataFactory.createProductForWorkcode('Test','100',1,'Test');
        case au=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,au.id);  
        FSL_TestDataFactory.createUserWithPermissions(TRUE);
        User usr = [SELECT Id, LastName from user limit 1];
                   
                  
        
        //Service Resource
        ServiceResource newSR = FSL_TestDataFactory.createNewServiceResource(usr.Id);
       // insert newSR;
        ResourcePreference RP=new ResourcePreference(RelatedRecordId=wo.Id,PreferenceType='Excluded', ServiceResourceId=newSR.Id);
        insert RP;      
        
         
    }  
    static testmethod void testReleaseExcludeResourceFromWO() {  
         
            map<Id,ServiceResource> mapSR=new map<Id,ServiceResource>([select id from serviceresource]);   
            Test.startTest(); 
                database.executeBatch(new ReleaseExcludeResourceFromWO(mapSR),1);
               
            Test.stopTest();
        
    }
    
   
    
    
}