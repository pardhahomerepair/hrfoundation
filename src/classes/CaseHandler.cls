/*
Apex Trigger to ensure that Appropriate Assessment Report Items
Are created when user interacts with the mobile App.
*/
/*To Do
1. Identify the fields on case which relates to Assessment Items in the background.
At the moment we have the following fields :
Appoint_Repair_Link_Assessor__c(If yes)
Cash_Settlement_Reason__c
Temporary_Accommodation_Reason__c
Asbestos__c
Safety_Repair_Working_Heights__c
Potential_Risks_To_Trades__c
Report_Type__c
Claim_Proceeding__c [Maintenance to be completed]
Claim_Proceeding__c (Decline)

*/

public class CaseHandler implements ITrigger {
    public static final String CLAIM_JOB_STATUS_COMPLETED = 'Completed';
    public static final String CLAIM_JOB_STATUS_ACCEPTED = 'acceptedInWork';
    public static final String CLAIM_HUB_STATUS_UPDATE = 'Pending';
     public static final Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
        public static final Id hrClaimRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();
    public List<Assessment_Report_Item__c> assessmentReportItems =  new List<Assessment_Report_Item__c>(); 
    public List<Assessment_Report_Item__c> delassessmentReportItems =  new List<Assessment_Report_Item__c>(); 
    public List<String>caseIds;
    public List<Case> claimList =  new List<Case>();
    public List<Case> claimAssessmentList =  new List<Case>();
    public Map<String,List<Assessment_Report_Item__c>>caseIdToReportItemsMap;
    Set<string> caseforTaskSet = new Set<string>();
    Set<ID> InscaseIDs = new Set<ID>();
    
    public CaseHandler() {
        
    }
    
    public void bulkBefore() {  
        //Modified Code by CRMIT on 18/02
        //Migrated Logic from claimhandler to casehandler with HR authority record type
        List<Job_Margin__mdt> marginList = [SELECT MasterLabel, QualifiedApiName, Margin__c, DeveloperName FROM Job_Margin__mdt];       
        Decimal dAndCMargin;
        Decimal contentsMargin;
        map<string, Job_Margin__mdt> jobMarginMap = new map<string, Job_Margin__mdt>();
        for(Job_Margin__mdt margin : marginList) {
            jobMarginMap.put(margin.DeveloperName, margin);
        }
        Job_Margin__mdt margin =  jobMarginMap.get('doAndCharge');      
        system.debug('margin==>'+margin);
        if(Trigger.IsInsert){          
            for(Sobject claimTemp : trigger.new){
                // This is to set trigger Claim Hub status update as part of Jitterbit integration
                Case claimRec = (Case) claimTemp;    
                if(claimRec.RecordTypeId == hrAuthorityRecId)
                {
                    if(claimRec.Status == CLAIM_JOB_STATUS_COMPLETED|| claimRec.Status == CLAIM_JOB_STATUS_ACCEPTED){
                        claimRec.ClaimHub_Status_Update__c=CLAIM_HUB_STATUS_UPDATE;
                    }
                } 
            }        
        }
        if(Trigger.IsUpdate){
            List<Case> newList = (List<case>)Trigger.new;
            Map<Id, Case> oldMap = (Map<Id, Case>) Trigger.oldMap;
            
            List<Id> closedCase = new List<Id>();
            for(Case claim : newList){
                if(claim.RecordTypeId == hrAuthorityRecId){
                    if(claim.Status != oldMap.get(claim.Id).Status){
                        if(claim.Status == CLAIM_JOB_STATUS_COMPLETED){
                            claim.ClaimHub_Status_Update__c=CLAIM_HUB_STATUS_UPDATE;
                        }
                    }
                    Decimal marginValue = 0;
                    if(jobMarginMap.containsKey(claim.Job_Type__c))
                        marginValue = jobMarginMap.get(claim.Job_Type__c).Margin__c;
                    else
                        marginValue = 14.5;
                    system.debug('margin==>'+marginValue);
                    claim.Non_Cash_Settle_Margin__c = (claim.Total_Labour_NonCashSettled__c + claim.Total_Material_NonCashSettled__c)*(marginValue/100);
                    claim.Cash_Settle_Margin__c = (claim.Total_Labour_CashSettled__c + claim.Total_Material_CashSettled__c)*(marginValue/100);
                    
                }
                if(claim.Status == NJ_CONSTANTS.CASE_STATUS_CLOSED && oldMap.get(claim.Id).Status != NJ_CONSTANTS.CASE_STATUS_CLOSED) {
                    closedCase.add(claim.Id);
                }
                claim.Non_Cash_Settle_Margin__c = claim.Total_NonCashSettled_Amount__c * (margin.Margin__c / 100);
                claim.Cash_Settle_Margin__c = (claim.Total_Material_CashSettled__c + claim.Total_Labour_CashSettled__c) * (margin.Margin__c / 100);
                system.debug('claim.Cash_Settle_Margin__c==>'+claim.Cash_Settle_Margin__c);
            }
            if(!closedCase.isEmpty()) {
                List<case> closedCaseList = [SELECT Id, 
                                             (SELECT Id FROM Tasks WHERE Subject !=: NJ_CONSTANTS.TASK_FOLLOWER_SUBJECT_CLOSED AND Status !=: NJ_CONSTANTS.TASK_STATUS_COMPLETED) 
                                             FROM Case 
                                             WHERE Id IN : closedCase];
                system.debug('closedCaseList==>'+closedCaseList );                             
                for(Case claim : closedCaseList) {
                    if(!claim.tasks.isEmpty()) {
                        Trigger.newMap.get(claim.Id).addError('Please complete all related tasks before closing this claim');
                    }
                }
            }
        }
    }
    public void bulkAfter() {       
         //Modified Code by CRMIT on 18/02
        //Migrated Logic from claimhandler to casehandler with HR authority record type
        if(Trigger.IsInsert){
            system.debug('update claim status to new when new claim HR Authority is created.');
            List<Id> caseIdList = new List<Id>();
            List<case> caseList = new List<case>();
            for(Sobject claimTemp : trigger.new){
                if(claimTemp.get('RecordTypeId') == hrAuthorityRecId)
                {
                if((Id)claimTemp.get('ParentId') != null)
                {
                    caseIdList.add((Id)claimTemp.get('ParentId'));
                }
                }
            }
            if(caseIdList.size()>0)
            {
                caseList = [SELECT Id, Status From Case WHERE Id IN: caseIdList and Status =: NJ_CONSTANTS.CASE_STATUS_CLOSED];
            }
            if(caseList.size()>0)
            {
                for(Case claim: caseList ) {
                    claim.status = NJ_CONSTANTS.SA_STATUS_NEW; 
                }
                system.debug('caseList==>'+caseList);
                update caseList; 
            }
            
            if(checkRecursive.runOnceClaimInsert()){
                claimList  = new List<Case>();
                for(Sobject claimTemp : trigger.new){ 
                    Case claimRec = (Case)claimTemp;
                    claimList.add(claimRec);
                }
            }
        }
        if(Trigger.IsUpdate){  
            if(checkRecursive.runOnceClaimUpdate()){
                claimList  = new List<Case>();
                system.debug(' Kemoji in Update');
                for(Sobject claimTemp : trigger.new){ 
                    Case claimRec = (Case)claimTemp;
                    Case oldCase = (Case) Trigger.OldMap.get(claimTemp.Id);
                    claimAssessmentList.add(claimRec);
                    if(claimRec.Status != oldCase.Status){
                        claimList.add(claimRec);
                        
                    }
                    if(claimRec.Assessment_Report_Created__c == true) { 
                        if(claimRec.insurance_Purchased__c == true  && (claimRec.Total_Estimate__c > oldCase.Total_Estimate__c ))
                        {
                            system.debug(claimRec);
                            String ID_Desc   = claimRec.ID + 'Purchase Additional Insurance';
                            caseforTaskSet.add(ID_Desc);
                            InscaseIDs.add(claimRec.ID);
                            system.debug(' addtionall ');
                            
                        }else{ 
                            
                            if(claimRec.State1__c != null){
                                if (claimRec.insurance_Purchased__c == false  && (claimRec.Total_Estimate__c > claimRec.State_Wise_Insurance_Thrsld__c) && 
                                    claimRec.State_Wise_Insurance_Thrsld__c > 0 ){
                                        String ID_Desc   = claimRec.ID + 'Purchase Insurance';
                                        caseforTaskSet.add(ID_Desc);
                                        InscaseIDs.add(claimRec.ID);
                                        system.debug('not in addi');
                                    }
                            }
                            
                        }
                    }
                    
                    
                }
            }
            
            /* if(checkRecursive.runOnceClaimUpdate()){
List<Case> newList = (List<Case>)Trigger.new;
Map<Id, Case> CaseoldMap = (Map<Id, Case>) Trigger.oldMap;
for(Case cse : newList)
{   
if(cse.insurance_Purchased__c == true  && (cse.Total_Estimate__c > CaseoldMap.get(cse.ID).Total_Estimate__c ))
{
String ID_Desc   = cse.ID + 'Insurance coverage exceeded, additional insurance required';
caseforTaskSet.add(ID_Desc);
system.debug(' addtionall ');

}else{ 

if(cse.State1__c != null){
if(cse.Total_Estimate__c  != CaseoldMap.get(cse.ID).Total_Estimate__c &&
cse.Total_Estimate__c > cse.State_Wise_Insurance_Thrsld__c) {
String ID_Desc   = cse.ID + 'The estimate is above $' + cse.State_Wise_Insurance_Thrsld__c +', insurance must be purchased'  ; 
caseforTaskSet.add(ID_Desc);
system.debug('not in addi');
}
}

}

}

} */
        }
        
    }
    public void beforeInsert(SObject so) {
    } 
    public void afterInsert(SObject so) {
        
    } 
    public void beforeUpdate(SObject oldSo, SObject so) {
        Case newcase = (Case)so;
        Case oldcase=(Case)oldSo;
        If (!System.isFuture() && !System.isBatch()){
            
            //phone validation
            If ((newcase.Home_Phone__c!=null && newcase.Home_Phone__c!=oldcase.Home_Phone__c)||
                (newcase.Customer_Mobile_Phone__c!=null && newcase.Customer_Mobile_Phone__c!=oldcase.Customer_Mobile_Phone__c)||
                (newcase.Customer_Contact_Phone__c!=null && newcase.Customer_Contact_Phone__c!=oldcase.Customer_Contact_Phone__c)){
                    
                    newcase.addError('Phone Numbers can not be updated at the claim level. Please update on the contact to reflect the same');
                    
                    
                }    
            
            
        }
        
        
    }
    public void beforeUnDelete(SObject so) {} 
    
    public void afterUpdate(SObject oldSo, SObject so) {
        
    }  
    public void beforeDelete(SObject so){} 
    public void afterDelete(SObject so) {} 
    public void afterUnDelete(SObject so) {} 
    public void andFinally() {
        system.debug('claimList :'+claimList);
        system.debug('claimAssessmentList :'+claimAssessmentList);
        if(Trigger.IsAfter){
            if (!claimList.isEmpty()||Trigger.IsInsert){
                CaseService.triggerClaimAssignmentRule(claimList); 
            } 
            if(!claimAssessmentList.isEmpty()){
                CaseService caseServ = new CaseService();
                caseServ.generateClaimAssessmentReportItems(claimAssessmentList,Trigger.OldMap);
                CaseService.updateClaimDeclineClause(claimAssessmentList,Trigger.OldMap);
                system.debug(claimAssessmentList);
                 system.debug(Trigger.OldMap);
            }
            if(!caseforTaskSet.isEmpty())
            {    
                //CaseService caseServ1 = new CaseService(); 
                system.debug('caseforTaskSet  Handler' + caseforTaskSet);
                  system.debug('InscaseIDs  Handler' + InscaseIDs);
                CaseService.createTasknWO(caseforTaskSet, InscaseIDs);
            }
        } 
    }               
}