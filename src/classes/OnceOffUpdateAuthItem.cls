global class OnceOffUpdateAuthItem implements Database.Batchable<sObject>, Database.RaisesPlatformEvents {
    
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query='select id,Authority__c,Claim_Job__c from Claim_Job_Work_Item__c';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Claim_Job_Work_Item__c> scope) {
    
        set<Id> CJWIdset=new set<Id>();
        map<string,Id> AuthCJmap=new map<string,Id>();
        for (Claim_Job_Work_Item__c cjitem:scope){
             CJWIdset.add(cjitem.Claim_Job__c);
        } 
        for(case c:[select id,CJID__c from case where CJID__c=:CJWIdset]){
            AuthCJmap.put(c.CJID__c,c.Id);
        
        }
        list<Claim_Job_Work_Item__c> updtCJWItemlst=new list<Claim_Job_Work_Item__c>();
        for (Claim_Job_Work_Item__c cjitem:scope){
        
             If (AuthCJmap.containskey(cjitem.Claim_Job__c)){
                 Claim_Job_Work_Item__c CJWItemTemp=new Claim_Job_Work_Item__c();
                 CJWItemTemp.Id=cjitem.Id;
                 CJWItemTemp.Authority__c=AuthCJmap.get(cjitem.Claim_Job__c);
                 updtCJWItemlst.add(CJWItemTemp);
                 
             }
                         
             
        } 
        update updtCJWItemlst;
       
      
    }
    
    global void finish(Database.BatchableContext bc){ 
        
    }
    
    
           
}