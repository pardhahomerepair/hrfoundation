public class CreateWorkOrderLineItemCtrl {
    @AuraEnabled
    public static WorkOrder getWorkOrderDetails(String workOrderId) {
        WorkOrder wo  = [SELECT Id, Status, Authority__r.Status,Case.Status,Case.Assessment_Count__c,RecordLocked__c FROM WorkOrder WHERE Id =: workOrderId];
        return wo;
    }
    @AuraEnabled 
    public static string createWorkOrderLineItem(WorkOrderLineItem woli){ //changed from void to string
        Savepoint sp; 
        try{
            sp = Database.setSavepoint();
            List <WorkOrderLineItem> returnList = new List <WorkOrderLineItem> ();        
            List<PricebookEntry> pricebookEntryList = [Select ID,Product2Id
                                                       FROM PricebookEntry 
                                                       WHERE Id =: woli.PricebookEntryId
                                                      ];
            
            List<PricebookEntry> pbeList = [Select ID,Product2Id
                                            FROM PricebookEntry 
                                            WHERE Product2Id =: pricebookEntryList[0].Product2Id AND
                                            Pricebook2.Name like 'Standard Price Book' AND IsActive = true
                                           ];
            WorkOrder wo=[Select id,WorkType.Name,Rectification__c,WorkTypeId from WorkOrder where Id=:woli.WorkOrderId limit 1];
            woli.WorkTypeId=wo.WorkTypeId;
            
            if (wo.Rectification__c == true){
                woli.RecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Rectification').getRecordTypeId();               
            }else{
                woli.RecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Repair Items').getRecordTypeId();
            }
            if(!pbeList.isEmpty())
                woli.PricebookEntryId = pbeList[0].Id;     
            insert woli;
            
            returnList = [Select ID, LineItemNumber FROM WorkOrderLineItem 
                          WHERE Id =: woli.Id
                         ]; 
            
            return returnList[0].LineItemNumber;
        }
        catch (System.DmlException e) {
            Database.rollback(sp);
            system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
            List<String> errorMessageList = new List<String>();
            errorMessageList.add(e.getDmlMessage(0));            
            String errorMessage = String.join(errorMessageList,',');
            return errorMessage;
        }
        catch (Exception e) {
            Database.rollback(sp);
            system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
            return e.getMessage();
        }
        
    }    
}