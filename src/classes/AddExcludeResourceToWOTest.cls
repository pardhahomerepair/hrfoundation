/***************************************************************** 
Purpose: Add ExcludeResource Work Order Testclass
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            XXXX          03/01/2020    Created      Home Repair Claim System  
*******************************************************************/
@istest 
public class AddExcludeResourceToWOTest { 
    @testSetup 
    static void setup(){
         //Create the Account Record.
        Account acc = HomeRepairTestDataFactory.createAccounts('Test WorkOrder Account')[0];        
        //Create the Contact Record.
        contact conVar = HomeRepairTestDataFactory.createTradeContact('test WO Contatct',acc.Id);        
        
        Home_Repairs_Trigger_Switch__c hrts = HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        BypassWOValidation__c  byps = HomeRepairTestDataFactory.createBypassWOValidation(false);
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);                
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');        
        case cj=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);        
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,cj.id); 
         
        
         
    }  
    static testmethod void testAddExcludeResourceToWO() {  
            
            Test.startTest(); 
                database.executeBatch(new AddExcludeResourceToWO(new map<Id,ServiceResource>()),1);
               
            Test.stopTest();
        
    }
    
   
    
    
}