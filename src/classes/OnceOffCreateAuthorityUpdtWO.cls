global class OnceOffCreateAuthorityUpdtWO implements Database.Batchable<sObject>, Database.RaisesPlatformEvents {
    
    public Boolean Batcherrors=false;
    public Boolean isIdSelection=false;
    public set<Id> CJIdSet=new set<Id>();
    global OnceOffCreateAuthorityUpdtWO(Boolean Batcherrors){
       this.Batcherrors=Batcherrors;
    }
    global OnceOffCreateAuthorityUpdtWO(set<Id> CJIdSet){
       this.CJIdSet=CJIdSet;
       isIdSelection=true;
    }      
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query='select id,job_type__c,Claim__c,Job_Number__c,Source_Internal_Reference_Id__c,Vendor_Reference_Number__c,Description__c,Date_Issued__c,Date_RespondBy__c,Reported_Date__c,';
        If (isIdSelection)
           query=query+'Status__c,Special_Instructions__c,Claim_Type__c,HomeAssist__c,Claim_Number__c,Source_Channel__c,Action_Type__c from Claim_Job__c where Id=:CJIdSet';
        else
           query=query+'Status__c,Special_Instructions__c,Claim_Type__c,HomeAssist__c,Claim_Number__c,Source_Channel__c,Action_Type__c from Claim_Job__c where Batcherror__c=:Batcherrors';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Claim_Job__c> scope) {
    
        set<Id> CJIdset=new set<Id>();
        map<Id,map<string,Id>> AuthCJmap=new map<Id,map<string,Id>>();
        list<workorder> instworkorder=new list<workorder>();
        list<case> authlst=new list<case>();
        for (Claim_Job__c cj:scope){
             case auth=getAuthorityClaim(cj,cj.Claim__c);
             authlst.add(auth);
             CJIdset.add(cj.Id);
        } 
        insert authlst;
        for(case c:authlst){
           if(!AuthCJmap.containskey(c.parentId)) 
             AuthCJmap.put(c.parentId,new map<string,id>{c.CJID__c=>c.Id});
           else    
             AuthCJmap.get(c.parentId).put(c.CJID__c,c.Id);
        }
        map<Id,workorder> womap=new map<Id,workorder>([select Id,Authority__c,CaseId,Claim_Job__c from workorder where Claim_Job__c=:CJIdset]);
        for(workorder wo:[select Id,Authority__c,CaseId,Claim_Job__c from workorder where Claim_Job__c=:CJIdset]){
            If (AuthCJmap.containskey(wo.caseId)){
                workorder wotemp=womap.get(wo.id);
                wotemp.Authority__c=AuthCJmap.get(wo.caseId).get(wo.Claim_Job__c);
            }       
        }
        update womap.values();
        map<Id,AR_Invoice__c> ARInvcmap=new map<Id,AR_Invoice__c>([select Id,Authority__c,Claim__c,Claim_Job__c from AR_Invoice__c where Claim_Job__c=:CJIdset]);
        for(AR_Invoice__c ARInvc:[select Id,Authority__c,Claim__c,Claim_Job__c from AR_Invoice__c where Claim_Job__c=:CJIdset]){
            If (AuthCJmap.containskey(ARInvc.Claim__c)){
                AR_Invoice__c artemp=ARInvcmap.get(ARInvc.id);
                artemp.Authority__c=AuthCJmap.get(ARInvc.Claim__c).get(ARInvc.Claim_Job__c);
            }       
        }
        update ARInvcmap.values();
      
    }
    
    global void finish(Database.BatchableContext bc){ 
        
    }
    
    private boolean isClaimJobOld(Claim_Job__c cjis) {
        string ClaimNumber=cjis.Claim_Number__c.touppercase();
        list<Case> cjc=[select Id from Case where Job_Number__c=:cjis.Job_Number__c and Claim_Number__c=:ClaimNumber];
        if (cjc.size()>0) return true; else return false;
       
   }
    
    private case getAuthorityClaim(Claim_Job__c cjis,Id caseId) {
 
        case c = new case();
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
        c.Job_Number__c = cjis.Job_Number__c ;
        c.Source_Internal_Reference_Id__c = cjis.Source_Internal_Reference_Id__c;
        c.Vendor_Reference_Number__c = cjis.Vendor_Reference_Number__c;
        c.Description = cjis.Description__c;
        c.Date_Issued__c = cjis.Date_Issued__c;
        c.Date_RespondBy__c = cjis.Date_RespondBy__c;
        c.Reported_Date__c = cjis.Reported_Date__c;
        If (!isClaimJobOld(cjis)) c.Status = cjis.Status__c;
        c.Special_Instructions__c = cjis.Special_Instructions__c;
        c.type = cjis.Claim_Type__c;
        if (cjis.HomeAssist__c == true) {
            c.Job_Type__c = BatchClaimJobIngestionStagingProcess.JOB_TYPE_HOME_ASSISST;
        } else {
          if ((cjis.Claim_Type__c== BatchClaimJobIngestionStagingProcess.CLAIM_TYPE_REPAIR || cjis.Claim_Type__c== BatchClaimJobIngestionStagingProcess.CLAIM_TYPE_REPLACE) && cjis.Job_Type__c== BatchClaimJobIngestionStagingProcess.JOB_SUB_TYPE_DOANDCHARGE){
            c.Job_Type__c = BatchClaimJobIngestionStagingProcess.JOB_TYPE_CONTENTS; 
          } else{
            c.Job_Type__c = cjis.Job_Type__c; 
          }
        }
        c.Action_Type__c = cjis.Action_Type__c; 
        c.HomeAssist__c = cjis.HomeAssist__c;
        if(cjis.Claim_Number__c!= null) c.parentId = caseId;
        c.CJID__c=cjis.Id;  
        c.Source_Channel__c = cjis.Source_Channel__c; 
        return c;
      }
    
           
}