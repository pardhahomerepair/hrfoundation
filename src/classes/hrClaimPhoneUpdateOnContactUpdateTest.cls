@isTest
public class hrClaimPhoneUpdateOnContactUpdateTest {

   @TestSetup
    public static void setupTestData() {
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        case cj=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        
         
         
    }
  
    public static testMethod void testClaimUpdateOnContactUpdate() {
        list<Id> ctcids=new list<Id>();
        contact firstcon=[select id,phone,mobilephone from contact limit 1];
        firstcon.phone='0265874112';
        update firstcon;
        Contact secondcon=HomeRepairTestDataFactory.createContact('SecondTest');
        ctcids.add(firstcon.id);  
        ctcids.add(secondcon.id); 
        Test.startTest();
            hrClaimPhoneUpdateOnContactUpdate.collectCTCforClaimPhoneUpdates(ctcids);
            
        Test.stopTest();        
    }
}