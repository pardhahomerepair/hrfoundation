/************************************************************************************************************
Name: TaskService
=============================================================================================================
Purpose: Task Service
===============================================================================================================
History
-----------
VERSION    AUTHOR                       
1.0        Jitendra Kawale          HomeRepair - Finance Module
*************************************************************************************************************/
public without sharing class TaskService {
/**
*
* Generate AR Invoice and completion call tasks for completed jobs
*/  
    /*public static void createInvoiceRecord(List<WorkOrder> WorkOrderList) {
        system.debug('WorkOrderList==>'+WorkOrderList);
        Set<Id> claimJobList = new Set<Id>();
        
        for(WorkOrder order : WorkOrderList) {
            claimJobList.add(order.Claim_Job__c);
        }
        
        Map<Id, Claim_Job__c> claimMap = new Map<Id, Claim_Job__c>([SELECT Id, claim__c, Job_Type__c, (Select Id FROM Work_Orders__r WHERE Status !=: NJ_CONSTANTS.WO_STATUS_CLOSED AND Do_Not_Invoice_AR__c != true AND Internal_Resource__c != true AND WorkType.Name !=: NJ_CONSTANTS.JOB_TYPE_ASSESMENT )
                                                                    FROM Claim_Job__c WHERE Id IN : claimJobList]);
        System.debug('claimMap==>'+claimMap);
        Map<Id, Claim_Job__c> otherJobList = new Map<Id, Claim_Job__c>();
        List<Claim_Job__c>  dAndCJobList = new List<Claim_Job__c>();
        for(Claim_Job__c job : claimMap.values()) {
            if(job.Work_Orders__r.size() == 0) {
                if(job.Job_Type__c == NJ_CONSTANTS.JOB_TYPE_DOANDCHARGE || job.Job_Type__c == NJ_CONSTANTS.JOB_TYPE_CONTENT)
                    dAndCJobList.add(job);
                else
                    otherJobList.put(job.Id, job);
            }
        }
        List<User> userList=[Select Id,Name,Email 
                             FROM User
                             WHERE Name =: NJ_CONSTANTS.HR_CLAIM 
                             Limit 1];
        if(!dAndCJobList.isEmpty()) {
            List<Task> completionCallTasks = new List<Task>();
            for(Claim_Job__c job : dAndCJobList) {
                completionCallTasks.add(new Task(priority = NJ_CONSTANTS.TASK_PRIORITY_IMPORTANT ,
                                                 status = NJ_CONSTANTS.TASK_STATUS_NOT_STARTED ,
                                                 subject = NJ_Constants.TASK_COMPLETION_CALL_SUBJECT,
                                                 IsReminderSet = true,                             
                                                 OwnerId = userList[0].id,
                                                 ActivityDate = System.Today() + 1,
                                                 ReminderDateTime = System.now()+1,
                                                 WhatId =  job.Claim__c,
                                                 Claim_Job__c = job.Id));
            }
            insert completionCallTasks;
            system.debug('completionCallTasks==>'+completionCallTasks);
        }      
        if(!otherJobList.isEmpty()) {
            createInvoiceRecord(otherJobList.keySet());
        }
        
    }*/
    
    //Commented BY CRMIT
    //public static final Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HR Authority').getRecordTypeId();
    //Added By CRMIT getting recordtype id based on DeveloperName
    public static final Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
   
    @InvocableMethod
    public static void invokeARGenerationFromAP(List<Id> JobidList) {
        Set<Id> jobSet = new Set<Id>();
        jobSet.addAll(JobidList);
        invokeARGeneration(jobSet);
    }
    //Added by crmit to migrated logic from claimjob to Authority
    public static void invokeARGeneration(Set<Id> jobIdList) {
        system.debug('In Invoice Generation Process');
        //Rejected AR Invoice excluded
        Map<Id, case> authorityMap = new Map<Id, case>([SELECT Id, parentId, Job_Type__c, (Select Id, AR_Invoice_Created_Flag__c FROM Work_Orders__r WHERE (Status !=: NJ_CONSTANTS.WO_STATUS_CLOSED AND Status !=: NJ_CONSTANTS.WO_STATUS_CANCELLED)),
        (SELECT Id, Status FROM Activities__r Where Subject =: NJ_Constants.TASK_COMPLETION_CALL_SUBJECT OR Subject =: NJ_Constants.FINAL_CONTACT_CALL_SUBJECT),
        (SELECT Id FROM AR_Invoices1__r WHERE Invoice_Type__c =: NJ_CONSTANTS.INVOICE_CUSTOMER AND Status__c !=:NJ_CONSTANTS.ARINVOICE_STATUS_REJECTED)
                                                                    FROM case WHERE Id IN : jobIdList and RecordTypeId =: hrAuthorityRecId]);
        Set<Id> workOrderList = new Set<Id>();
        Set<Id> relatedJobId = new Set<Id>();        
        for(case c : authorityMap.values()) {
            /*Integer index = 0;
            for(WorkOrder order : job.Work_Orders__r) {
                workOrderList.add(order.Id);
                if(order.AR_Invoice_Created_Flag__c) {                 
                    index++;
                }
            }*/
            //Check if all workorders closed and Completion call is completed
            if(c.Work_Orders__r.size() == 0 && (c.Job_Type__c == NJ_CONSTANTS.JOB_TYPE_DOANDCHARGE || c.Job_Type__c == NJ_CONSTANTS.JOB_TYPE_CONTENT) && c.AR_Invoices1__r.size() == 0) {
                system.debug('job.Activities__r==>'+c.Activities__r);
                 //Old code commented
                /* if(job.Activities__r.size() > 0 ) {
                    for(Sobject tsk : job.Activities__r) {
                        if(tsk.get('Status') == NJ_CONSTANTS.TASK_STATUS_COMPLETED)                  
                            relatedJobId.add(job.Id);                       
                    }
                }*/
                //Check if all completion call are completed for a claimjob to create ARInvoice
                Integer tskIndex = 0;
                if(c.Activities__r.size() > 0 ) {
                    for(Sobject tsk : c.Activities__r) {
                        if(tsk.get('Status') == NJ_CONSTANTS.TASK_STATUS_COMPLETED){                
                            tskIndex++;
                        }                                               
                    }
                    if(c.Activities__r.size()==tskIndex)
                    {
                        relatedJobId.add(c.Id); 
                        system.debug('---------------- All completion calls are completed ---------------');

                    }
                }
            }
            //check if all activities closed
            else if(c.Work_Orders__r.size() == 0 && c.AR_Invoices1__r.size() == 0)
                relatedJobId.add(c.Id);
                
        }
        if(!relatedJobId.isEmpty())
                createInvoiceRecord(relatedJobId);
        /*//Check if all related AP Invoices are approved
        if(!relatedJobId.isEmpty()) {
            List<AP_Invoice__c> invoiceList = [SELECT Id, Status__c, Work_Order__r.Claim_Job__c FROM AP_Invoice__c WHERE Work_Order__c IN: workOrderList];  
            Map<Id, List<AP_Invoice__c>> jobInvoiceMap = new Map<Id, List<AP_Invoice__c>>();
            for(AP_Invoice__c invoice : invoiceList) {
                if(jobInvoiceMap.containsKey(invoice.Work_Order__r.Claim_Job__c)) {
                    List<AP_Invoice__c> apInvoices = jobInvoiceMap.get(invoice.Work_Order__r.Claim_Job__c);
                    apInvoices.add(invoice);
                    jobInvoiceMap.put(invoice.Work_Order__r.Claim_Job__c, apInvoices);
                }
                else
                    jobInvoiceMap.put(invoice.Work_Order__r.Claim_Job__c, new List<AP_Invoice__c>{invoice});
            }    
            
            Set<Id> jobIdToGenerateInvoices = new Set<Id>();
            for(Id cJobId : jobInvoiceMap.KeySet()) {   
                Integer approvedInvIndex = 0;
                for(AP_Invoice__c inv : jobInvoiceMap.get(cJobId)) {
                    if(inv.Status__c == NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED) 
                        approvedInvIndex++;
                }           
                if(approvedInvIndex == jobInvoiceMap.get(cJobId).size())
                    jobIdToGenerateInvoices.add(cJobId);
            }
            if(!jobIdToGenerateInvoices.isEmpty())
                createInvoiceRecord(jobIdToGenerateInvoices);
        }*/
    }
    public static void createCompletionCallTask(Set<Id> jobIdList) {
        Map<Id, case> auMap = new Map<Id, case>([SELECT Id, ParentId, Job_Type__c, 
                                                                   (Select Id FROM Work_Orders__r WHERE (Status !=: NJ_CONSTANTS.WO_STATUS_CLOSED AND Status !=: NJ_CONSTANTS.WO_STATUS_JOBCOMPLETE AND Status !=: NJ_CONSTANTS.WO_STATUS_CANCELLED) )
                                                                    FROM case WHERE Id IN : jobIdList AND Completion_Call_created__c != true and RecordTypeId =: hrAuthorityRecId]);
        System.debug('claimMap==>'+auMap);   
        Map<Id, case> CancelauMap = new Map<Id, case>([SELECT Id, ParentId, Job_Type__c, 
                                                                   (Select Id FROM Work_Orders__r WHERE (Status !=: NJ_CONSTANTS.WO_STATUS_CANCELLED) )
                                                                    FROM case WHERE Id IN : jobIdList and RecordTypeId =: hrAuthorityRecId]);
        System.debug('Cancel claim Map ===>' + CancelauMap);
        if(!auMap.isEmpty()) {
            List<case>  dAndCJobList = new List<case>();
            List<case>  otherJobList = new List<case>();
            for(case authCase : auMap.values()) {
                system.debug('job.Work_Orders__r==>'+authCase.Work_Orders__r);
                if(authCase.Work_Orders__r.size() == 0) {
                     if(authCase.Job_Type__c == NJ_CONSTANTS.JOB_TYPE_DOANDCHARGE || authCase.Job_Type__c == NJ_CONSTANTS.JOB_TYPE_CONTENT)
                        //If the workorder is cancelled skip creating completion call task
                        If(CancelauMap.get(authCase.Id).Work_Orders__r.size()!=0){
                            dAndCJobList.add(authCase);  
                        }
                    else
                        otherJobList.add(authCase);          
                          
                    }
                }
            system.debug('dAndCJobList==>'+dAndCJobList);
            system.debug('otherJobList==>'+otherJobList);
            List<User> userList=[Select Id,Name,Email 
                                 FROM User
                                 WHERE Name = :NJ_CONSTANTS.HR_CLAIM 
                                 Limit 1];
            System.debug('userList '+userList);
            if(!dAndCJobList.isEmpty()) {
                List<Task> completionCallTasks = new List<Task>();
                for(case authCase : dAndCJobList) {
                    completionCallTasks.add(new Task(priority = NJ_CONSTANTS.TASK_PRIORITY_IMPORTANT ,
                                                     status = NJ_CONSTANTS.TASK_STATUS_NOT_STARTED ,
                                                     subject = NJ_Constants.TASK_COMPLETION_CALL_SUBJECT,
                                                     IsReminderSet = true,                             
                                                     OwnerId = userList[0].id,
                                                     ActivityDate = System.Today() + 1,
                                                     ReminderDateTime = System.now()+1,
                                                     WhatId =  authCase.ParentId,
                                                     Authority__c = authCase.Id));
                    authCase.Completion_Call_created__c = true;
                    authCase.Date_Completed__c = system.today();
                }
                
                insert completionCallTasks;
                //update dAndCJobList;
                system.debug('completionCallTasks in task service==>'+completionCallTasks);
            }
            if(!otherJobList.isEmpty()) {
                for(case authCase : otherJobList) {
                    authCase.Date_Completed__c = system.today();
                }
                dAndCJobList.addAll(otherJobList);
            }
            
            update dAndCJobList;
        }
    }
    public static void createInvoiceRecord(set<Id> jobList) {
        Map<Id, List<AP_Invoice__c>> jobAPMap = new Map<Id, List<AP_Invoice__c>>();
        Set<Id> apInvoiceIdSet = new Set<Id>();
        system.debug('jobList==>'+jobList);   
        Map<string, String> codeMap = new Map<string, String>();
        Map<string, String> codeMapGST = new Map<string, String>();    
        List<AR_Distribution_Code__mdt> distributionSet = [SELECT Id, Distribution_Set_GST__c, Distribution_Set_Total__c, Invoice_Type__c, 
                                                           Job_Type__c,  State__c  FROM AR_Distribution_Code__mdt WHERE Invoice_Type__c ='Customer'];
        for(AR_Distribution_Code__mdt code : distributionSet) {
            String key = code.Job_Type__c+''+code.State__c;            
            codeMap.put(key, code.Distribution_Set_Total__c);
            codeMapGST.put(key, code.Distribution_Set_GST__c);
        }
        system.debug('codeMap==>'+codeMap);
        Map<Id, case> claimAuthMap = new Map<Id, case>([SELECT Id, Job_Type__c, ParentId, Parent.Policy__r.State__c FROM case WHERE Id IN : jobList and RecordTypeId =: hrAuthorityRecId]);
        system.debug('claimjobMap==>'+claimAuthMap);
        List<AP_Invoice__c> APInvoiceList = [SELECT Id, Work_Order__c, Work_Order__r.Authority__c 
                                            //(SELECT Id, Prepayment_Amount__c, Quantity__c, Scope_Of_Work__c, Trade__c, UOM__c FROM AP_SOWs__r)
                                            FROM AP_Invoice__c WHERE Work_Order__r.Authority__c IN:jobList and Status__c =: NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED];
        system.debug('11111111=====' +[SELECT Id, Work_Order__c, Work_Order__r.Authority__c 
                                            FROM AP_Invoice__c]);
        system.debug('jobList=====' +jobList);
        system.debug('APInvoiceList=====' +APInvoiceList);
        system.debug('NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED=====' +NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED);
        
        if(APInvoiceList.size() > 0) {
            for(AP_Invoice__c inv : APInvoiceList) {            
                if(jobAPMap.containsKey(inv.Work_Order__r.Authority__c)) {
                    List<AP_Invoice__c> orderList = jobAPMap.get(inv.Work_Order__r.Authority__c);
                    orderList.add(inv);
                    jobAPMap.put(inv.Work_Order__r.Authority__c, orderList);
                }
                else 
                    jobAPMap.put(inv.Work_Order__r.Authority__c, new List<AP_Invoice__c>{ inv });
            }       
            
            
            //Group ARQueue = [Select Id FROM Group Where Type='Queue' AND DeveloperName = 'AR_Invoice_Queue'];
            List<AR_Invoice__c> customerInvoices = new List<AR_Invoice__c>();
            for(Id jobId : jobAPMap.keySet()) {
                case job = claimAuthMap.get(jobId);
                AR_Invoice__c CusInvToCreate = new AR_Invoice__c();
                CusInvToCreate.Authority__c = jobId;
                CusInvToCreate.Claim__c = job.ParentId;
                CusInvToCreate.Status__c = NJ_CONSTANTS.AP_INVOICE_STATUS_SUBMITTED;
                CusInvToCreate.Invoice_Type__c = NJ_CONSTANTS.INVOICE_CUSTOMER;
                CusInvToCreate.Generate_Invoice_Statement__c = true;            
                String key = job.Job_Type__c+''+job.parent.Policy__r.State__c; 
                system.debug('key==>'+key);
                CusInvToCreate.Distribution_Set_Total_ex_GST__c = codeMap.get(key);
                CusInvToCreate.Distribution_Set_GST__c  = codeMapGST.get(key);
                system.debug('Distribution_Set_Total_ex_GST__c==>'+CusInvToCreate.Distribution_Set_Total_ex_GST__c);
                //CusInvToCreate.Labour_ex_GST__c = jobInvoiceMap.get(jobId).labourSum - jobInvoiceMap.get(jobId).creditMemoLabourSum; 
                //CusInvToCreate.Material_ex_GST__c = jobInvoiceMap.get(jobId).materialSum - jobInvoiceMap.get(jobId).creditMemomaterialSum; 
                customerInvoices.add(CusInvToCreate);
            }
            system.debug('customerInvoices==>'+customerInvoices);
            if(!customerInvoices.isEmpty()) {
                insert customerInvoices;            
                Set<Id> arInvoiceIdSet = new Set<Id>();
                for(AR_Invoice__c arInvoice : customerInvoices) {
                    arInvoiceIdSet.add(arInvoice.Id);
                }
                
                //Update AR reference on AP
                /*List<AP_Invoice__c> apListToUpdate = new List<AP_Invoice__c>();        
                for(AR_Invoice__c arInvoice : customerInvoices) {
                    for(AP_Invoice__c inv : jobAPMap.get(arInvoice.Claim_Job__c)) { 
                        apInvoiceIdSet.add(inv.Id);
                        inv.AR_invoice_Created__c = true;
                        inv.AR_Invoice__c = arInvoice.Id;
                        apListToUpdate.add(inv);                
                    }
                }        
                
                update apListToUpdate;*/
                updateARAmount(jobAPMap.keySet());
            }
        }
    }   
    public static void updateARAmount(Set<Id> claimAuthId) {
        Map<Id, Id> claimAuthInvoiceMap = new Map<Id, Id>();
        Set<Id> arInvoiceSet = new Set<Id>();
        Set<Id> customerInvoiceSet = new Set<Id>();
         //Rejected AR Invoice excluded
        List<AR_Invoice__c> arInvoiceList = [SELECT Id, Authority__c,Status__c FROM AR_Invoice__c WHERE Authority__c IN: claimAuthId AND Invoice_Type__c = 'Customer' AND Status__c !=:NJ_CONSTANTS.ARINVOICE_STATUS_REJECTED];
        map<Id,string> arInvcstatus=new map<Id,string>();
        for (AR_Invoice__c arInvc:arInvoiceList){
             arInvcstatus.put(arInvc.Id,arInvc.Status__c);
                
        }
        for(AR_Invoice__c invoice : arInvoiceList) {
            claimAuthInvoiceMap.put(invoice.Authority__c, invoice.Id);
            arInvoiceSet.add(invoice.Id);
        }
        List<AP_Invoice__c> apInvList = [SELECT Id, Work_Order__r.Authority__c FROM AP_Invoice__c WHERE Work_Order__r.Authority__c IN: claimAuthInvoiceMap.keySet() AND Status__c =: NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED]; 
        system.debug('apInvList==>'+apInvList);
        for(AP_Invoice__c inv : apInvList) {
            inv.AR_invoice_Created__c = true;
            inv.AR_Invoice__c = claimAuthInvoiceMap.get(inv.Work_Order__r.Authority__c);
        }
        //Logic to update APInvoices references to the newly created AR Invoices
        map<Id,AR_Invoice__c> cliamARInvoicemap=new map<Id,AR_Invoice__c>();
        for(AR_Invoice__c  arInvoice:[select Id,Authority__c from AR_Invoice__c where Authority__c IN: claimAuthId AND Status__c=:NJ_CONSTANTS.ARINVOICE_STATUS_SUBMITTED]){
            cliamARInvoicemap.put(arInvoice.Authority__c,arInvoice);  
        }
        for(case cAuthARInvoice:[select Id,(SELECT Id FROM AR_Invoices1__r WHERE Invoice_Type__c =: NJ_CONSTANTS.INVOICE_CREDIT_MEMO_CUSTOMER AND Status__c =:NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED)
                                     FROM case WHERE Id=:claimAuthId and RecordTypeId =: hrAuthorityRecId]){
                          
            If (cAuthARInvoice.AR_Invoices1__r.size()!=0 && cliamARInvoicemap.containsKey(cAuthARInvoice.Id)){
                    for(AP_Invoice__c inv : apInvList) {
                        inv.AR_invoice_Created__c = true;
                        inv.AR_Invoice__c = cliamARInvoicemap.get(cAuthARInvoice.Id).Id;
                    }
                         
            }           
        }                 
        update apInvList;
        
        List<AggregateResult> summaryResult = [SELECT SUM(Labour_ex_GST__c) labour, SUM(Material_ex_GST__c) material, AR_Invoice__c inv 
                                FROM AP_Invoice__c WHERE AR_Invoice__c IN: arInvoiceSet AND Do_Not_Invoice_AR__c = false 
                                AND Status__c =: NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED GROUP BY AR_Invoice__c];
                                
        List<AR_Invoice__c> customerInvoiceList = new List<AR_Invoice__c>();
        for(AggregateResult result : summaryResult) {
            AR_Invoice__c invoice = new AR_Invoice__c();
            invoice.Id = (Id)result.get('inv');
            if(invoice.Id !=null) {
                invoice.Labour_ex_GST__c = (decimal)result.get('labour');
                invoice.Material_ex_GST__c = (decimal)result.get('material');
                customerInvoiceList.add(invoice);
                customerInvoiceSet.add(invoice.id);
            }
        }
        
        //Logic to update AR Invoice when all the AP Invoices have Do Not Invoice AR set to true
        for(AR_Invoice__c arInvoice : arInvoiceList){
            if(!customerInvoiceSet.contains(arinvoice.id)){
                AR_Invoice__c invoice = new AR_Invoice__c();
                invoice.Id = (Id)arInvoice.Id;
                if(invoice.Id !=null && arInvcstatus.get(arInvoice.Id)!='Approved'){
                    invoice.Labour_ex_GST__c = 0;
                    invoice.Material_ex_GST__c = 0;
                    customerInvoiceList.add(invoice);
                }
            }
        }
        
        system.debug('customerInvoiceList==>'+customerInvoiceList);
        if(!customerInvoiceList.isEmpty()) {
            update customerInvoiceList;
            
            //recreate AP_SOW__c 
            List<AR_SOW__c> existingSow = [SELECT Id FROM AR_SOW__c WHERE AR_Invoice__c IN: arInvoiceSet];
            if(!existingSow.isEmpty())
                delete existingSow;
            
            List<AP_Invoice__c> APInvoiceList = [SELECT Id, Work_Order__c, Work_Order__r.Authority__C, AR_Invoice__c, 
                                                (SELECT Id, Prepayment_Amount__c, Quantity__c, Room__c, Scope_Of_Work__c, Trade__c, UOM__c, AP_Invoice__c, AP_Invoice__r.Do_Not_Invoice_AR__c FROM AP_SOWs__r)
                                                FROM AP_Invoice__c WHERE AR_Invoice__c IN: arInvoiceSet AND Do_Not_Invoice_AR__c = false AND Status__c =: NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED ];
                                                
            Map<Id, List<AP_SOW__c>> ARSOWMap = new Map<Id, List<AP_SOW__c>>();                     
            for(AP_Invoice__c arInv : APInvoiceList) {
                for(AP_SOW__c sow : arInv.AP_SOWs__r) {
                    if(ARSOWMap.containsKey(arInv.AR_Invoice__c)) {
                        List<AP_SOW__c> sowList = ARSOWMap.get(arInv.AR_Invoice__c);
                        sowList.add(sow);
                        ARSOWMap.put(arInv.AR_Invoice__c, sowList);
                    }
                    else
                        ARSOWMap.put(arInv.AR_Invoice__c, new List<AP_SOW__c>{sow});
                }
            }
            //create AR_Sow
            List<AR_SOW__c> sowList = new List<AR_SOW__c>();
            for(Id AR : ARSOWMap.keySet()) {
                for(AP_SOW__c lineItem : ARSOWMap.get(AR)) {   
                    if(!lineItem.AP_Invoice__r.Do_Not_Invoice_AR__c) {                 
                        AR_SOW__c InvLiItemToCreate = new AR_SOW__c();
                        InvLiItemToCreate.AR_Invoice__c = AR;
                        InvLiItemToCreate.Scope_Of_Work__c = lineItem.Scope_Of_Work__c;
                        InvLiItemToCreate.Trade__c = lineItem.Trade__c;
                        InvLiItemToCreate.Quantity__c = lineItem.Quantity__c;
                        InvLiItemToCreate.UOM__c = lineItem.UOM__c;
                        InvLiItemToCreate.Room__c = lineItem.Room__c;
                        //InvLiItemToCreate.Room__c = lineItem.Room__r.Name;
                        sowList.add(InvLiItemToCreate);    
                    }                
                }
            }
            insert sowList;
        }
    }
    //Create Manual AR from Claim Job if a Rejected AR Invoice is present and all the Work Orders are closed and completion call is completed
    @auraEnabled
    public static string createManualAR(String recordId) {
        System.debug('recordId------------>' +recordId);
        Set<Id> authoritySet = new Set<Id>();
        authoritySet.add(recordId);
        case authorityClaim = new case();    
               authorityClaim = [SELECT Id, Rejected_AR_Invoice__c, (Select Id FROM Work_Orders__r WHERE (Status !=: NJ_CONSTANTS.WO_STATUS_CLOSED AND Status !=: NJ_CONSTANTS.WO_STATUS_CANCELLED)),
        (SELECT Id, Status FROM Activities__r Where (Subject =: NJ_Constants.TASK_COMPLETION_CALL_SUBJECT OR Subject =: NJ_Constants.FINAL_CONTACT_CALL_SUBJECT) AND status = :NJ_CONSTANTS.TASK_STATUS_NOT_STARTED),
        (SELECT Id FROM AR_Invoices1__r WHERE Invoice_Type__c =: NJ_CONSTANTS.INVOICE_CUSTOMER AND Status__c !=:NJ_CONSTANTS.ARINVOICE_STATUS_REJECTED)
                                                                    FROM Case WHERE Id=:recordId and RecordTypeId =: hrAuthorityRecId];
        System.debug('authorityClaim------------>' +authorityClaim);
        If(authorityClaim.AR_Invoices1__r.size()!=0){
        
            Case auARInvoice = new Case();
            auARInvoice=[select Id,(SELECT Id FROM AR_Invoices1__r WHERE Invoice_Type__c =: NJ_CONSTANTS.INVOICE_CREDIT_MEMO_CUSTOMER AND Status__c =:NJ_CONSTANTS.ARINVOICE_STATUS_APPROVED)
                         FROM case WHERE Id=:recordId];
           	System.debug('auARInvoice------------>' +auARInvoice);

            If  (auARInvoice.AR_Invoices1__r.size()!=0 && authorityClaim.Work_Orders__r.size()==0){
                Set<Id> cJobSet = new Set<Id>();
                cJobSet.add((Id)recordId);
                 createInvoiceRecord(cJobSet);
                return 'Success';
            
            }
            else{
             return 'AR Invoice already generated for this Claim Job';
            }
        }
          else if(authorityClaim.Activities__r.size()==0 && authorityClaim.Rejected_AR_Invoice__c>0 && authorityClaim.AR_Invoices1__r.size() ==0){

            If(authorityClaim.Work_Orders__r.size()==0){
                Set<Id> cJobSet = new Set<Id>();
                cJobSet.add((Id)recordId);
                invokeARGeneration(cJobSet);
                return 'Success';
            }
            else {
                return 'Cannot create AR Invoice. Work Orders still Open';
            }  
        }else{
            return 'Cannot create AR Invoice.';
        }
    }
    
    
    public class ARInvoiceWrapper {
        public Decimal labourSum = 0;
        public Decimal materialSum = 0;
        public Decimal creditMemoLabourSum = 0;
        public Decimal creditMemoMaterialSum = 0;
    }
}