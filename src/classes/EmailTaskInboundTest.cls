@isTest
private class EmailTaskInboundTest{
    @isTest
    static void EmailTaskInboundWithClaimTest(){
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Carpenter');
         //Create Claim Authority Record(Added by CRMIT)
        Case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);        
        List<WorkOrderLineItem> woliList=HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
        List<String> toAddresses = new List<String>(); 
        toAddresses.add('test@test.com');
        EmailTaskInbound emailTask = new EmailTaskInbound();
        Messaging.inboundEmail email = new Messaging.inboundEmail();
        email.subject = 'claim #' + cs.CaseNumber;
        email.plainTextBody = 'my plain text';
        email.toAddresses=toAddresses;
        
        // add an attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfile.txt';
        attachment.mimeTypeSubType = 'text/plain';
        
        Messaging.Inboundemail.TextAttachment attachmentText = new Messaging.Inboundemail.TextAttachment();
        attachmentText.body = 'my attachment text';
        attachmentText.fileName = 'textfile.txt';
        attachmentText.mimeTypeSubType = 'text/plain';
        
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        email.textAttachments= new Messaging.Inboundemail.TextAttachment[]{attachmentText};                
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();        
        emailTask.handleInboundEmail(email, env);        
    }
    
    @isTest
    static void EmailTaskInboundWithoutClaimTest(){
        List<String> toAddresses = new List<String>(); 
        toAddresses.add('test@test.com');
        EmailTaskInbound emailTask = new EmailTaskInbound();
        Messaging.inboundEmail email = new Messaging.inboundEmail();
        email.subject = 'case #';
        email.plainTextBody = 'my plain text';
        email.toAddresses=toAddresses;
        
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();        
        emailTask.handleInboundEmail(email, env);
    }
    @isTest
    static void EmailTaskInboundWithClaimTest2(){
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Carpenter');
        //Create Claim Authority Record(Added by CRMIT)
        Case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);
        list<WorkOrder> ww = new list<workOrder>([SELECT ID, WorkOrderNumber FROM workOrder where ID = :wo.ID]);        
        List<WorkOrderLineItem> woliList=HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
        List<String> toAddresses = new List<String>(); 
        toAddresses.add('test@test.com');
        EmailTaskInbound emailTask = new EmailTaskInbound();
        Messaging.inboundEmail email = new Messaging.inboundEmail();
        email.subject = 'Quote WO#'+ww[0].WorkOrderNumber+'#';
        email.plainTextBody = 'my plain text';
        email.toAddresses=toAddresses;
        
        // add an attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfile.txt';
        attachment.mimeTypeSubType = 'text/plain';
        
        Messaging.Inboundemail.TextAttachment attachmentText = new Messaging.Inboundemail.TextAttachment();
        attachmentText.body = 'my attachment text';
        attachmentText.fileName = 'textfile.txt';
        attachmentText.mimeTypeSubType = 'text/plain';
        
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        email.textAttachments= new Messaging.Inboundemail.TextAttachment[]{attachmentText};                
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();        
        emailTask.handleInboundEmail(email, env);        
    }
    
}