<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Priority_to_Normal</fullName>
        <field>Priority</field>
        <literalValue>Normal</literalValue>
        <name>Set Priority to Normal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Priority_to_Urgent</fullName>
        <field>Priority</field>
        <literalValue>Urgent</literalValue>
        <name>Set Priority to Urgent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Authority Priority</fullName>
        <actions>
            <name>Set_Priority_to_Urgent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Job_Type__c</field>
            <operation>equals</operation>
            <value>makeSafe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR Authority</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Job_Type__c</field>
            <operation>equals</operation>
            <value>homeAssist</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Priority based on Activities</fullName>
        <actions>
            <name>Update_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR Case Feed</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Priority to Normal</fullName>
        <actions>
            <name>Set_Priority_to_Normal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR Authority</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Job_Type__c</field>
            <operation>notEqual</operation>
            <value>makeSafe,homeAssist</value>
        </criteriaItems>
        <description>For the Authority Cases, set the priority to normal</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
